import { FunctionComponent, useState, useEffect } from 'react';
import Image from 'next/image';

import type { Dog, ReservesHP } from '../lib/types';
import Close from '../public/close-white.svg';
import Vaccine from '../public/content/vaccine.svg';

import moment from 'moment';
import 'moment/locale/es';
import  { Popconfirm } from 'antd';
import { useQueryClient } from 'react-query';
import { ToastContainer, toast } from 'react-toastify';

import CardParadise from './HouseParadise/cmp/CardParadise';
const { NEXT_PUBLIC_API_URL } = process.env;

type Props = {
  detail: ReservesHP;
  handleCloseDetail: () => void;
  showEditReserve: (id: string) => void;
};

const ReserveDetail: FunctionComponent<Props> = (props) => {
  const { detail, handleCloseDetail,showEditReserve } = props;
  const [Dog, setDog] = useState<Dog>();
  const [showModal, setshowModal] = useState(false);
  const [ModalPaseador, setModalPaseador] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const queryClient = useQueryClient();

  // const [paseo, set] = useState<Dog>()
  console.log('details', detail);
  const cancel = () => {
    setOpen(false);
  };
  const [showEdit, setshowEdit] = useState('');
  const handleEditPaseo = (paseo: Dog) => {
    console.log('paseo', paseo);

    setDog(paseo);

  };
  const confirm =  async (id:string) => {
    console.log('id',id);
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/reserves-hps/${id}`,{
        method:'DELETE',
        headers: {
          'Content-Type': 'application/json',
          // Authorization: `Bearer ${token.jwt}`,
        },

      })
      console.log('response',response);
        queryClient.invalidateQueries('reserves')
      handleCloseDetail()
      setOpen(false)

    } catch (error) {
      console.log('error',error); 
    }
  }

  const [selectLoading, setSelectLoading] = useState(false);

  
  const handleStatusChange = async (event: React.ChangeEvent<HTMLSelectElement>) => {
    try {
      setSelectLoading(true); // Set loading state to true

      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/reserves-hps/${detail.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          // Authorization: `Bearer ${token.jwt}`,
        },
        body: JSON.stringify({
          status: event.target.value,
        }),
      });

      console.log('response', response);

      toast.success('El estado de la reserva fue editado con éxito', {
        position: 'bottom-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });

      queryClient.invalidateQueries('reserves')

    } catch (error) {
      console.log('error', error);

      toast.error('Ocurrio un error al editar la reserva', {
        position: 'bottom-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });


    } finally {
      setSelectLoading(false); // Set loading state to false after the call is complete
    }
  }
  
  const statusOptions = ['Consulta', 'Anticipo Pagado', 'A retirar', 'En House Paradise', 'Finalizado']

    function formatPrice(price: number): string {
        const formatter = new Intl.NumberFormat('es-AR', {
            style: 'currency',
            currency: 'ARS',
            minimumFractionDigits: 0,
        });
        return formatter.format(price);
    }
    const [totalAmount, setTotalAmount] = useState(null);

    const handleTotalPayed = async (paymentId : string) => {
        try {
        const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/hp-payments?[payment_id]=${paymentId}`);
        
        if (!response.ok) {
            throw new Error(`Error fetching data. Status: ${response.status}`);
        }

        const data = await response.json();

        console.log(data[0])

        // Assuming the response is an array of payments
        // You might need to adapt this based on the actual structure of your data
        setTotalAmount(data[0].total_amount)

        // return totalAmountPaid;
        } catch (error) {
        console.error('Error fetching total amount paid:', error);
        throw error; // Propagate the error for the calling code to handle
        }
    };


    useEffect(() => {
        detail.payment_id && (
            handleTotalPayed(detail.payment_id)
        )
        }, []); 
        

  return (
    <div className='px-5 py-8 sm:p-10 w-full sm:w-[90%] h-screen overflow-y-auto  border-white-primary hp-reserve-details bg-black-secondary font-laussane z-[99 sm:pb-20 pb-52'>
      <div className={` ${Dog && (showModal || ModalPaseador) && 'opacity-10'}`} >

        {/* Header */}
        <div className='flex justify-between fixed w-full sm:w-[90%] z-[99] right-0 top-0 sm:px-10 sm:py-5 p-5 bg-black-secondary'>
            <div className="flex items-center gap-2 cursor-pointer" onClick={handleCloseDetail}>
                <Image
                src={require(`../public/content/arrow-left.svg`)}
                height={14}
                width={20}
                alt='arrow icon'
                    />  
                <p >Volver al listado</p>
            </div>
          
          <Image
            src={Close}
            alt='uGo admin logo'
            width={18}
            height={18}
            onClick={handleCloseDetail}
            className='hover:cursor-pointer'
          />
        </div>

        {/* End Header */}


        <div className="flex flex-col-reverse sm:flex-row items-start flex-1 justify-between relative mt-10 gap-10">
            <div className="w-full sm:w-3/5">
                <p className='text-3xl font-normal font-faro'>Reserva de<span className='text-pink-primary'> {detail.dog_name}</span></p>
                <div className="flex items-center mt-2 gap-3">
                    <p className='uppercase py-1 px-2 rounded-sm text-black-primary bg-white text-xs font-medium'>Dueño: {detail.owner_name} {detail.owner_surname}</p>

                    <p className='uppercase py-1 px-2 rounded-sm text-black-primary bg-white text-xs font-medium'>
                        Total:{' '}{moment(detail.aob_date_end).diff(detail.aob_date_start, 'days')}{' '}días
                    </p>

                    <p className='uppercase py-1 px-2 rounded-sm text-black-primary bg-white text-xs font-medium'>{detail.status}</p>
                    <p className='uppercase py-1 px-2 rounded-sm text-black-primary bg-white text-xs font-medium'>{formatPrice(detail.aob_price)}</p>
                </div>

                            
          

                <div className={` ${Dog && (showModal || ModalPaseador) && 'opacity-10'}`}>
                    <CardParadise width="full">
                        <p className='text-2xl font-light font-faro'>Sobre <span className="text-pink-primary">{detail.dog_name}</span></p>

                        <div className="flex justify-between items-center border-y-[1px] border-white-primary my-5 py-5 gap-5">
                            <p className='text-2xl'>{detail.dog_age} {' '} años</p>
                            <p className='text-2xl capitalize'>{detail.dog_raza}</p>
                            <p className='text-2xl capitalize'>{detail.dog_genre}</p>
                            <p className='text-2xl capitalize'>{detail.dog_castrado ? '' : 'No'} está castrado</p>
                        </div>


                        <div className="flex sm:flex-row flex-col gap-10">

                            <div className="w-full sm:w-1/2">
                                {detail.dog_behaviour && (
                                <p className='py-4 border-b-[1px] flex justify-between capitalize'>
                                    Comportamiento: <span>{detail.dog_behaviour}</span>
                                </p>
                                )}

                                {detail.dog_deworming && (
                                <p className='py-4 border-b-[1px] flex justify-between capitalize'>
                                    Desparasitado: <span>{detail.dog_deworming ? 'Si' : 'No'}</span>
                                </p>
                                )}

                                {detail.dog_social && (
                                <p className='py-4 border-b-[1px] flex justify-between capitalize'>
                                    Sociabilización: <span>{detail.dog_social}</span>
                                </p>
                                )}

                                <p className='py-4 flex justify-between capitalize'>
                                    <span className='flex items-center'>
                                    <Image
                                        src={Vaccine}
                                        alt='vaccine logo'
                                        // width={31}
                                        // height={32}
                                        className='pr2 w-15'
                                    />
                                    Vacuna:
                                    </span>
                                    <span>{detail.dog_vaccine ? 'Si' : 'No'}</span>
                                </p>
                            </div>

                            <div className="w-full sm:w-1/2">
                                {detail.dog_food && (
                                <p className='py-4 border-b-[1px] flex justify-between capitalize'>
                                    Comida: <span>{detail.dog_food}</span>
                                </p>
                                )}

                                {detail.dog_bite && (
                                <p className='py-4 border-b-[1px] flex justify-between capitalize'>
                                    Mordió a otros perros: <span>{detail.dog_bite}</span>
                                    </p>
                                )}

                                {detail.dog_swim && (
                                <p className='py-4 border-b-[1px] flex justify-between capitalize'>
                                    Sabe nadar: <span>{detail.dog_swim}</span>
                                    </p>
                                )}

                                {!detail.dog_castrado && (
                                <p className='py-4 border-b-[1px] flex justify-between capitalize'>
                                    Última fecha de celo:{detail.date_celo}
                                    <span>
                                    {detail.date_celo === '2022-12-18'
                                        ? 'Está castrado'
                                        : detail.date_celo}
                                    </span>
                                </p>
                                )}
                            </div>
                        </div>
                    </CardParadise>

                    {detail.dog_comments && (
                        <CardParadise width="full">
                        
                            <p className='flex justify-between'>
                                Comentarios: <span className="w-80">{detail.dog_comments}</span>
                            </p>
                    
                        </CardParadise>    
                     )}
                </div>

                <div className="flex flex-col sm:flex-row gap-10">
                    <CardParadise width="1/2">
                        <p className='text-2xl font-light font-faro'>Datos de <span className="text-pink-primary">{detail.owner_name} {detail.owner_surname}</span></p>

                        <p className='py-4 border-b-[1px] flex justify-between'>
                            Email:<span>{detail.owner_email}</span>
                        </p>

                        <p className='py-4 border-b-[1px] flex justify-between'>
                            DNI: <span>{detail.owner_dni}</span>
                        </p>

                        <p className='py-4 flex justify-between'>Telefono: <span>{detail.owner_phone}</span>
                        </p>
                    </CardParadise>

                
                    <CardParadise width="1/2">
                        <p className='text-2xl font-light font-faro'>Desgloce de precio</p>
                        {detail.precio_noche && (
                            <p className='py-4 border-b-[1px] flex justify-between'>
                                Precio por noche:<span>{formatPrice(detail.precio_noche)}</span>
                            </p>
                        )}
                        {detail.tarifa_traslado && (
                        <p className='py-4 border-b-[1px] flex justify-between'>
                            Traifa de transporte:<span>{formatPrice(detail.tarifa_traslado)}</span>
                        </p>
                        )}
                        {detail.discount_cupon && !detail.discount_cupon.includes('undefined') && (
                            <p className='py-4 flex justify-between'>
                                <span className="flex items-center">Cupón utilizado: <span className="ml-5 uppercase p-2 rounded-md text-black-primary bg-pink-primary text-xs font-bold">{detail.discount_cupon}</span>
                                </span>
                                <span>-
                                {new Intl.NumberFormat('es-AR', {
                                    style: 'currency',
                                    currency: 'ARS',
                                    maximumSignificantDigits: 3,
                                }).format(detail.discount_amount)
                                }      
                                </span>
                            </p>
                        )}

                
                            <p className='py-4 border-b-[1px] flex justify-between text-pink-primary font-semibold'>
                                Total por {moment(detail.aob_date_end).diff(detail.aob_date_start, 'days')} días:<span>{formatPrice(detail.aob_price)}</span>
                            </p>
                      
                        
                    </CardParadise>
                </div>

            </div>

            <div className="sm:sticky sm:top-[50px] h-max sm:z-[999] sm:w-2/5 w-full">
                <div className='flex items-stretch sm:items-end sm:justify-end center flex-row '>
                    <div className='flex flex-row items-center gap-3 w-full sm:w-max'>
                        <button 
                            onClick={()=>showEditReserve(detail.id)}
                            className='border-[1px] border-white-primary rounded-lg bg-gray-secondary  py-4 px-4 w-1/2 sm:w-max'>
                            <p>Editar Reserva</p>
                        </button>
                        
                        <Popconfirm
                        placement='bottom'
                        onConfirm={() => confirm(detail.id)}
                        title='Estás seguro que querés borrar esta reserva?'
                        description='Se eliminara la reserva'
                        okText='Eliminar'
                        cancelText='Cancelar'
                        className='cursor-pointer'
                        okButtonProps={{
                        loading: confirmLoading,
                        danger: true,
                        className:  'bg-primaryRed text-white rounded-md px-2 py-1 hover:bg-primaryRed',
                        }}
                        >
                            <div className='border-[1px] border-red-primary bg-red-primary rounded-lg py-4 px-4  w-1/2 sm:w-max'>
                            <p className='text-black text-center'>Borrar Reserva</p>
                            </div>
                        </Popconfirm>
                    </div>
                </div>

                <CardParadise width="full">
                    <p className='py-4 border-b-[1px] flex flex-column'>
                        <span className="text-xs mb-2">Estado de reserva:</span>
                        <div className="flex items-center gap-2 justify-between">
                            <select className="input-reset appearance-none bg-transparent border-none text-pink-primary text-xl flex-1" onChange={(event) => handleStatusChange(event)} disabled={selectLoading}>
                                <option className="white bg-black-secondary input-reset">{detail.status}</option>

                                {statusOptions.map((s) => (
                                    s !== detail.status &&
                                    <option className="white bg-black-secondary input-reset" key={s} value={s}>
                                        {s}
                                    </option>
                                ))}
                            </select>

                            <Image
                                src={require(`../public/content/arrow-simple-down.svg`)}
                                height={10}
                                width={10}
                                alt='arrow icon'
                            />
                        </div>
                    </p>

                    <div className="flex gap-20 items-center border-b-[1px]">
                        <p className='py-4 flex flex-column justify-between'>
                            <span className="text-xs mb-2">Desde el {moment(detail.aob_date_start).format('dddd')}</span>
                            <span className="font-faro text-5xl font-medium">{moment(detail.aob_date_start).format('DD.MM')}</span>
                        </p>
                        <p className='py-4 flex flex-column justify-between'>
                        <span className="text-xs mb-2">Hasta el {moment(detail.aob_date_end).format('dddd')}</span>
                        <span className="font-faro text-5xl font-medium">{moment(detail.aob_date_end).format('DD.MM')}</span>
                        <span className="text-xs mb-2"></span>
                        </p>
                    </div>

                    <p className='py-4 border-b-[1px] flex flex-column'>
                        <span className="text-xs mb-2 font-laussane">A retirar por:</span>
                        <span className="text-xl capitalize">{detail.owner_address}, {detail.provincia}</span>
                    </p>

                    
                    <p className='py-4 flex flex-column'>
                        Precio total por: {moment(detail.aob_date_end).diff(detail.aob_date_start, 'days')} días
                        <span className="font-faro text-2xl">
                        {new Intl.NumberFormat('es-AR', {
                            style: 'currency',
                            currency: 'ARS',
                            maximumSignificantDigits: 3,
                        }).format(detail.aob_price)}
                        </span>
                        
                        {detail.payment_id && (
                            <p className="mt-3">Total pagado: <span>{totalAmount}</span></p> 
                        )}
                    </p>
                   
                </CardParadise>

                
            </div>
        </div>

      </div>

     
    

      <ToastContainer
        position='bottom-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />

    </div>
  );
};

export default ReserveDetail;
