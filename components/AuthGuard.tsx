import { FunctionComponent } from 'react'
import { useSession } from 'next-auth/react'

import AccessDenied from './AccessDenied'
import Layout from './Layout'

const AuthGuard: FunctionComponent = ({ children }) => {
  const { data: session, status } = useSession()
  const loading = status === 'loading'

  if (loading) {
    return <p>Loading…</p>
  }

  if (!loading && !session) {
    return <AccessDenied />
  }

  return <Layout>{children}</Layout>
}

export default AuthGuard
