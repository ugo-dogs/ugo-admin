import {
    FunctionComponent,
    ReactChild,
    ReactFragment,
    ReactPortal,
    SetStateAction,
    useState,
  } from 'react';
  

  import { ToastContainer, toast } from 'react-toastify';
  import 'react-toastify/dist/ReactToastify.css';
  import Image from 'next/image';

  import { Barrios, Walker, Dog } from '../lib/types';

  import type { GetServerSideProps } from 'next';
  import MagnifyingGlass from '../public/magnifying-glass.svg';

  import { MdKeyboardArrowLeft } from 'react-icons/md';
  
  import "react-datepicker/dist/react-datepicker.css";
import FlatlistPaseadores from './FlatlistPaseadores';
import { QueryFunctionContext, useQuery } from 'react-query';
  
 
  const { NEXT_PUBLIC_API_URL } = process.env;
  const getWalkers = async (
    key?: QueryFunctionContext<any, any>
  ): Promise<Array<Walker>> => {
    const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/users`);
  
    apiUrl.searchParams.append('paseador', 'true');
    apiUrl.searchParams.append('verified', 'true');
  
    let filters: Array<(paseador: Walker) => boolean> = [];
  
    const name = key?.queryKey[1].name;
    if (name) {
      const regex = new RegExp(name, 'i');
      filters.push(function (paseador: Walker) {
        return regex.test(paseador.first_name) || regex.test(paseador.last_name);
      });
    }

  
    const res = await fetch(apiUrl.href);
    const data = await res.json();
  
    if (filters.length > 0) {
      return data.filter((paseador: Walker) =>
        filters.every((f) => f(paseador))
      );
    }
  
    return data;
  };
  export const getServerSideProps: GetServerSideProps = async () => {
    try {
      const walkers = await getWalkers();

  console.log('ssssaaa',walkers);
  
      return {
        props: {
          walkers: walkers,
   
        },
      };
    } catch (e) {
      console.log(e);
      return {
        props: {
          walks: [],
          feed:[]
        },
      };
    }
  };
  type Props = {
   
    walkers: Array<Walker>;
    walkerPreSelected:Dog;
    handleModalClose: () => void
    onSelectPaseador: (walker:object) => void
};
  const ModalChosePaseador: FunctionComponent<Props> = ({ walkers,handleModalClose,onSelectPaseador,walkerPreSelected }) => {
const [name, setName] = useState('')
    const { data, isSuccess, isLoading } = useQuery(
        ['walks', { name }, ],
        getWalkers,
        {
          initialData: walkers,
          retry: false,
        }
      );
    console.log('data',walkerPreSelected);
    
      
      const handleSidebar = async (id: string) => {
   
    
        // setActiveWalker(id);
        // setShowSidebar(!showSidebar);
        try {
          const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/resenas`);
    
          apiUrl.searchParams.append('paseador._id', id);
    
          const res = await fetch(apiUrl.href);
    
          const reviews = await res.json();
        //   setReviews(reviews);
        } catch (e) {
          console.log(e);
        //   setReviews([]);
        }
      };
    return (
      <div
        className='fixed inset-0 overflow-y-auto z-50'
        aria-labelledby='modal-title'
        role='dialog'
        aria-modal='true'
      >
        <div className='flex items-end justify-center h-5/6	  text-center sm:block sm:p-0  z-50'>
          {/* <div
            className='fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity'
            aria-hidden='true'
          ></div> */}
  
          <span
            className='hidden sm:inline-block sm:align-middle sm:h-screen '
            aria-hidden='true'
          >
            &#8203;
          </span>
  
          <div className=' h-full	 relative  py-8 inline-block align-bottom bg-gray-primary border-pink-primary  border-[1px] rounded-lg text-left shadow-xl transform transition-all sm:my-10 sm:align-middle sm:max-w-7xl   sm:w-full'>
            <div
              onClick={() => handleModalClose()}
              className=' absolute left-[40px] top-[15px] z-100 flex items-center justify-center w-[40px] h-[40px] rounded-full hover:cursor-pointer'
            >
              {/* <p className='font-medium text-xl'>X</p> */}
              {/* <Image
                src={Close}
                alt='uGo admin logo'
                width={18}
                height={18}
                className='hover:cursor-pointer'
              /> */}
              <MdKeyboardArrowLeft className='hover:cursor-pointer' size={200}  />
            </div>
            <div className='bg-white  px-16 flex-row flex justify-between mt-10'>
             
       
            </div>
            <div className=' mx-16   relative '>
            <div>
            <h1 className='text-lg  font-bold opacity-60 '>
            Paseo de {walkerPreSelected.dog.name} {'>'} Editar paseador
            
              </h1>
              <h1 className='text-3xl  font-bold my-5 '>Seleccionar nuevo paseador para {walkerPreSelected.dog.name}</h1>

                </div>

                <div className='flex flex-row items-center  px-3  bg-gray-secondary py-3 lg:py-5  md:py-4 placeholder-white-primary border-[1px] border-white-primary rounded-lg focus:outline-none w-[40%]'>
            <Image
              src={MagnifyingGlass}
              alt='magnifying glass'
              width={18}
              height={18}
            />
    
          <input
            type='text'
            placeholder='Buscar por nombre'
            className='bg-gray-secondary focus:outline-none pl-4'
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
            </div>
          <div className=' mx-16 '>
          {data &&  <FlatlistPaseadores data={data} handleSidebar={(id:string)=>handleSidebar(id)} initialValue={walkerPreSelected} onSelectPaseador={onSelectPaseador}/>}

          </div>
          </div>
        </div>
        <ToastContainer
          position='bottom-right'
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>
    );
  };
  
  export default ModalChosePaseador;
  