import { FunctionComponent, useState } from 'react'
import Image from 'next/image'

import type { Barrios, Dog, FinalResult,Walk } from '../lib/types'
import { turnFormatter } from '../lib/utils/turnFormatter'
import Close from '../public/close-white.svg'
import ModalPaseoEdit from './ModalPaseoEdit'
import ModalChosePaseador from './ModalChosePaseador'

type Props = {
  detail: FinalResult
  handleCloseDetail: () => void
  barrios:Array<Barrios>
}

const WalkDetail: FunctionComponent<Props> = props => {
  const { detail, handleCloseDetail,barrios } = props
const [Dog, setDog] = useState<Dog>()
const [showModal, setshowModal] = useState(false)
const [ModalPaseador, setModalPaseador] = useState(false)
// const [paseo, set] = useState<Dog>()
console.log('details',detail);
const [showEdit, setshowEdit] = useState('')
const handleEditPaseo = (paseo:Dog)=>{
  
  setDog(paseo)  

}
  return (
<div className="p-10 w-[400px] h-screen overflow-y-auto border-l-[3px] border-white-primary">
  <div className={` ${Dog && (showModal || ModalPaseador) && 'opacity-10'}`}>
      <div className="flex justify-between ">
        <p className="text-sm">Fecha: {detail.date}</p>
        <Image
          src={Close}
          alt="uGo admin logo"
          width={18}
          height={18}
          onClick={handleCloseDetail}
          className="hover:cursor-pointer"
        />
      </div>
      <p className="text-3xl font-bold mt-10">
        Paseo de<span className="text-pink-primary"> {detail.name}</span>
      </p>
      <p className="text-sm mt-2">ID Paseador: {detail.phone}</p>
      <div className="border-[1px] border-white-primary rounded-lg mt-6">
        <p className="p-4 border-b-[1px] flex justify-between">
          Cantidad de perros: <span>{detail.dogs.length}</span>
        </p>
        <p className="p-4 border-b-[1px] flex justify-between">
          Zona: <span>{detail.zone}</span>
        </p>
        <p className="p-4 flex justify-between">
          Turno: <span>{turnFormatter(detail.end)}</span>
        </p>
      </div>
      <p className="mt-8 text-2xl font-bold">Perros del paseo</p>
      {detail.dogs.map((paseo:Dog, idx) => (
        <div
          key={idx}
          className="mt-6 bg-black-primary border-[1px] border-pink-primary rounded-lg pt-7 pb-7 pr-3 pl-12 relative"
        >
     <div className='absolute top-0 right-3 cursor-pointer' onClick={()=>{idx.toString() == showEdit ? setshowEdit(''): setshowEdit(idx.toString())}}><span className='text-[25px] font-bold' >...</span>
     
     </div>
      {idx.toString() == showEdit && (

     <div className=' bg-white-primary  px-4 py-2 rounded-sm absolute right-[-20px] mt-4 cursor-pointer' onClick={()=>{handleEditPaseo(paseo);setshowModal(true);setshowEdit('')}}>
       <div   className='bg-white-primary absolute h-4 w-4  rotate-45 transform origin-bottom-left top-[-15px] left-7'></div>
       <p className='text-black-primary'>Editar</p>
       </div>
            )}
   
     
     
          <div className="flex flex-col">
            <span className="font-bold">{paseo.dog.name}</span>
            <span className="font-bold text-[8px] mb-4">
              {paseo.dog.raza.name},{' '}
              {paseo.dog.age === 1 ? ' 1 año' : `${paseo.dog.age} años`}
            </span>
            <span className="font-bold mb-1 text-[11px]">{`${paseo.start.slice(
              0,
              5,
            )} - ${paseo.end.slice(0, 5)}`}</span>
            <span className="font-bold mb-1 text-[11px]">
              {paseo.direccion.label} , {paseo.direccion.desc}
            </span>
            <span className="font-bold mb-4 text-[11px]">
              {paseo.direccion.barrio.barrio}
            </span>
            <span className="font-bold text-[11px]">
              Dueño: {paseo.user.first_name +' '+ paseo.user.last_name}
            </span>
          </div>
          <div className="inline-block rounded-full border-2 border-white-primary w-[70px] h-[70px] shadow absolute top-8 -left-8">
            <Image
              src={paseo.dog.avatar.formats.thumbnail.url}
              alt={paseo.dog.name}
              width={70}
              height={70}
              className="rounded-full"
            />
          </div>
        </div>
      ))}
      </div>
      {Dog && showModal && <ModalPaseoEdit dog={Dog} barrios={barrios} openPaseadores={()=>{setModalPaseador(true);setshowModal(false)}}  handleModalClose={()=>setshowModal(false)} />}
     {ModalPaseador && Dog && <ModalChosePaseador handleModalClose={() => { setModalPaseador(false); setshowModal(true) } } walkerPreSelected={Dog} onSelectPaseador={(walker:object) => {setDog({ ...Dog,paseador:walker });setModalPaseador(false); setshowModal(true)} } walkers={[]}  />}
    </div>
  )
}

export default WalkDetail


