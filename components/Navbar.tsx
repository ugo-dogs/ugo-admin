import { FunctionComponent } from 'react';
import { signOut } from 'next-auth/react';

import { useRouter } from 'next/router';
import Link from 'next/link';
import Image from 'next/image';

import logo from '../public/logo.svg';

import dog from '../public/nav/dog.svg';
import dogBlack from '../public/nav/dog-black.svg';
import bill from '../public/nav/bill.svg';
import billBlack from '../public/nav/bill-black.svg';
import dogStrap from '../public/nav/dogStrap.svg';
import dogStrapBlack from '../public/nav/dogStrap-black.svg';
import { AiOutlineCalendar } from 'react-icons/ai';


const Navbar: FunctionComponent = () => {
  const router = useRouter();

  const isActive = (href: string): string =>
    router.pathname === href ? 'underline text-black-primary' : '';

  const isActiveWalks = isActive('/walks');
  const isActivePayments = isActive('/payments');
  const isActiveWalkers = isActive('/walkers');
  const isActiveOwners = isActive('/owners');
  const isActiveClaims = isActive('/claims');
  const isActiveReserve = isActive('/houseParadise');
  const isActiveDashboard = isActive('/dashboard');

  return (
    <nav className='sm:!h-screen bg-pink-primary sm:!pl-4 sm:!pr-4 sm:!pt-11 sm:!sticky sm:top-0 bottom-0 fixed z-50 nav-container'>
      <div className='text-center'>
        <Link legacyBehavior href='/'>
          <a className="sm:flex hidden">
            <Image src={logo} alt='uGo admin logo' width={100} height={33} />
          </a>
        </Link>
      </div>
      <ul className='sm:mt-16 sm: space-y-5 flex sm:!flex-col flex-row nav-main p-5 sm:p-0'>
      <li>
          <Link legacyBehavior href='/houseParadise'>
            <a>
              <Image
                src={isActiveReserve ? dogStrapBlack : dogStrap}
                alt='dog'
                width={15}
                height={15}
              />{' '}
              <span className={isActiveReserve}>Reservas</span>
            </a>
          </Link>
        </li>

        <li>
          <Link legacyBehavior href='/dashboard'>
            <a>
              <Image
                src={isActiveReserve ? dogStrapBlack : dogStrap}
                alt='dog'
                width={15}
                height={15}
              />{' '}
              <span className={isActiveDashboard}>DashBoard</span>
            </a>
          </Link>
        </li>

        <li>
          <Link legacyBehavior href='/walks'>
            <a>
              <Image
                src={isActiveWalks ? dogBlack : dog}
                alt='dog'
                width={15}
                height={15}
              />{' '}
              <span className={isActiveWalks}>Paseos</span>
            </a>
          </Link>
        </li>
        <li>
          <Link legacyBehavior href='/payments'>
            <a>
              <Image
                src={isActivePayments ? billBlack : bill}
                alt='dog'
                width={15}
                height={15}
              />{' '}
              <span className={isActivePayments}>Pagos</span>
            </a>
          </Link>
        </li>
        <li>
          <Link legacyBehavior href='/walkers'>
            <a>
              <Image
                src={isActiveWalkers ? dogStrapBlack : dogStrap}
                alt='dog'
                width={15}
                height={15}
              />{' '}
              <span className={isActiveWalkers}>Paseadores</span>
            </a>
          </Link>
        </li>
        <li>
          <Link legacyBehavior href='/owners'>
            <a>
              <Image
                src={isActiveOwners ? dogStrapBlack : dogStrap}
                alt='dog'
                width={15}
                height={15}
              />{' '}
              <span className={isActiveOwners}>Dueños</span>
            </a>
          </Link>
        </li>
        <li>
          <Link legacyBehavior href='/claims'>
            <a>
              <Image
                src={isActiveClaims ? dogStrapBlack : dogStrap}
                alt='dog'
                width={15}
                height={15}
              />{' '}
              <span className={isActiveClaims}>Reclamos</span>
            </a>
          </Link>
        </li>
        
      </ul>
      <div className='text-xs bottom-0 text-black-primary mb-20 absolute hidden sm:db'>
        <p className='mb-5'>v1.0</p>
        <p className='mb-5'>Configuración</p>
        <Link legacyBehavior href='/api/auth/signout'>
          <a
            onClick={(e) => {
              e.preventDefault();
              signOut();
            }}
          >
            Cerrar sesión
          </a>
        </Link>
      </div>
    </nav>
  );
};

export default Navbar;
