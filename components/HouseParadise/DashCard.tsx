import React from 'react'

interface DaschardProps {
    dataDaschard: any;
    setdataDaschard: (data: any) => void;
    title: string;
    comparison: string;
}

const Dashcard: React.FC<DaschardProps> = ({ dataDaschard, setdataDaschard, title, comparison, children }) => {
    return (
        <div className="bg-gray-secondary rounded-lg p-5 flex flex-col justify-between w-full sm:w-1/4">
            <div className="flex justify-between">
             <h2 className="text-xl lh-solid  font-medium text-white w-1/2">{title}</h2>
             <p className="text-md text-aob-green ">{comparison}</p>
            </div>
           
            {children}
        </div>
    );
};

export default Dashcard;
