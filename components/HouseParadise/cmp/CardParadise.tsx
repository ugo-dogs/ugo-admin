import React, {Children} from 'react';
import Image from 'next/image';

interface CardParadiseProps {
    width: string;
    // title: string;
    // status: string;
    // phone: string;
    // avatar: React.ReactNode;
}

const CardParadise: React.FC<CardParadiseProps> = ({children, width}) => {

    return (
        <div className={`bg-black-light rounded-lg mt-6 reserve-details-inner justify-between p-8 sm:w-${width}`}>
           {children}
        </div>
    )
}

export default CardParadise;
