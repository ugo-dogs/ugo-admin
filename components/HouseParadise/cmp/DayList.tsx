import React from 'react';
import Image from 'next/image';

interface DayListProps {
    title: string;
    status: string;
    phone: string;
    avatar: any
}

const DayList: React.FC<DayListProps> = ({ title, status, phone, avatar }) => {
    return (
        <div className="flex justify-between items-center border-b-[1px] border-b-white-primary pb-5 hover:scale-[0.97]">
            <div className="flex items-center gap-2 w-1/4">
                <div className="rounded-full border-[1px] border-white-primary w-14 h-14 flex items-center justify-center overflow-hidden bg-cover" style={{ backgroundImage: `url(${avatar})` }}></div>
                <h2 className="text-base">{title}</h2>
            </div>

            <div className="flex items-center gap-10">
                <p className="p-3 rounded-md bg-pink-primary text-black-primary uppercase text-xs">{status} ↓</p>
                <a target="_blank"  rel="noopener noreferrer" href={`https://api.whatsapp.com/send?phone=${phone}`}>
                    <Image
                        src={require(`../../../public/content/phone.svg`)}
                        height={30}
                        width={30}
                        
                        alt='menu icon'
                    />
                </a>
            </div>
         
        </div>
    )
}

export default DayList;
