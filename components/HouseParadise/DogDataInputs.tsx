import {
  Checkbox,
  FormGroup,
  FormControlLabel,
  RadioGroup,
  Radio,
} from '@mui/material';

interface DogDataInputsProps {
  inputs?: {
    type: string;
    value: string;
    placeholder: string;
    section: string;
    selectOptions?: {
      value: string;
      title: string;
    }[];
  }[];
  social?: {
    desc: string;
    title: string;
  }[];
  healthDogs?: {
    question: string;
  }[];
  social_comportamiento?: {
    id: string;
    questions: string;
    options: {
      title: string;
      value: string;
      id: string;
    }[];
  }[];
  dataDog: any;
  setDataDog: (data: any) => void;
}

const DogDataInputs: React.FC<DogDataInputsProps> = ({
  inputs,
  social,
  healthDogs,
  social_comportamiento,
  dataDog,
  setDataDog,
}) => {
console.log('healthDogs',healthDogs);

  return (
    <div className="pop-field-container">
      {inputs?.map((field, i) => (
        <div className='relative mt-5 mr-5 pop-field' key={i}>
          <p className='absolute border-white-primary border-[1px] p-1 rounded-sm left-4 z-50 bg-gray-secondary capitalize px-2 font-laussane'>
            {field.placeholder}
          </p>
          {field.type === 'select' ? (
            <select
            defaultValue={'Seleccionar'}
              className='appearance-none cursor-pointer mt-4 pl-10 rounded-lg focus:border-pink-primary py-6 bg-gray-secondary focus:outline-none border-[1px] border-gray-secondary capitalize w-60 outline-none font-laussane'
              value={dataDog[field.placeholder]}
              onChange={(e) => {
                setDataDog({
                  ...dataDog,
                  [field.placeholder]: e.target.value,
                });
              }}
            >
              {[{title:'Seleccionar',value:''}].concat(field.selectOptions || []).map((option,i) => (
                <option key={option.title} value={option.value} disabled={i ==0}>
                  {option.title}
                </option>
              ))}
            </select>
          ) : (
            <input
              type={field.type}
              value={dataDog[field.placeholder]}
              placeholder={field.placeholder}
              onChange={(e) => {
                setDataDog({
                  ...dataDog,
                  [field.placeholder]: e.target.value,
                });
              }}
              className='mt-4 pl-10 rounded-lg focus:border-pink-primary py-6 bg-gray-secondary focus:outline-none border-[1px] border-gray-secondary capitalize'
            />
          )}
        </div>
      ))}
      <div className='relative mt-5 mr-5 pop-field'>
        <p className='absolute border-white-primary border-[1px] p-1 rounded-sm left-4 z-50 bg-gray-secondary capitalize px-2'>
          Que tan sociable es?
        </p>
        <select
          value={dataDog.social}
          onChange={(e) => {
            setDataDog({
              ...dataDog,
              social: e.target.value,
            });
          }}
          className='appearance-none cursor-pointer mt-4 pl-10 rounded-lg focus:border-pink-primary py-6 bg-gray-secondary focus:outline-none border-[1px] border-gray-secondary capitalize w-60 outline-none'
        >
          {[{desc:'Seleccionar',title:''}].concat(social || []).map((option, i) => (
            <option key={i} value={option.desc} disabled={i == 0}>
              {option.desc}
            </option>
          ))}
        </select>
      </div>
      <div>
        <div className='relative mt-5 mr-5 pop-field'>
          <p>Sobre su salud</p>
          {healthDogs?.map((field, i) => (
            <FormGroup key={i}>
              <FormControlLabel
                control={<Checkbox  />}
                name={
                  field.question.includes('vacunado')
                    ? 'dog_vaccine'
                    : 'dog_deworming'
                }
                checked={dataDog[field.question]}
                value={dataDog[field.question]}
                onChange={(e: any) => {
                  setDataDog({
                    ...dataDog,
                    [e.target.name]: e.target!.checked,
                  });
                }}
                label={field.question}
              />
            </FormGroup>
          ))}
        </div>
      </div>
      <div>
        <div className='relative mt-5 mr-5 pop-field'>
          {social_comportamiento?.map((field, i) => (
            <div key={i} className='pt-3'>
              <p>{field.questions}</p>
              <div>
                {/* checkbox input */}
                <select
                defaultValue={'Seleccionar'}
                value={dataDog[field.questions]}
                  //  value={field.questions}
                  onChange={(e) => {
                    console.log({ [field.questions]: e.target.value });
                    setDataDog({
                      ...dataDog,
                      [field.questions]: e.target.value,
                    });
                  }}
                  className='appearance-none cursor-pointer mt-4 pl-10 rounded-lg focus:border-pink-primary py-6 bg-gray-secondary focus:outline-none border-[1px] border-gray-secondary capitalize w-60 outline-none'
                >
                  {[{title:'Seleccionar'}].concat(field.options).map((option, i) => (
                    <option
                    defaultValue={''}
                  disabled={i==0}
                      key={i}
                      value={option.title}
                      selected={dataDog[field.questions] == option.title}
                    >
                      {option.title} 
                    </option>
                  ))}
                </select>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
export default DogDataInputs;
