import React from 'react';
import DayList from './cmp/DayList';

interface DaysResumeProps {
  title: string;
  comparison: string;
  isEntering: boolean;
  data: any;
}



const DaysResume: React.FC<DaysResumeProps> = ({ title, comparison, isEntering, data }) => {

    const defaultAvatar = 'https://ugo.com.ar/wp-content/uploads/2022/09/Rectangle-873-scaled.jpg';
  // Check if data is an array and not empty
  if (!Array.isArray(data) || data.length === 0) {
    return (
      <div className="bg-gray-secondary rounded-lg p-10 flex flex-col justify-between sm:w-2/4" data-is-entering={isEntering}>
        <h2 className="text-xl lh-solid font-medium text-white w-1/2">{title}</h2>
        <p className="text-md text-aob-green ">{comparison}</p>
        <div>No data available</div>
      </div>
    );
  }

  return (
    <div className="bg-gray-secondary rounded-lg p-10 flex flex-col justify-between w-2/4" data-is-entering={isEntering}>
      <h2 className="text-xl lh-solid font-medium text-white w-1/2">{title}</h2>
      <p className="text-md text-aob-green ">{comparison}</p>
      <div className="flex flex-col gap-5 mt-10">
        {data.map((reserve: any, i: number) => (
          <DayList key={i} avatar={reserve.dog.avatar ? reserve.dog.avatar.url : defaultAvatar } title={reserve.dog_name ? reserve.dog_name : reserve.dog.name} status={reserve.status ? reserve.status : 'Manual'} phone={reserve.owner_phone} />
        ))}
      </div>
    </div>
  );
};

export default DaysResume;
