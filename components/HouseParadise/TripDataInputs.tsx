import DatePicker, { registerLocale } from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import 'moment/locale/es';
import { useState, useEffect } from 'react';
import { AiOutlineCalendar } from 'react-icons/ai';
import es from 'date-fns/locale/es';
import { calculatePrice } from '../../lib/utils/HouseParadise';
registerLocale('es', es);
interface TripDatainputsProps {
  inputs?: {
    type: string;
    value: string;
    placeholder: string;
    section: string;
    selectOptions?: {
      value: string;
      title: string;
    }[];
  }[];
  social?: {
    desc: string;
    title: string;
  }[];
  healthDogs?: {
    question: string;
  }[];
  social_comportamiento?: {
    id: string;
    questions: string;
    options: {
      title: string;
      value: string;
      id: string;
    }[];
  }[];
  dataTrip: any;
  setdataTrip: (data: any) => void;
}

const TripDataInputs: React.FC<TripDatainputsProps> = ({
  inputs,
  dataTrip,
  setdataTrip,
}) => {
    
  const [startDate, setStartDate] = useState<Date>();
  const [endDate, setEndDate] = useState<Date>();
  const [price, setPrice] = useState<number>(0);

  const filterDates = (date: { getDay: () => any }) => {
    const day = date.getDay();
    return day === 1 || day === 3 || day === 5;
  };
 
  console.log('endDate setup',endDate);

  useEffect(() => {
   setdataTrip({
      ...dataTrip,
      aob_date_start: dataTrip.aob_date_start ? moment(dataTrip.aob_date_start).toDate() :moment().toDate(),
      aob_date_end:  dataTrip.aob_date_end ? moment(dataTrip.aob_date_end).toDate(): moment().toDate(),
    });
    setStartDate(dataTrip.aob_date_start ? moment(dataTrip.aob_date_start).toDate() :moment().toDate());
    setEndDate(dataTrip.aob_date_end ? moment(dataTrip.aob_date_end).toDate(): moment().toDate());
  }, [])
  
  const handleStartDateChange = (date: Date) => {
    
    setStartDate(date);
    setdataTrip({
      ...dataTrip,
      aob_date_start: date,
    });
    if (endDate) {
      setPrice(calculatePrice(date, endDate));
    }
  };

  const handleEndDateChange = (date: Date) => {

    if (startDate) {
      const price = calculatePrice(startDate, date);
      
      setPrice(price);
      setEndDate(date);
      setdataTrip({
        ...dataTrip,
        aob_date_end: date,
        aob_price: price,
      });
    }
  };
  useEffect(() => {
    if (price > 0) {
      setdataTrip({
        ...dataTrip,
        aob_price: price,
      });
    }
  }, [price]);

  return (
    <div className='w-full flex-col sm:flex-row flex justify-between'>
     <div className='w-full sm:w-[50%]'>
      <div className='flex flex-row  justify-between z-50 date-container'>

   
      <DatePicker
        selected={startDate}
        onChange={handleStartDateChange}
        dateFormat='dd/MM/yyyy'
        startOpen={false}
        className='flex flex-1 w-full z-100'
        selectsStart
        startDate={startDate}
        // maxDate={endDate}
        preventOpenOnFocus={true}
        filterDate={filterDates}
        popperPlacement='bottom-start'
        locale='es'
        autoFocus={false}
        customInput={
          <div className='flex flex-1 w-full pop-field'>
            <p className='absolute border-white-primary border-[1px] p-1 rounded-sm left-4 z-50 bg-gray-secondary capitalize px-2 top-0'>
              fecha de entrada
            </p>
            <button className='mt-4 rounded-lg flex flex-row items-center justify-center focus:border-pink-primary py-5 bg-gray-secondary  focus:outline-none border-[1px] border-gray-secondary capitalize w-100'>
              <div className='absolute left-5'>
                <AiOutlineCalendar size={25} color='#6C6C6C' />
              </div>
              {moment(startDate).format('DD/MM/yyyy')}{' '}
            </button>
          </div>
        }
      />
      <DatePicker
        selected={endDate}
        onChange={handleEndDateChange}
        dateFormat='dd/MM/yyyy'
        startOpen={false}
        className='flex flex-1 w-full'
        filterDate={filterDates}
        minDate={startDate}
        preventOpenOnFocus={true}
        selectsEnd
        startDate={startDate}
        endDate={endDate}
        locale='es'
        popperPlacement='bottom-start'
        autoFocus={false}
        customInput={
          <div className='pop-field '>
            <p className='absolute border-white-primary border-[1px] p-1 rounded-sm left-4 z-50 bg-gray-secondary capitalize px-2 top-0'>
              fecha de salida  
            </p>
            <button className='mt-4 rounded-lg flex flex-row items-center justify-center focus:border-pink-primary py-5 bg-gray-secondary  focus:outline-none border-[1px] border-gray-secondary capitalize w-100'>
              <div className='absolute left-5'>
                <AiOutlineCalendar size={25} color='#6C6C6C' />
              </div>
              {moment(endDate).format('DD/MM/yyyy')}{' '}
            </button>
          </div>
        }
      />


  </div>
  <div className='bg-gray-secondary rounded-lg py-5 mt-5'>
        <p className='text-center text-xl font-bold '>
        Cantidad de noches: {moment(endDate).diff(moment(startDate), 'days')}
        </p>
  </div>
         </div>
         <div className='w-full sm:w-[40%] h-full  rounded-lg relative pop-field mt-5 sm:mt-0'>
         <p className='absolute border-white-primary border-[1px] p-1 rounded-sm left-4 z-50 bg-gray-secondary capitalize px-2 top-0'>
             Notas de estadia
            </p>
          <textarea className='w-full mt-5 rounded-lg py-3 px-3 bg-gray-secondary h-full outline-none pt-10' placeholder='Comentarios' onChange={
            (e) => {
              setdataTrip({
                ...dataTrip,
                aob_comments: e.target.value,
              });
            }
          } />

         </div>

    </div>
  );
};
export default TripDataInputs;
