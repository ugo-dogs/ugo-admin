interface OwnerDataInputsProps {
    inputs?: {
      type: string;
      value: string;
      placeholder: string;

    }[];
    dataOwner: any;
    setDataOwner : (data:any) => void;
  }
  

  const OwnerDataInputs: React.FC<OwnerDataInputsProps> = ({ inputs ,dataOwner,setDataOwner}) => {


    return (
      <>
        {inputs?.map((field) => (
          <div className='relative mt-5 mr-5 pop-field' key={field.value}>
            <p className='absolute border-white-primary border-[1px] p-1 rounded-sm left-4 z-50 bg-gray-secondary capitalize px-2 pop-field-name'>
              {field.placeholder}
            </p>
            <input
              type={field.type}
              value={dataOwner[field.placeholder]}
              placeholder={field.placeholder}
              onChange={(e) => {
                setDataOwner({
                  ...dataOwner,
                  [field.placeholder]: e.target.value,
                });
              }}
              className='mt-4 pl-10 rounded-lg focus:border-pink-primary py-6 bg-gray-secondary focus:outline-none border-[1px] border-gray-secondary capitalize'
            />
          </div>
        ))}
      </>
    );
  };
  export default OwnerDataInputs;