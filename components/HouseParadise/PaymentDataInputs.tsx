import { registerLocale } from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import 'moment/locale/es';

import es from 'date-fns/locale/es';
import { MdKeyboardArrowDown } from 'react-icons/md';

import { calculatePrice } from '../../lib/utils/HouseParadise';
registerLocale('es', es);
interface PaymentDataInputProps {
  dataTrip: any;
  setdataTrip: (data: any) => void;
}
const PaymentDataInputs: React.FC<PaymentDataInputProps> = ({ dataTrip ,setdataTrip}) => {

  return (
    <div className=' w-full sm:flex-row flex flex-col sm:justify-between'>
      <div className='relative bg-gray-secondary rounded-lg sm:w-[70%] px-7 h-fit py-8'>
        <div className='flex flex-row justify-between border-b-[1px] pb-4'>
          <p>
            $4000 por{' '}
            {moment(dataTrip.aob_date_end).diff(
              moment(dataTrip.aob_date_start),
              'days'
            )}{' '}
            noches
          </p>
          <p>
            {new Intl.NumberFormat('es-AR', {
              style: 'currency',
              currency: 'ARS',
              maximumSignificantDigits: 3,
            }).format(dataTrip.aob_price)}
          </p>
        </div>
        <div className='flex flex-row justify-between  pb-4 pt-4'>
          <p>Tarifa traslado</p>
          <p>
            {new Intl.NumberFormat('es-AR', {
              style: 'currency',
              currency: 'ARS',
              maximumSignificantDigits: 3,
            }).format(4000)}
          </p>
        </div>
        <div className='flex flex-row justify-between pb-4 pt-4 bg-white-primary absolute w-[100%] left-0 right-0 px-7 rounded-b-lg'>
          <p className='text-black-primary font-bold'>Saldo total a pagar</p>
          <p className='text-black-primary font-bold'>
            {new Intl.NumberFormat('es-AR', {
              style: 'currency',
              currency: 'ARS',
              maximumSignificantDigits: 3,
            }).format(
              calculatePrice(dataTrip.aob_date_start, dataTrip.aob_date_end)
            )}
          </p>
        </div>
      </div>
      <div className='sm:w-[30%] sm:ml-7 mt-10  sm:mt-0'>
        <button className=' bg-pink-primary rounded-lg w-full mb-8 py-4'>
          Registrar pago
        </button>
        <button className=' bg-gray-secondary rounded-lg w-full py-4'>
          Agregar descuento
        </button>
        <div className='relative mt-5 mr-5 pop-field w-full'>
          <p className='absolute border-white-primary border-[1px] p-1 rounded-sm left-4 z-50 bg-gray-secondary capitalize px-2'>
            Estado de reserva
          </p>
          <div className='relative w-full pop-field'>
          <select
              className='mt-4 pl-10 rounded-lg focus:border-pink-primary py-6 bg-gray-secondary focus:outline-none border-[1px] border-gray-secondary capitalize w-60  appearance-none cursor-pointer w-full'
        value={dataTrip.aob_purchased}
              onChange={(e) => {
                setdataTrip({
                  ...dataTrip,
                  "aob_purchased": e.target.value,
                });
              }}
            >
              {['consulta','anticipo','full'].map((option) => (
                <option key={option} value={option} >
                  {option}
                </option>
              ))}
            </select> 
            <MdKeyboardArrowDown className=' absolute  top-[49%] right-3 z-50' color='#fff'  size={21}/>

          </div>

   
        </div>
        <div className='relative mt-5 mr-5 pop-field w-full'>
          <p className='absolute border-white-primary border-[1px] p-1 rounded-sm left-4 z-50 bg-gray-secondary capitalize px-2 top-[-5px]'>
            Datos de estadia
          </p>
          <div className='mt-4 pl-10 rounded-lg focus:border-pink-primary py-6 bg-gray-secondary focus:outline-none border-[1px] border-gray-secondary capitalize outline-none w-full'>
            <p>{dataTrip.aob_comments}</p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default PaymentDataInputs;
