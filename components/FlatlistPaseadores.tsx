import { FunctionComponent } from 'react';
import classnames from 'classnames';
import { useState, useRef } from 'react';
import Image from 'next/image';

import CloseWhite from '../public/close-white.svg';
import Pen from '../public/pen.svg';

import Valoration from '../components/Valoration';
import Turno from './Turno';
import Day from './Day';
import Review from './Review';
import { MdModeEditOutline } from 'react-icons/md';
import { getToken } from 'next-auth/jwt';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ModalWalkerEdit from '../components/ModalWalkerEdit';
import { Barrios, Dog, Walker } from '../lib/types';

const { NEXT_PUBLIC_API_URL } = process.env;

type Props = {
    handleSidebar: (id:string) => void;
    onSelectPaseador:(walker:object)=>void;
    initialValue:any
  data:any;
};

const FlatlistPaseadores: FunctionComponent<Props> = ({

  data,
  handleSidebar,
  initialValue,
  onSelectPaseador
}) => {
  console.log('ii',initialValue);
  

  return (
    <ul className='grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-10 mt-20'>
    {data!.map((walker:any) => {
      const completedTurns =
        walker.turnos.length === 1
          ? '1 paseo completado'
          : `${walker.turnos.length} paseos completados`;

      return (
        <li
          key={walker._id}
          className={`hover:cursor-pointer  border-2 hover:border-white-primary bg-gray-secondary flex flex-col items-center pt-4 pb-8 rounded-lg max-w-[230px] ${initialValue?.paseador._id == walker._id ? 'border-pink-secondary' : 'border-gray-secondary'}`}
          onClick={() => {handleSidebar(walker._id);onSelectPaseador(walker)}}
        >
          <div className={`rounded-full border-2 border-white-primary w-[80px] h-[80px] mb-6`}>
      {walker.thumb && (
  <Image
  src={walker.thumb.formats.thumbnail.url}
  alt={walker.first_name}
  width={80}
  height={80}
  className='rounded-full'
/>
      )}
      
          </div>
          <span className='font-bold'>
            {walker.first_name} {walker.last_name}
          </span>
          <span className='text-xs mb-6'>{completedTurns}</span>
          <Valoration valoration={walker.valoration} />
        </li>
      );
    })}
  </ul>
  );
};

export default FlatlistPaseadores;
