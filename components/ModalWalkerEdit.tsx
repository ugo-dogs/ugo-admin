import {
  FunctionComponent,
  ReactChild,
  ReactFragment,
  ReactPortal,
  useState,
} from 'react';

import Head from 'next/head';
import Filter from '../lib/utils/WalkerEditFilter';
import classnames from 'classnames';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Image from 'next/image';
import Close from '../public/close-white.svg'

import { Barrios, Walker } from '../lib/types';
import Turno from './Turno';
import Day from './Day';
import type { GetServerSideProps } from 'next';
import router from 'next/router';



type Props = {
  walker: Walker;
  handleModalClose: () => void;
  barrios:Array<Barrios>,
  //   onClose: () => void;
  //   data: Payment;
  //   handlePayment: (data: Payment) => void;
};
const { NEXT_PUBLIC_API_URL } = process.env;
const fullDays = ["1","2","3","4","5","6","7"]
const fullTurnos = [{ titulo:'Turno mañana',id: "6239bea5626931001bb264e9",start: "07:00:00.000",
end: "12:00:00.000" },{ titulo:'Turno tarde',id: "6239bea5626931001bb264ea",
start: "12:00:00.000",
end: "18:00:00.000" },{ titulo:'Turno noche',end: "22:00:00.000",
id: "6239bea5626931001bb264eb",
start: "18:00:00.000" }]

const ModalWalkerEdit: FunctionComponent<Props> = (props) => {
  const { walker, handleModalClose,barrios } = props;
  
  const [menu_active, setmenu_active] = useState(Filter[0]);
  const [walkerData, setwalkerData] = useState<any>(walker);

  const select_barrio =(barrio:string) => {
    // let barrioType = barrio:Barrios
  let selectedBarrio = barrios.find((barrioItem) => barrioItem.barrio == barrio)
    if(selectedBarrio){
      setwalkerData({ ...walkerData,paseador_zone:selectedBarrio })

    }
  
  }
const selectDayPaseo = (day:string)=> {
if(walkerData.days_available.days.includes(day)){
  setwalkerData({ ...walkerData,days_available: { days : walkerData.days_available.days.filter((dayItem: string)=> dayItem !== day) } })
} else {
  let addDay = [...walkerData.days_available.days,day]
  addDay.sort(function (a, b) {
    if (a  < b) {
        return -1;
    }
    if (b > a) {
        return 1;
    }
    return 0;
});
  setwalkerData({ ...walkerData,days_available: { days : addDay  } })

}
}
const selectTurnoPaseador = (turno:{
  titulo:string;
  
  id:string;
  start:string;
  end:string
})=>{
  if(walkerData?.turnos?.some((turnoItem: { titulo: string; })=> turnoItem?.titulo == turno?.titulo)){
    setwalkerData({ ...walkerData,turnos:  walkerData.turnos.filter((turnoItem: { titulo: string; })=> turnoItem.titulo !== turno.titulo) })
  } else {
    let addTurno = [...walkerData.turnos,turno]
    addTurno.sort(function (a, b) {
      if (a.start < b.start) {
          return -1;
      }
      if (b.start > a.start) {
          return 1;
      }
      return 0;
  });
    setwalkerData({ ...walkerData,turnos:addTurno })
  
  }
}
console.log('walker',walkerData);

  const submitPaseador = async () => {
    console.log('entra?');

    // const token = getToken({ req, secret: SECRET! }) as any;
    try {
      const result = await fetch(process.env.NEXT_PUBLIC_API_URL + '/users/' + walker._id, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          // Authorization: `Bearer ${token.jwt}`,
        },
        body: JSON.stringify({
          ...walkerData,
        }),
      });
      const parseResult = await result.json();
      console.log('parse', parseResult);

      toast.success('Paseador actualizado correctamente!', {
        position: 'bottom-right',
        autoClose: 2200,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'dark',
      });

      setTimeout(() => {
        router.reload();
      }, 1000);
    } catch (e) {
      toast.error('ERROR!', {
        position: 'bottom-right',
        autoClose: 2200,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'dark',
      });
    }
  };

  const menuClassnames = (
    active: {
      title: string;
      fields?: {
        type: string;
        value: string;
        placeholder: string;
        section: string;
      }[];
    },
    current: {
      title: string;
      fields?: {
        type: string;
        value: string;
        placeholder: string;
        section: string;
      }[];
    }
  ) =>
    classnames(
      'text-white-primary rounded-md h-14 items-center justify-center flex cursor-pointer mr-[-2px]',
      {
        'bg-pink-primary ': active.title == current.title,
      },
      {
        'bg-gray-secondary': active.title !== current.title,
      }
    );
  return (
    <div
      className='fixed inset-0 overflow-y-auto z-50'
      aria-labelledby='modal-title'
      role='dialog'
      aria-modal='true'
    >
      <div className='flex items-end justify-center h-5/6	  text-center sm:block sm:p-0  z-50'>
        {/* <div
        className='fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity'
        aria-hidden='true'
      ></div> */}

        <span
          className='hidden sm:inline-block sm:align-middle sm:h-screen '
          aria-hidden='true'
        >
          &#8203;
        </span>

        <div className=' h-full	 relative  py-8 inline-block align-bottom bg-gray-primary border-pink-primary  border-[1px] rounded-lg text-left shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-4xl   sm:w-full'>
          <div
            onClick={() => handleModalClose()}
            className='absolute right-[15px] top-[15px] z-100 flex items-center justify-center w-[40px] h-[40px] rounded-full hover:cursor-pointer'
          >
            {/* <p className='font-medium text-xl'>X</p> */}
            <Image
          src={Close}
          alt="uGo admin logo"
          width={18}
          height={18}
          className="hover:cursor-pointer"
        />
          </div>
          <div className='bg-white  px-16 '>
            <h1 className='text-3xl  font-bold '>
              Editar paseador{' '}
              <span className='text-pink-primary text-3xl mb-[2px] font-bold'>
                {walker.first_name}
              </span>
            </h1>
          </div>
          <div className='flex flex-row pt-6 mx-20 relative '>
            {Filter.map(
              (menu_item: {
                title: string;
                icon: string;
                fields: Array<{
                  type: string;
                  value: string;
                  placeholder: string;
                  section: string;
                }>;
              }) => (
                <div className='flex-1 w-[100%]' key={menu_item.title}>
                  <div
                    className={` ${menuClassnames(menu_active, menu_item)}`}
                    onClick={() => setmenu_active(menu_item)}
                  >
                    <Image
                      src={require(`../public/${menu_item.icon}.svg`)}
                      height={20}
                      width={20}
                      alt='menu icon'
                    />
                    <h3 className=' font-semibold ml-3'>{menu_item.title}</h3>
                  </div>
                  <div className='pt-8 flex flex-row flex-wrap  flex-1 left-0 w-[100%] absolute mr-20  '>
                    {menu_item.title == menu_active.title &&
                      menu_item.fields.map((field) => (
                
                        <div className='relative   mt-5 mr-5 ' key={field.value}>
                          <p className='absolute border-white-primary border-[1px] p-1  rounded-sm left-4 z-50 bg-gray-secondary capitalize px-2'>
                            {field.placeholder}
                          </p>
                          {field.section == 'turnos' ? (   //FIELD TURNOS
                            <ul className='flex flex-row mt-6 mb-7'>
                              {fullTurnos.map((turno,i) => (
                                <Turno key={i} turno={turno}  selected={walkerData.turnos} onPress={(turno)=>selectTurnoPaseador(turno)}/>
                              ))}
                            </ul>
                          ) : field.section == 'days_available' ? ( //FIELD DIAS
                            <ul className='flex flex-row mt-6'>
                              {fullDays.map((day) => (
                                <Day key={day} day={day} selected={walkerData.days_available.days} onPress={(day)=>selectDayPaseo(day)} />
                              ))}
                            </ul>
                          ) : field.section == 'paseador_zone' ? <select  className='mt-4 pl-10 rounded-lg focus:border-pink-primary py-6 bg-gray-secondary  focus:outline-none border-[1px] border-gray-secondary capitalize w-60 outline-none' onChange={(e) => select_barrio(e.target.value)}>
                            {barrios.map((barrio)=> <option key={barrio.barrio} selected={walkerData.paseador_zone.barrio == barrio.barrio ? true : false} value={barrio.barrio}>{barrio.barrio}</option>)}
                          </select> :   ( //FIELD TEXTO
                            <input 
                              type={field.type}
                              value={
                                field.section && walkerData[`${field.section}`] 
                                  ? walkerData[`${field.section}`][
                                      `${field.value}`
                                    ]
                                  : walkerData[`${field.value}`]
                              }
                              placeholder={field.placeholder}
                              onChange={(e) => {
                                if (field.section) {
                                  setwalkerData({
                                    ...walkerData,
                                    [`${field.section}`]: {
                                      ...walkerData[`${field.section}`],
                                      [field.value]: e.target.value,
                                    },
                                  });
                                } else {
                                  setwalkerData({
                                    ...walkerData,
                                    [field.value]: e.target.value,
                                  });
                                }
                              }}
                              className='mt-4 pl-10 rounded-lg focus:border-pink-primary py-6 bg-gray-secondary  focus:outline-none border-[1px] border-gray-secondary capitalize w-60'
                            />
                          )}
                        </div>
                      ))}
                  </div>
                </div>
              )
            )}
          </div>
      
          <div className='absolute bottom-8 right-15 flex flex-row items-center'>
            <div
              className=' bg-pink-primary px-12 py-4 rounded-md cursor-pointer'
              onClick={() => submitPaseador()}
            >
              <p className='font-medium text-lg'>Guardar cambios</p>
            </div>
            <div className='ml-10 cursor-pointer'>
              <p
                className=' text-lg border-b-[1px]'
                onClick={() => handleModalClose()}
              >
                Cancelar
              </p>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer
            position='bottom-right'
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
    </div>
  );
};

export default ModalWalkerEdit;
function handleModalClose(): void {
  throw new Error('Function not implemented.');
}
