import {
  FunctionComponent,
  ReactChild,
  ReactFragment,
  ReactPortal,
  SetStateAction,
  useState,
  useEffect,
} from 'react';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Image from 'next/image';
import Close from '../public/close-white.svg';
import moment from 'moment';
import { Barrios, Walker, Dog } from '../lib/types';
import Turno from './Turno';
import Day from './Day';
import type { GetServerSideProps } from 'next';
import { AiOutlineUser } from 'react-icons/ai';
import { MdKeyboardArrowDown } from 'react-icons/md';

import { HiOutlinePhone } from 'react-icons/hi';
import { AiOutlineCalendar ,AiOutlineClockCircle } from 'react-icons/ai';

import { TiLocationArrowOutline } from 'react-icons/ti';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Autocomplete from 'react-google-autocomplete';
import { getSession } from 'next-auth/react';
import router from 'next/router';
type Props = {
  dog: any;
  // walker: Walker;
  barrios: Array<Barrios>;
  handleModalClose: () => void;
  openPaseadores: () => void;


};
const { NEXT_PUBLIC_API_URL } = process.env;

const ModalPaseoEdit: FunctionComponent<Props> = (props) => {
  const { dog, handleModalClose, barrios, openPaseadores } = props;

  const [editPaseo, setEditPaseo] = useState({ ...dog });
  const [startDate, setStartDate] = useState(moment(editPaseo.date).toDate());

  const select_barrio = (barrio: string) => {
    let selectedBarrio = barrios.find(
      (barrioItem) => barrioItem.barrio == barrio
    );
    if (selectedBarrio) {
      console.log(selectedBarrio);

      setEditPaseo({ ...editPaseo, direccion: selectedBarrio });
    }
  };

  const submitPaseo = async () => {

    const token = await getSession();

    if (token) {
      try {
        console.log({
          ...editPaseo,
          date: moment(startDate).format('YYYY-MM-DD'),
        });

        const result = await fetch(
          process.env.NEXT_PUBLIC_API_URL + '/paseos/' + editPaseo._id,
          {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${token.jwt}`,
            },
            body: JSON.stringify({
              ...editPaseo,
              date: moment(startDate).format('YYYY-MM-DD'),
              start: moment(editPaseo.start, 'HH:mm').format('HH:mm:ss'),
              end: moment(editPaseo.end, 'HH:mm').format('HH:mm:ss'),
            }),
          }
        );
        const parseResult = await result.json();
        console.log('parse', parseResult);
        // console.log('date', moment(startDate).format('YYYY-MM-DD'));
        if (parseResult) {
          toast.success('Paseo actualizado correctamente!', {
            position: 'bottom-right',
            autoClose: 2200,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: 'dark',
          });
          setTimeout(() => {
            router.reload();
          }, 1000);
        }
      } catch (e) {
        toast.error('ERROR!', {
          position: 'bottom-right',
          autoClose: 2200,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'dark',
        });
      }
    }
  };

  return (
    <div
      className='fixed inset-0 overflow-y-auto z-50'
      aria-labelledby='modal-title'
      role='dialog'
      aria-modal='true'
    >
      <div className='flex items-end justify-center h-[88%]  text-center sm:block sm:p-0  z-50'>

        <span
          className='hidden sm:inline-block sm:align-middle sm:h-screen '
          aria-hidden='true'
        >
          &#8203;
        </span>

        <div className=' h-full	 relative   inline-block align-bottom bg-gray-primary border-pink-primary  border-[1px] rounded-lg text-left shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-4xl   sm:w-full'>
          <div
            onClick={() => handleModalClose()}
            className='absolute right-[15px] top-[15px] z-100 flex items-center justify-center w-[40px] h-[40px] rounded-full hover:cursor-pointer'
          >
            <Image
              src={Close}
              alt='uGo admin logo'
              width={18}
              height={18}
              className='hover:cursor-pointer'
            />
          </div>
          <div className='bg-white  px-16 flex-row flex justify-between mt-10'>
       
            <div>
              <h1 className='text-3xl  font-bold '>
                Editar paseo de{' '}
                <span className='text-pink-primary text-3xl mb-[2px] font-bold'>
                  {dog.dog.name}
                </span>
              </h1>
              <span className='font-bold text-md mb-4 opacity-60'>
                {dog.dog.raza.name},{' '}
                {dog.dog.age === 1 ? ' 1 año' : `${dog.dog.age} años`}
              </span>
              <div className='flex flex-row items-center mt-3 text-pink-primary'>
                <div className=' bg-gray-secondary px-12 py-4 rounded-md mr-5 flex-row flex items-center justify-between'>
                  <AiOutlineUser size={20} />
                  <p className='ml-2'>
                    Dueño:{' '}
                    {editPaseo.paseador.first_name +
                      editPaseo.paseador.last_name}
                  </p>
                </div>
                <div className=' bg-gray-secondary px-12 py-4 rounded-md flex-row flex items-center'>
                  <HiOutlinePhone size={20} />
                  <p className='ml-2'>+{editPaseo.paseador.phone}</p>
                </div>
              </div>
            </div>
            <div className='border-white-primary  border-[2px] rounded-full flex '>
              <Image
                className='flex-1 rounded-full border-pink-primary  border-[2px]'
                src={dog.dog.avatar.formats.thumbnail.url}
                alt='uGo admin logo'
                width={150}
                height={150}
              />
            </div>
          </div>
          <div className='flex flex-row  mx-16   relative  w-[80%] '>
            <div className='flex flex-row flex-wrap  flex-1 left-0 w-[100%] absolute mr-2'>
              <div className='relative   mt-5 mr-5 '>
                <p className='absolute border-white-primary border-[1px] p-1  rounded-sm left-4 z-50 bg-gray-secondary  px-2  text-xs'>
                  Fecha de paseo
                </p>
                <DatePicker
                  selected={startDate}
                  onChange={(date: Date) => setStartDate(date)}
                  dateFormat='dd/MM/yyyy'
                  startOpen={false}
                  preventOpenOnFocus={true}
                  popperPlacement='bottom-start'
                  autoFocus={false}
                  customInput={
                    <button className='mt-4 rounded-lg flex flex-row items-center justify-center focus:border-pink-primary py-5 bg-gray-secondary  focus:outline-none border-[1px] border-gray-secondary capitalize w-52'>
                      <div className='absolute left-5'>
                        <AiOutlineCalendar size={25}  color='#6C6C6C'/>
                      </div>
                      {moment(startDate).format('DD/MM/yyyy')}{' '}
                    </button>
                  }
                />
              </div>
              <div className='relative   mt-5 mr-5 '>
                <p className='absolute border-white-primary border-[1px] p-1  rounded-sm left-4 z-20 bg-gray-secondary  px-2 text-xs'>
                  Hora de retiro
                </p>
                <div className='absolute bottom-5 left-4'>
                <AiOutlineClockCircle  size={25} color='#6C6C6C'/>

                </div>
                <select
                  onChange={(e) =>
                    setEditPaseo({ ...editPaseo, start: e.target.value })
                  }
                  className='mt-4 pl-14 rounded-lg focus:border-pink-primary py-5 bg-gray-secondary  focus:outline-none border-[1px] border-gray-secondary capitalize w-36 appearance-none'
                >
                  {[
                    '07:00',
                    '08:00',
                    '09:00',
                    '10:00',
                    '11:00',
                    '12:00',
                    '13:00',
                    '14:00',
                    '15:00',
                    '16:00',
                    '17:00',
                    '18:00',
                    '19:00',
                    '20:00',
                    '21:00',
                    '22:00',
                    '23:00',
                  ].map((time) => (
                    <option
                    key={time}
                      selected={
                        time == moment(editPaseo.start, 'HH:mm').format('HH:mm')
                      }
                    >
                      {time}
                    </option>
                  ))}
                </select>
           
              </div>
              <div className='relative   mt-5 mr-5 '>
                <p className='absolute border-white-primary border-[1px] p-1  rounded-sm left-4 z-50 bg-gray-secondary  px-2 text-xs'>
                  Hora de entrega
                </p>
                <div className='absolute bottom-5 left-4'>
                <AiOutlineClockCircle  size={25} color='#6C6C6C'/>

                </div>
                <select
                  onChange={(e) =>
                    setEditPaseo({ ...editPaseo, end: e.target.value })
                  }
                  className='mt-4 pl-14 rounded-lg focus:border-pink-primary py-5 bg-gray-secondary  focus:outline-none border-[1px] border-gray-secondary capitalize w-36 outline-none appearance-none'
                >
                  {[
                    '07:00',
                    '08:00',
                    '09:00',
                    '10:00',
                    '11:00',
                    '12:00',
                    '13:00',
                    '14:00',
                    '15:00',
                    '16:00',
                    '17:00',
                    '18:00',
                    '19:00',
                    '20:00',
                    '21:00',
                    '22:00',
                    '23:00',
                  ].map((time) => (
                    <option
                    key={time}
                      className='outline-none'
                      selected={
                        time == moment(editPaseo.end, 'HH:mm').format('HH:mm')
                      }
                    >
                      {time}
                    </option>
                  ))}
                </select>
              </div>
              <div className='relative   mt-7 mr-5 z-90'>
                <p className='absolute border-white-primary border-[1px] p-1  rounded-sm left-4 z-10 bg-gray-secondary  px-2 text-xs'>
                  Dirección de entrega
                </p>
                <div className='absolute bottom-5 left-4'>
                <TiLocationArrowOutline  size={30} color='#6C6C6C'/>

                </div>
                
                <Autocomplete
                  apiKey={'AIzaSyBlQDuF84af0IFux3UKY3Rt8azRk2xn4BA'}
                  defaultValue={editPaseo.direccion.label}
                  language='es'
                  className='mt-4 pl-14 rounded-lg focus:border-pink-primary py-5 bg-gray-secondary   focus:outline-none border-[1px] border-gray-secondary capitalize w-80'
                  options={{
                    types: ['address'],
                    componentRestrictions: { country: 'AR' },
                    region: 'AR-C',
                    strictBounds: true,
                  }}
                  onPlaceSelected={(place) => {
                    setEditPaseo({
                      ...editPaseo,
                      direccion: {
                        barrio: editPaseo.direccion.barrio,
                        label: place.formatted_address
                          .replace(', Argentina', '')
                          .replace(', Buenos Aires', ''),
                        lat: place.geometry.location.lat(),
                        lng: place.geometry.location.lng(),
                      },
                    });
                  }}
                />
              </div>
              <div className='relative   mt-7 mr-5 z-0'>
                <p className='absolute border-white-primary border-[1px] p-1  rounded-sm left-4 z-10 bg-gray-secondary capitalize px-2 text-xs'>
                  Barrio
                </p>
                <div className='absolute bottom-5 left-4'>
                <TiLocationArrowOutline  size={30} color='#6C6C6C'/>

                </div>
                <select
                  onChange={(e) => select_barrio(e.target.value)}
                  className='  cursor-pointer mt-4 pl-14 rounded-lg focus:border-pink-primary py-5 bg-gray-secondary  focus:outline-none border-[1px] border-gray-secondary capitalize  w-36 appearance-none'
                >
                  {barrios.map((barrio) => (
                    <option
                    key={barrio.barrio}
                      selected={
                        editPaseo.direccion.barrio.barrio == barrio.barrio
                          ? true
                          : false
                      }
                      value={barrio.barrio}
                    >
                      {barrio.barrio}
                    </option>
                  ))}
                </select>
              </div>
              <div className='relative   mt-7 mr-5 z-0'>
                <p className='absolute border-white-primary border-[1px] p-1  rounded-sm left-4 bg-gray-secondary  px-2 text-xs'>
                  Paseador a cargo
                </p>
                <div
                  className='mt-4 rounded-lg focus:border-pink-primary pt-6 pb-3 bg-gray-secondary z-0 focus:outline-none border-[1px] border-gray-secondary capitalize w-80 cursor-pointer flex-row flex items-center justify-between px-3'
                  onClick={() => openPaseadores()}
                >
                  <Image
                    src={dog.paseador.thumb.formats.thumbnail.url}
                    alt={dog.paseador.first_name}
                   
                    width={60}
                    height={60}
                    className='rounded-full border-2 border-white-secondary'
                  />
                  <p className='text-center'>
                    {dog.paseador.first_name + ' ' + dog.paseador.last_name}
                  </p>
                  <MdKeyboardArrowDown
                    className='hover:cursor-pointer'
                    size={30}
                    color='#6C6C6C'
                  />
                </div>
              </div>
            </div>
          </div>
          <div className='absolute bottom-14 left-15 flex flex-row items-center'>
            <input type={'checkbox'} className='mr-2' />
            <p>Guardar para futuros paseos</p>
          </div>
          <div className='absolute bottom-8 right-15 flex flex-row items-center'>
            <div
              className=' bg-pink-primary px-12 py-4 rounded-md cursor-pointer'
              onClick={submitPaseo}
            >
              <p className='font-medium text-lg'>Guardar cambios</p>
            </div>
            <div className='ml-10 cursor-pointer'>
              <p
                className=' text-lg border-b-[1px]'
                onClick={() => handleModalClose()}
              >
                Cancelar
              </p>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer
        position='bottom-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
};

export default ModalPaseoEdit;
