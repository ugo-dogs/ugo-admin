import { FunctionComponent } from 'react';

import Head from 'next/head';

import Navbar from './Navbar';
import { Payment } from '../lib/types';
type Props = {
  onClose: () => void;
  data: Payment;
  handlePayment: (data: Payment) => void;
};
const Modal: FunctionComponent<Props> = (props) => {
  const { onClose, data, handlePayment } = props;

  return (
    <div
      className='fixed z-10 inset-0 overflow-y-auto z-100 '
      aria-labelledby='modal-title'
      role='dialog'
      aria-modal='true'
    >
      <div className=' flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0'>
        {/* <div
        className='fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity'
        aria-hidden='true'
      ></div> */}

        <span
          className='hidden sm:inline-block sm:align-middle sm:h-screen '
          aria-hidden='true'
        >
          &#8203;
        </span>

        <div className=' relative  inline-block align-bottom bg-white-primary rounded-lg text-left shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-4xl   sm:w-full text-black-primary'>
          <div
            onClick={() => onClose()}
            className='absolute right-[-20px] top-[-30px] z-100 bg-pink-primary  flex items-center justify-center w-[60px] h-[60px] rounded-full hover:cursor-pointer'
          >
            <p className='font-medium text-2xl'>X</p>
          </div>
          <div className='bg-white  px-16 py-20'>
            <div className='sm:flex sm:items-start w-full'>
              <div className='mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left w-full'>
                <h2
                  className='text-4xl  font-medium text-gray-900'
                  id='modal-title'
                >
                  Total a pagar a {data.paseador.first_name}
                </h2>
                <div className='my-6'>
                  <p className='text-6xl font-bold'> $ {data.total_amount}</p>
                </div>
                <div className='my-6 flex'>
                  {/* <svg
                    width='22'
                    height='22'
                    viewBox='0 0 23 23'
                    fill='none'
                    xmlns='http://www.w3.org/2000/svg'
                  >
                    <path
                      d='M19.9057 22H3.08909C1.93541 22 1 21.0646 1 19.9109V5.73942V3.08909C1 1.93541 1.93541 1 3.08909 1H19.9109C21.0646 1 22 1.93541 22 3.08909V19.9109C21.9948 21.0646 21.0594 22 19.9057 22Z'
                      stroke='black'
                      stroke-width='1.5'
                      stroke-miterlimit='10'
                      stroke-linecap='round'
                      stroke-linejoin='round'
                    />
                    <path
                      d='M22 9H1'
                      stroke='black'
                      stroke-width='1.5'
                      stroke-miterlimit='10'
                      stroke-linecap='round'
                      stroke-linejoin='round'
                    />
                    <path
                      d='M5.68511 6.28038C6.31482 6.28038 6.8253 5.7699 6.8253 5.14019C6.8253 4.51048 6.31482 4 5.68511 4C5.0554 4 4.54492 4.51048 4.54492 5.14019C4.54492 5.7699 5.0554 6.28038 5.68511 6.28038Z'
                      fill='black'
                    />
                    <path
                      d='M17.3668 6.30626C17.9965 6.30626 18.5069 5.79578 18.5069 5.16607C18.5069 4.53636 17.9965 4.02588 17.3668 4.02588C16.737 4.02588 16.2266 4.53636 16.2266 5.16607C16.2266 5.79578 16.737 6.30626 17.3668 6.30626Z'
                      fill='black'
                    />
                  </svg> */}

                  <p className='text-xl font-medium pl-3'>
                    Vence el {data.expiration}
                  </p>
                </div>
                {data.paseador.bank_account ? (
                  <div className='flex  justify-between'>
                    <div className='mt-2 border border-slate-300  rounded-sm p-7 shadow-sm'>
                      <h3 className='text-md font-bold uppercase mb-2'>
                        Datos de meli del paseador
                      </h3>
                      <p className='text-sm text-gray-500 uppercase'>
                        BANCO : {data.paseador.bank_account.bank_name}
                      </p>
                      <p className='text-sm text-gray-500 uppercase '>
                        CBU : {data.paseador.bank_account.cbu}
                      </p>
                      <p className='text-sm text-black uppercase'>
                        alias : {data.paseador.bank_account.alias}
                      </p>
                    </div>
                    <div className='self-end justify-self-end'>
                      <button
                        type='button'
                        onClick={() => handlePayment(data)}
                        className='w-full justify-center text-pink-primary  text-lg bg-gray-primary rounded-sm  font-medium px-16 py-6 sm:w-auto '
                      >
                        Pagar via Mercadopago
                      </button>
                    </div>
                  </div>
                ) : (
                  <p>Este paseador no tiene cuenta bancaria</p>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
