import { FunctionComponent, useState, KeyboardEvent } from "react";

import Image from 'next/image';

import MagnifyingGlass from '../public/magnifying-glass.svg'

type TextFilterProps = {
  label: string
  placeholder: string
  action: (value: string) => void
}

const TextFilter: FunctionComponent<TextFilterProps> = ({ label, placeholder, action }) => {

  const [value, setValue] = useState('')

  const onKeyUp = (event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter'){
      action(value)
    }
  }

  return (
    <div className="">
    <label className="relative">
      <span className="font-bold">{label}</span>
      <div className="absolute top-0 left-5">
        <Image
          src={MagnifyingGlass}
          alt="magnifying glass"
          width={18}
          height={18}
        />
      </div>
      <input
        type="text"
        placeholder={placeholder}
        className="bg-black-primary w-full py-5 pl-14 placeholder-white-primary rounded-lg focus:outline-none"
        onKeyUp={onKeyUp}
        value={value}
        onChange={(event => setValue(event.target.value))}
      />
    </label>
  </div>
  )
}

export default TextFilter;