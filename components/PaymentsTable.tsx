import { FunctionComponent, useEffect, useState } from 'react';
import classnames from 'classnames';

import Image from 'next/image';
import moment from 'moment';
import { Payment } from '../lib/types';

type Props = {
  payments: Array<Payment>;
  handleClick: (payment: Payment) => void;
  isClosed: boolean;
  isLoading: boolean;
  historyPayments: boolean;
  ExpirationPayment: boolean;
};

const PaymentsTable: FunctionComponent<Props> = (props) => {
  const {
    payments,
    isClosed,
    isLoading,
    handleClick,
    historyPayments,
    ExpirationPayment,
  } = props;

  const [rowActive, setRowActive] = useState<number>();
  const [tablePayments, set_TablePayments] = useState(payments);
  useEffect(() => {
    if (historyPayments) {
      let historyPayments = payments.filter(
        (payment) => payment.status == 'approved'
      );
      set_TablePayments(historyPayments);
    } else {
      // moment().format('YYY-MM-DD')
      let current_payments = payments.filter(
        (payment) =>
          payment.status == 'pending' &&
          moment(moment(payment.expiration).format('YYYY-MM-DD')).isSameOrAfter(
            moment().format('YYYY-MM-DD')
          )
      );
      set_TablePayments(current_payments);
    }
  }, [historyPayments, ExpirationPayment, payments]);
  const rowClassnames = (idRow: number, status: String) =>
    classnames(
      'grid grid-cols-5 relative gap-5 border-b-[1px] border-white-primary border-opacity-30 pt-7 pb-7 hover:cursor-pointer hover:text-pink-primary ',
      {
        'text-white-primary hover:text-white-primary bg-pink-primary':
          idRow === rowActive && isClosed,
      },

      {
        'text-white-primary': status === 'approved',
      }
    );
  const payClassnames = (idRow: number, status: String) =>
    classnames(
      ' bg-pink-primary hover:bg-gray-secondary hover:text-white-primary rounded-md px-6 py-2 text-gray-primary font-medium',
      {
        'bg-gray-secondary text-white-primary ':
          idRow === rowActive && isClosed,
      }
      // {
      //   'bg-gray-secondary': status === 'approved',
      // }
    );
  const handleClickRow = (indexRow: number, payment: Payment) => {
    setRowActive(indexRow);
    handleClick(payment);
  };

  return (
    <>
      <div className=' grid grid-cols-5 gap-5 border-b-[1px] border-white-primary pb-5 font-bold'>
        <div>
          <span>Paseador</span>
        </div>
        {/* <div>
          <span>Horas</span>
        </div> */}
        <div>
          <span>Total a pagar</span>
        </div>
        <div>
          <span>Fecha de pago</span>
        </div>
        <div>
          <span>Estado de pago</span>
        </div>
      </div>
      {tablePayments.map(
        ({ status, total_amount, date, id, paseador }, idx) => (
          <div
            key={id}
            className={rowClassnames(idx, status)}
            onClick={() => handleClickRow(idx, tablePayments[idx])}
          >
            <div>
              <p>{paseador.first_name}</p>
            </div>
            {/* <div>
              <p>36</p>
            </div> */}
            <div>
              <p>${total_amount}</p>
            </div>
            <div>{date}</div>

            <div>{status}</div>
            <div className='absolute right-2 top-5'>
              {status == 'approved' ? (
                <button
                  type='button'
                  data-modal-toggle='defaultModal'
                  onClick={() => handleClickRow(idx, tablePayments[idx])}
                  className={payClassnames(idx, status)}
                >
                  Comprobante
                </button>
              ) : (
                <button
                  type='button'
                  data-modal-toggle='defaultModal'
                  onClick={() => handleClickRow(idx, tablePayments[idx])}
                  className={payClassnames(idx, status)}
                >
                  Pagar
                </button>
              )}
            </div>
          </div>
        )
      )}
    </>
  );
};

export default PaymentsTable;
