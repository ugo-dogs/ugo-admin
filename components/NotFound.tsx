import { FunctionComponent } from 'react';

import Image from 'next/image';

import NotFoundImage from '../public/not-found.svg';

const NotFound: FunctionComponent = () => {
  return (
    <div className='max-w-[500px] m-auto flex flex-col items-center text-center'>
      <Image src={NotFoundImage} alt='not found' width={177} height={123} />
      <h1 className='mt-10 font-bold text-3xl'>
        Ups! No encontramos la página que estás buscando
      </h1>
      <button
        // onClick={()=>}
        className='mt-14 min-h-[62px] w-[344px] rounded-xl bg-gradient-to-r from-pink-primary to-pink-secondary font-semibold'
        type='submit'
      >
        Volver al inicio
      </button>
    </div>
  );
};

export default NotFound;
