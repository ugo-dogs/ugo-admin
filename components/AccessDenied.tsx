import { FunctionComponent } from 'react'
import { signIn } from 'next-auth/react'

import Link from 'next/link'

const AccessDenied: FunctionComponent = () => (
  <>
    <h1>Acesso restringido</h1>
    <p>
      <Link legacyBehavior href="/api/auth/signin">
        <a
          onClick={e => {
            e.preventDefault()
            signIn(undefined, { callbackUrl: '/houseParadise' })
          }}
        >
          Debés ingresar con tu usuario y contraseña para poder ver esta página.
        </a>
      </Link>
    </p>
  </>
)

export default AccessDenied
