import { FunctionComponent } from 'react';
import classnames from 'classnames';
import { useState, useRef } from 'react';
import Image from 'next/image';

import CloseWhite from '../public/close-white.svg';
import Pen from '../public/pen.svg';
import  { useRouter } from 'next/router'

import Valoration from '../components/Valoration';
import Turno from './Turno';
import Day from './Day';
import Review from './Review';
import { MdModeEditOutline } from 'react-icons/md';
import { getToken } from 'next-auth/jwt';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ModalWalkerEdit from '../components/ModalWalkerEdit';
import moment from 'moment';
const { NEXT_PUBLIC_API_URL } = process.env;
import Alerts from '../public/alerts.svg';
import { Barrios, Walker } from '../lib/types';
import { result } from 'lodash';
import { BiCheckCircle ,BiChevronRight,BiChevronLeft } from "react-icons/bi";

type Props = {
  handleClose: () => void;
  showSidebar: boolean;
  walker:Walker,
  feeds:Array<Walker>,
  barrios:Array<Barrios>
};

const SidebarFeed: FunctionComponent<Props> = ({
  walker,
  showSidebar,
  feeds,
  barrios,
  handleClose,
}) => {
  const router = useRouter()
  const [activeEdit, setactiveEdit] = useState(false);
  const [itemEdit, setitemEdit] = useState('');
const [showFeed, setshowFeed] = useState(true)
const [walkerFocus, setwalkerFocus] = useState<Walker>()
  const [bank_account, setBank_account] = useState();
  const [modalIsOpen, setIsOpen] = useState(false);
  const cbuRef = useRef<HTMLInputElement>(null);
  const aliasRef = useRef<HTMLInputElement>(null);
  const bankRef = useRef<HTMLInputElement>(null);
  const dniRef = useRef<HTMLInputElement>(null);
  console.log('WALKER',feeds);

  const sidebarClassnames = classnames(
    'top-0 right-0 w-[35vw] bg-gray-primary border-l-2 border-l-white-primary  pt-20 text-white fixed h-full z-40 overflow-y-auto',
    { 'ease-in-out duration-300 translate-x-0': showSidebar },
    { 'translate-x-full': !showSidebar }
  );
  const showPaseador = classnames(
    '',
    { 'ease-in-out duration-300 translate-x-0  visible': !showFeed },
    { 'translate-x-full hidden ease-in-out duration-300 ': showFeed }
  );
  const handleBankSave = async () => {
    setactiveEdit(false);
    setitemEdit('');
    postBankData();
  };

  const postBankData = async () => {
    console.log('entra?');

    // const token = getToken({ req, secret: SECRET! }) as any;
    try {
      const result = await fetch(process.env.NEXT_PUBLIC_API_URL + '/users/' + walker._id, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          // Authorization: `Bearer ${token.jwt}`,
        },
        body: JSON.stringify({
          bank_account: bank_account,
        }),
      });
      const parseResult = await result.json();
      toast.success('Cuenta bancaria guardada!', {
        position: 'bottom-right',
        autoClose: 2200,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'dark',
      });
   
    } catch (e) {
      console.log('e', e);
    }
  };

  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#fff',
    },
  };

  const handleModalClose = () => {
    setIsOpen(false);
  };
  // const fullName = `${walker.first_name} ${walker.last_name}`;

  const approveWalker  = async (walker:Walker)=> {
    try {
      const result = await fetch(process.env.NEXT_PUBLIC_API_URL + '/users/' + walker._id, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          // Authorization: `Bearer ${token.jwt}`,
        },
        body: JSON.stringify({
          confirmed:true,
          verified:true
          ,
        })

      
      });
      const parsedResult = await result.json();
        
      toast.success('Cuenta de paseador aceptada!', {
        position: 'bottom-right',
        autoClose: 2200,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'dark',
      });
setTimeout(() => {
  

    router.reload()
 
}, 1000);      
    } catch (error) {
      toast.error('Error!', {
        position: 'bottom-right',
        autoClose: 2200,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'dark',
      });
    }
  }
  return (
    <div className='flex flex-col items-center justify-center min-w-[100vw] min-h-[100vh] bg-black-primary/80 top-0 left-0 absolute text-sm'>
      {modalIsOpen &&  walkerFocus && <ModalWalkerEdit walker={walkerFocus} handleModalClose={handleModalClose} barrios={barrios} /> }

      <div className={sidebarClassnames}>
      
     { showFeed && (
       <>
             <div className='flex justify-between mb-12 px-12'>
             <span>
             Paseadores &gt; <span className='underline'>{showFeed ? 'Solicitudes': ''}</span>
           </span>
           <button onClick={handleClose} className='pt-1 self-end'>
             <Image src={CloseWhite} alt='close' width={15} height={15} />
           </button>
           </div>
   <div className='flex px-12  flex-row flex-wrap mt-8'>
   {feeds.map((walker:Walker) => 
   <div key={walker._id} className='bg-gray-secondary rounded-md px-5  py-5 w-full flex-grow my-3 justify-between flex flex-row cursor-pointer' onClick={()=> {setwalkerFocus(walker);setshowFeed(false)}}>
     <p className='text-lg'>{walker.first_name + ' '+walker.last_name}</p>
     <BiChevronRight size={35} />
     </div>
   )}
 </div>
 </>
     )}
     {walkerFocus && (
    <div className={showPaseador}>

      
<div className='flex  mb-12 px-12 items-center  cursor-pointer' onClick={()=>setshowFeed(true)}>
<BiChevronLeft size={35} />
  <span>
  Ver todas las solicitudes
  </span>

</div>
<div className='px-12'>
  <div className='flex flex-row flex-1 w-[100%] justify-between items-center'>
    <h2 className='text-pink-primary text-3xl mb-[2px] font-bold'>
  {  `${walkerFocus.first_name} ${walkerFocus.last_name}`}
    </h2>
    <div
      className='flex rounded-full border-[2px] border-pink-primary bg-gray-secondary w-16 h-16  justify-center cursor-pointer'
      onClick={() =>{ setwalkerFocus(walkerFocus); setIsOpen(true)}}
    >
      <Image src={Pen} width={20} height={20}       alt='pen'/>
    </div>
  </div>

  <span>ID Paseador: {walkerFocus._id}</span>
  <div className=' bg-gray-secondary rounded-md py-8 px-5 my-10'>
    <p>{walkerFocus.description || 'No posee descripcion'}</p>
  </div>

  <div className='border-[1px] border-white-primary rounded-lg mt-6'>
    <div className='flex justify-between py-4 px-6 border-b-[1px] border-white-primary'>
      <span>Telefono</span>
      <span>{walkerFocus.phone}</span>
    </div>
    <div className='flex justify-between py-4 px-6 border-b-[1px] border-white-primary'>
      <span>Mail</span>
      <span>{walkerFocus.email}</span>
    </div>
    <div className='flex justify-between py-4 px-6 border-b-[1px]'>
      <span>Zona de paseo elegida</span>
      <span>{walkerFocus.paseador_zone.barrio}</span>
    </div>
    <div className='flex justify-between py-4 px-6 border-b-[1px]'>
      <span>Direccion</span>
      <span>{walkerFocus.direccion}</span>
    </div>
    <div className='flex justify-between py-4 px-6'>
      <span>Fecha de nacimiento</span>
      <span>{walkerFocus.bornDate}</span>
    </div>
  </div>

  <div className='pt-8'>
    <span>Turnos de paseo</span>
    <ul className='flex flex-row mt-6 mb-7'>
      {walkerFocus.turnos.map((turno) => (
        <Turno key={turno.id} turno={turno} onPress={() => { } } selected={[]}/>
      ))}
    </ul>
  </div>
  <div>
    <span>Cuenta bancaria</span>
    <div className='border-[1px] border-white-primary rounded-lg mt-6 mb-6'>
      <div className='flex justify-between py-4 px-6 border-b-[1px] border-white-primary relative'>
        <span>CBU</span>
        <p className='bg-gray-primary outline-none'>
          {walkerFocus.bank_account?.cbu}
        </p>
      </div>
      <div className='flex justify-between py-4 px-6 relative'>
        <span>ALIAS</span>
        <p className='bg-gray-primary outline-none'>
          {walkerFocus.bank_account?.alias}
        </p>
      </div>

      <div className='flex justify-between py-4 px-6 border-t-[1px] border-white-primary relative'>
        <span>BANCO</span>
        <p className='bg-gray-primary outline-none'>
          {walkerFocus.bank_account?.bank_name}
        </p>
      </div>
      <div className='flex justify-between py-4 px-6 border-t-[1px] border-white-primary relative'>
        <span>DNI</span>
        <p className='bg-gray-primary outline-none'>
          {walkerFocus.bank_account?.dni}
        </p>
      </div>
    </div>
    {activeEdit && (
      <div className=' justify-end  flex'>
        <button
          className='bg-pink-secondary py-4 px-8 rounded-md'
          onClick={() => handleBankSave()}
        >
          GUARDAR
        </button>
      </div>
    )}
  </div>
  <div>
    <span>Dias de paseo</span>
    <ul className='flex flex-row mt-6'>
      {walkerFocus.days_available.days.map((day) => (
        <Day key={day} day={day} selected={[]} onPress={()=>{}} />
      ))}
    </ul>
  </div>

</div>

<div className='flex   mb-20 mt-8  sticky bottom-0  w-[100%] left-0 right-0 bg-black-primary border-white-secondary border-t-[1px] px-12 py-5'>
    <div
      className='flex flex-row items-center  flex-1 py-6 justify-center rounded-lg focus:outline-none bg-pink-secondary cursor-pointer mr-4 '
      onClick={() => approveWalker(walkerFocus)}
    >
      <BiCheckCircle
       size={25}
      />

      <p className='pl-3'>Aprobar solicitud</p>
    </div>
    <div
      className='flex flex-row items-center flex-1 justify-center rounded-lg focus:outline-none bg-gray-secondary cursor-pointer border-[1px] border-pink-primary ml-4'
      onClick={() => {}}
    >
      <p className='pl-3'>Cancelar</p>
    </div>
  </div>

</div>
     )}
        <ToastContainer
  position='bottom-right'
  autoClose={5000}
  hideProgressBar={false}
  newestOnTop={false}
  closeOnClick
  rtl={false}
  pauseOnFocusLoss
  draggable
  pauseOnHover
/>
      </div>

    </div>
  );
};

export default SidebarFeed;
