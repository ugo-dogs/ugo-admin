import { FunctionComponent, useState } from 'react';
import Image from 'next/image';

import type { Dog, ReservesHP } from '../lib/types';
import Close from '../public/close-white.svg';
import Vaccine from '../public/content/vaccine.svg';

import moment from 'moment';
import 'moment/locale/es';
import  { Popconfirm } from 'antd';
import { useQueryClient } from 'react-query';
import { ToastContainer, toast } from 'react-toastify';
const { NEXT_PUBLIC_API_URL } = process.env;

type Props = {
  detail: ReservesHP;
  handleCloseDetail: () => void;
  showEditReserve: (id: string) => void;
};

const ReserveDetail: FunctionComponent<Props> = (props) => {
  const { detail, handleCloseDetail,showEditReserve } = props;
  const [Dog, setDog] = useState<Dog>();
  const [showModal, setshowModal] = useState(false);
  const [ModalPaseador, setModalPaseador] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const queryClient = useQueryClient();

  // const [paseo, set] = useState<Dog>()
  console.log('details', detail);
  const cancel = () => {
    setOpen(false);
  };
  const [showEdit, setshowEdit] = useState('');
  const handleEditPaseo = (paseo: Dog) => {
    console.log('paseo', paseo);

    setDog(paseo);

  };
  const confirm =  async (id:string) => {
    console.log('id',id);
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/reserves-hps/${id}`,{
        method:'DELETE',
        headers: {
          'Content-Type': 'application/json',
          // Authorization: `Bearer ${token.jwt}`,
        },

      })
      console.log('response',response);
        queryClient.invalidateQueries('reserves')
      handleCloseDetail()
      setOpen(false)

    } catch (error) {
      console.log('error',error); 
    }
  }

  const [selectLoading, setSelectLoading] = useState(false);

  const handleStatusChange = async (event: React.ChangeEvent<HTMLSelectElement>) => {
    try {
      setSelectLoading(true); // Set loading state to true

      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/reserves-hps/${detail.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          // Authorization: `Bearer ${token.jwt}`,
        },
        body: JSON.stringify({
          status: event.target.value,
        }),
      });

      console.log('response', response);

      toast.success('El estado de la reserva fue editado con éxito', {
        position: 'bottom-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });

      queryClient.invalidateQueries('reserves')

    } catch (error) {
      console.log('error', error);

      toast.error('Ocurrio un error al editar la reserva', {
        position: 'bottom-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });


    } finally {
      setSelectLoading(false); // Set loading state to false after the call is complete
    }
  }

  const statusOptions = ['Consulta', 'Anticipo Pagado', 'A retirar', 'En House Paradise', 'Finalizado']
  
  return (
    <div className='p-10 w-60-ns h-screen overflow-y-auto border-l-[3px] border-white-primary hp-reserve-details'>
      <div className={` ${Dog && (showModal || ModalPaseador) && 'opacity-10'}`} >
        <div className='flex justify-between '>
          <p className='text-sm'>
            Total estadia:{' '}
            {moment(detail.aob_date_end).diff(detail.aob_date_start, 'days')}{' '}
            días
          </p>
          <Image
            src={Close}
            alt='uGo admin logo'
            width={18}
            height={18}
            onClick={handleCloseDetail}
            className='hover:cursor-pointer'
          />
        </div>
        <div className='flex items-center justify-between center flex-row  mt-10'>
          <p className='text-3xl font-bold'>
            Reserva de
            <span className='text-pink-primary'> {detail.dog_name}</span>
          </p>
          <div className='flex flex-row items-center gap-3'>
          <Popconfirm
          placement='bottom'
          onConfirm={() => confirm(detail.id)}
          title='Estás seguro que querés borrar esta reserva?'
          description='Se eliminara la reserva'
          okText='Eliminar'
          cancelText='Cancelar'
          className='cursor-pointer'
          okButtonProps={{
            loading: confirmLoading,
            danger: true,
            className:  'bg-primaryRed text-white rounded-md px-2 py-1 hover:bg-primaryRed ',
          }}
          >
          <div className='border-[1px] border-red-primary bg-red-primary rounded-lg py-4 px-4'>
            <p className='text-black'>Borrar Reserva</p>
          </div>
          </Popconfirm>
                
          <button 
          onClick={()=>showEditReserve(detail.id)}
          className='border-[1px] border-white-primary rounded-lg bg-gray-secondary  py-4 px-4'>
            <p>Editar Reserva</p>
          </button>
         
          </div>
        </div>
        <p className='text-sm mt-2'>
          Dueño: {detail.owner_name} {detail.owner_surname}
        </p>
        <div className='border-[1px] border-white-primary rounded-lg mt-6'>

        <p className='p-4 border-b-[1px] flex justify-between'>
            Dirección:
            <span>{detail.owner_address}</span>
          </p>

          <p className='p-4 border-b-[1px] flex justify-between'>
            Provincia:
            <span>{detail.provincia}</span>
          </p>

          <p className='p-4 border-b-[1px] flex justify-between'>
            Estado de reserva:
            <div className="flex items-center gap-2">
              <select className="input-reset bg-transparent border-none" onChange={(event) => handleStatusChange(event)} disabled={selectLoading}>
                  <option className="black">{detail.status}</option>

                  {statusOptions.map((s) => (
                    s !== detail.status &&
                      <option className="black" key={s} value={s}>
                        {s}
                      </option>
                  ))}
              </select>

              <Image
              src={require(`../public/content/arrow-simple-down.svg`)}
              height={10}
              width={10}
              alt='arrow icon'
          />
            </div>
         
          </p>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Fecha de ingreso:{' '}
            <span>{moment(detail.aob_date_start).format('LL')}</span>
          </p>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Fecha de Egreso:{' '}
            <span>{moment(detail.aob_date_end).format('LL')}</span>
          </p>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Estadia:{' '}
            <span>
              {moment(detail.aob_date_end).diff(detail.aob_date_start, 'days')}{' '}
              días
            </span>
          </p>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Valor reserva:{' '}
            <span>
              {new Intl.NumberFormat('es-AR', {
                style: 'currency',
                currency: 'ARS',
                maximumSignificantDigits: 3,
              }).format(detail.aob_price)}
            </span>
          </p>

          {detail.discount_cupon && (
              <p className='p-4 flex justify-between'>
                <span className="flex items-center">Cupón utilizado: <span className="ml-5 uppercase p-2 rounded-md text-black-primary bg-pink-primary text-xs font-bold">{detail.discount_cupon}</span>
                </span>
                <span>
                  {new Intl.NumberFormat('es-AR', {
                    style: 'currency',
                    currency: 'ARS',
                    maximumSignificantDigits: 3,
                  }).format(detail.discount_amount)
                  }      
                </span>
              </p>
            )}

          {/* <p className='p-4 border-b-[1px] flex justify-between'>
            Telefono Dueño: <span>{detail.owner_phone}</span>
          </p>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Email Dueño: <span>{detail.owner_email}</span>
          </p> */}
        </div>
      </div>

      <p className='mt-8 text-2xl font-bold'>Datos del perro</p>
      <div
        className={` ${Dog && (showModal || ModalPaseador) && 'opacity-10'}`}
      >
        <div className='border-[1px] border-white-primary rounded-lg mt-6 reserve-details-inner'>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Nombre:
            <span>{detail.dog_name}</span>
            {/* Cantidad de perros: <span>{detail.dogs.length}</span> */}
          </p>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Años: <span>{detail.dog_age}</span>
          </p>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Raza: <span>{detail.dog_raza}</span>
          </p>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Genero:
            <span>{detail.dog_genre}</span>
          </p>
          {detail.dog_behaviour && (
          <p className='p-4 border-b-[1px] flex justify-between'>
            Comportamiento: <span>{detail.dog_behaviour}</span>
          </p>
          )}

          {detail.dog_deworming && (
          <p className='p-4 border-b-[1px] flex justify-between'>
            Desparasitado: <span>{detail.dog_deworming ? 'Si' : 'No'}</span>
          </p>
          )}

          {detail.dog_social && (
          <p className='p-4 border-b-[1px] flex justify-between'>
            Sociabilización: <span>{detail.dog_social}</span>
          </p>
          )}

          {detail.dog_castrado && (
          <p className='p-4 border-b-[1px] flex justify-between'>
            Castrado: <span>{detail.dog_castrado ? 'Si' : 'No'}</span>
          </p>
          )}

          {detail.dog_food && (
          <p className='p-4 border-b-[1px] flex justify-between'>
            Comida: <span>{detail.dog_food}</span>
          </p>
          )}

          {detail.dog_bite && (
          <p className='p-4 border-b-[1px] flex justify-between'>
            Mordió a otros perros: <span>{detail.dog_bite}</span>
            </p>
          )}

          {detail.dog_swim && (
          <p className='p-4 border-b-[1px] flex justify-between'>
            Sabe nadar: <span>{detail.dog_swim}</span>
            </p>
          )}

          {detail.dog_comments && (
          <p className='p-4 border-b-[1px] flex justify-between'>
            Comentarios: <span className="w-80">{detail.dog_comments}</span>
          </p>
          )}

          <p className='p-4 border-b-[1px] flex justify-between'>
            Última fecha de celo:{detail.date_celo}
            <span>
              {detail.date_celo === '2022-12-18'
                ? 'Está castrado'
                : detail.date_celo}
            </span>
          </p>
          

          <p className='p-4 flex justify-between'>
            <span className='flex items-center'>
              <Image
                src={Vaccine}
                alt='vaccine logo'
                width={31}
                height={32}
                className='pr4'
              />{' '}
              Vacuna:
            </span>
            <span>{detail.dog_vaccine ? 'Si' : 'No'}</span>
          </p>
        </div>
      </div>
      <p className='mt-8 text-2xl font-bold'>Datos del Dueño</p>
      <div
        className={` ${Dog && (showModal || ModalPaseador) && 'opacity-10'}`}
      >
        <div className='border-[1px] border-white-primary rounded-lg mt-6'>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Nombre:
            <span>{detail.owner_name}</span>
            {/* Cantidad de perros: <span>{detail.dogs.length}</span> */}
          </p>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Apellido: <span>{detail.owner_surname}</span>
          </p>
          <p className='p-4 border-b-[1px] flex justify-between'>
            DNI: <span>{detail.owner_dni}</span>
          </p>
          <p className='p-4 border-b-[1px] flex justify-between'>
            Email:
            <span>{detail.owner_email}</span>
          </p>
          <p className='p-4 flex justify-between'>
            Telefono: <span>{detail.owner_phone}</span>
          </p>
        </div>
      </div>

      <ToastContainer
        position='bottom-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />

    </div>
  );
};

export default ReserveDetail;
