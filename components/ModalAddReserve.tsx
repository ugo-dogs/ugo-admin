import React, { useState, useEffect } from 'react';
import classnames from 'classnames';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Image from 'next/image';
import Close from '../public/close-white.svg';
import OwnerDataInputs from './HouseParadise/OwnerDataInputs';
import DogDataInputs from './HouseParadise/DogDataInputs';
import TripDataInputs from './HouseParadise/TripDataInputs';
import PaymentDataInputs from './HouseParadise/PaymentDataInputs';
import { EditReserve } from '../lib/types';
import { useQueryClient } from 'react-query';
// const { NEXT_PUBLIC_API_URL, SECRET } = process.env;

interface Props {
  input: {
    title: string;
    icon: string;
    fields?: {
      type: string;
      value: string;
      placeholder: string;
      section: string;
    }[];
    inputsOwner?: {
      type: string;
      value: string;
      placeholder: string;
    }[];
    inputDogs?: {
      type: string;
      value: string;
      placeholder: string;
      section: string;
      selectOptions?: {
        value: string;
        title: string;
      }[];
    }[];
    social?: {
      desc: string;
      title: string;
    }[];
    healthDogs?: {
      question: string;
    }[];
    social_comportamiento?: {
      id: string;
      questions: string;
      options: {
        title: string;
        value: string;
        id: string;
      }[];
    }[];
  }[];
  isEditMode: boolean;
  reserveData?:EditReserve;
  hideModal: () => void;
  editReserve: any;
  handleCloseDetail: () => void;
}

enum MenuOptions {
  OwnerData = 'Datos dueño',
  DogData = 'Datos perro',
  TripData = 'Datos estadia',
  PaymentData = 'Pagos',
  None = '',
}

const ModalAddReserve: React.FC<Props> = ({
  hideModal,
  input,
  editReserve,
  isEditMode,
  reserveData,
  handleCloseDetail
}) => {
  const [menuActive, setMenuActive] = useState<MenuOptions>(
    MenuOptions.OwnerData
  );
  
  const [dataOwner, setDataOwner] = useState<any>(isEditMode && reserveData ? reserveData.ownerData : {});
  const [dataDog, setdataDog] = useState<any>(isEditMode && reserveData ? reserveData.dogData : {});
  const [dataTrip, setdataTrip] = useState<any>(isEditMode && reserveData ? reserveData.dataTrip : {});
  const [dataPayment, setDataPayment] = useState({});
    const queryClient = useQueryClient()
  const menuClassnames = (current: typeof input[0]) =>
    classnames(
      'text-white-primary rounded-md h-14 items-center justify-center flex cursor-pointer mr-[-2px] test',
      {
        'bg-pink-secondary': menuActive === current.title,
        'bg-gray-secondary': menuActive !== current.title,
      }
    );
    
  const handleSubmit = async () => {

    const bodyObject = {
      owner_name: dataOwner.nombre,
      owner_surname: dataOwner.apellido,
      owner_phone: dataOwner.telefono,
      owner_email: dataOwner.mail,
      owner_dni: dataOwner.dni,
      dog_genre: dataDog['Género'],
      dog_raza: dataDog.raza,
      dog_social: dataDog.social,
      dog_age: dataDog.edad,
      dog_name: dataDog.nombre,
      dog_castrado: dataDog.castrado,
      date_celo: dataDog['¿Cuál fue la fecha del último celo?'],
      dog_behaviour: dataDog['¿Tiene alguno de estos comportamientos?'],
      dog_vaccine: dataDog.dog_vaccine,
      dog_deworming: dataDog.dog_deworming,
      bite_dogs: dataDog['Mordío otros perros?'],
      aob_date_start: dataTrip.aob_date_start,
      aob_date_end: dataTrip.aob_date_end,
      aob_price: dataTrip.aob_price,
      aob_purchased: dataTrip.aob_purchased,
    };
    
let q = isEditMode ? `${reserveData?.id}` : ''

    try {
      const result = await fetch(
        'https://u-go-backend-deveop-lc9t2.ondigitalocean.app/reserves-hps/'+q,
        {
          method: isEditMode ? 'PUT': 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(bodyObject),
        }
      );

      const data = await result.json();
      queryClient.invalidateQueries('reserves')
      handleCloseDetail()
     hideModal()
      toast.success( editReserve? 'Reserva actualizada': 'Reserva creada con éxito', {
        position: 'bottom-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      
    } catch (error) {
      console.log(error);
      toast.error('Ocurrio un error al generar la reserva', {
        position: 'bottom-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };
  useEffect(() => {
    if (isEditMode && reserveData) {
      // Establece los datos de la reserva en los estados
      setDataOwner(reserveData.ownerData || {});
      setdataDog(reserveData.dogData || {});
      // ... y así sucesivamente para los otros datos
    }
  }, [isEditMode, reserveData]);

  return (
    <div
      className='fixed inset-0 overflow-y-auto z-50 sm:px-20 sm:py-16'
      aria-labelledby='modal-title'
      role='dialog'
      aria-modal='true'
    >
      <div className="z-40 relative h-full overflow-y-auto inset-0">
        <div className='flex items-center justify-center text-center sm:p-0 z-50 h-full'>
          {/* <span className='hidden sm:inline-block sm:align-middle sm:h-screen' aria-hidden='true'>&#8203;</span> */}
          <div className='m-auto reserve-container h-full relative p-5 sm:p-8 inline-block align-bottom bg-gray-primary sm:border-pink-primary sm:border-[1px] sm:rounded-lg text-left shadow-xl transform transition-all sm:align-middle w-full overflow-y-scroll'>
            <div
              onClick={hideModal}
              className='absolute right-[15px] top-[15px] z-100 flex items-center justify-center w-[40px] h-[40px] rounded-full hover:cursor-pointer'
            >
              <Image
                src={Close}
                alt='close modal'
                width={18}
                height={18}
                className='hover:cursor-pointer'
              />
            </div>
            <div>
              <h1 className='text-3xl font-medium font-faro'>
                {editReserve ? 'Editar estadía de ' : 'Agregar reserva'}

                <span className='text-pink-primary text-3xl mb-[2px] font-bold'>
                  {/* {walker.first_name} */}
                </span>
              </h1>
            </div>
            <div className='flex flex-row pt-6 relative reserve-header'>
              {input.map((menu_item: typeof input[0]) => {
                return (
                  <div className='flex-1 w-[100%]' key={menu_item.title}>
                    <div
                      className={` ${menuClassnames(menu_item)} flex-col justify-center sm:flex-row`}
                      onClick={() =>
                        setMenuActive(menu_item.title as MenuOptions)
                      }
                    >
                      <Image
                        src={require(`../public/${menu_item.icon}.svg`)}
                        height={20}
                        width={20}
                        alt='menu icon'
                      />

                      <h3 className='font-medium font-laussane text-center sm:text-left sm:ml-3'>{menu_item.title}</h3>
                    </div>
                    {menuActive === menu_item.title && (
                      <div className='reserve-content pt-8 flex flex-row flex-wrap flex-1 left-0 w-[100%] absolute'>
                        {menuActive === MenuOptions.OwnerData && (
                          <OwnerDataInputs
                            inputs={menu_item.inputsOwner}
                            dataOwner={dataOwner}
                            setDataOwner={setDataOwner}
                          />
                        )}
                        {menuActive === MenuOptions.DogData && (
                          <DogDataInputs
                            inputs={menu_item.inputDogs}
                            social={menu_item.social}
                            healthDogs={menu_item.healthDogs}
                            social_comportamiento={
                              menu_item.social_comportamiento
                            }
                            dataDog={dataDog}
                            setDataDog={setdataDog}
                          />
                        )}
                        {menuActive === MenuOptions.TripData && (
                          <TripDataInputs
                            inputs={menu_item.inputDogs}
                            social={menu_item.social}
                            healthDogs={menu_item.healthDogs}
                            social_comportamiento={
                              menu_item.social_comportamiento
                            }
                            dataTrip={dataTrip}
                            setdataTrip={setdataTrip}
                          />
                        )}
                        {menuActive === MenuOptions.PaymentData && (
                          <PaymentDataInputs
                            dataTrip={dataTrip}
                            setdataTrip={setdataTrip}
                          />
                        )}
                      </div>
                    )}
                  </div>
                );
              })}
            </div>

            <div className='fixed bottom-0 sm:bottom-8 left-0 sm:right-15 flex flex-row items-center bg-black-secondary z-50 w-full sm:w-max justify-between border-t-2 border-t-black-secondary'>
              <div
                className='bg-pink-primary px-12 py-4 sm:rounded-md cursor-pointer'
                onClick={handleSubmit}
              >
                <p className='font-medium text-lg'>Guardar cambios</p>
              </div>
              <div className='sm:ml-10 cursor-pointer m-auto' onClick={hideModal}>
                <p className='text-lg text-center border-b-[1px]'>
                  Cancelar
                </p> 
              </div>
            </div>
          </div>
        </div>
        <ToastContainer
          position='bottom-right'
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>

      <div className="absolute z-10 bg-black-primary opacity-70 w-full h-full top-0 left-0" onClick={hideModal} ></div>
    </div>
  );
};

export default ModalAddReserve;
function handleModalClose(): void {
  throw new Error('Function not implemented.');
}
