import { FunctionComponent } from 'react';
import classnames from 'classnames';
import { useState, useRef } from 'react';
import Image from 'next/image';

import CloseWhite from '../public/close-white.svg';
import Pen from '../public/pen.svg';

import Valoration from '../components/Valoration';
import Turno from './Turno';
import Day from './Day';
import Review from './Review';
import { MdModeEditOutline } from 'react-icons/md';
import { getToken } from 'next-auth/jwt';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ModalWalkerEdit from '../components/ModalWalkerEdit';
import { Barrios, Walker } from '../lib/types';

const { NEXT_PUBLIC_API_URL } = process.env;

type Props = {
  handleClose: () => void;
  showSidebar: boolean;
  reviews: Array<{
    id: string;
    comment: string;
  }>;
  walker: Walker;
  barrios:Array<Barrios>

};

const Sidebar: FunctionComponent<Props> = ({
  walker,
  reviews,
  showSidebar,
  barrios,
  handleClose,
}) => {
  
  const [activeEdit, setactiveEdit] = useState(false);
  const [itemEdit, setitemEdit] = useState('');

  const [bank_account, setBank_account] = useState(walker.bank_account);
  const [modalIsOpen, setIsOpen] = useState(false);
  const cbuRef = useRef<HTMLInputElement>(null);
  const aliasRef = useRef<HTMLInputElement>(null);
  const bankRef = useRef<HTMLInputElement>(null);
  const dniRef = useRef<HTMLInputElement>(null);

  const sidebarClassnames = classnames(
    'top-0 right-0 w-[35vw] bg-gray-primary border-l-2 border-l-white-primary px-12 pt-20 text-white fixed h-full z-40 overflow-y-auto pb-14',
    { 'ease-in-out duration-300 translate-x-0': showSidebar },
    { 'translate-x-full': !showSidebar }
  );
  const handleBankSave = async () => {
    setactiveEdit(false);
    setitemEdit('');
    postBankData();
  };

  const postBankData = async () => {
    console.log('entra?');

    // const token = getToken({ req, secret: SECRET! }) as any;
    try {
      const result = await fetch(process.env.NEXT_PUBLIC_API_URL + '/users/' + walker._id, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          // Authorization: `Bearer ${token.jwt}`,
        },
        body: JSON.stringify({
          bank_account: bank_account,
        }),
      });
      const parseResult = await result.json();
      toast.success('Cuenta bancaria guardada!', {
        position: 'bottom-right',
        autoClose: 2200,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'dark',
      });
      console.log('aca', parseResult);
    } catch (e) {
      console.log('e', e);
    }
  };


  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#fff',
    },
  };

  const handleModalClose = () => {
    setIsOpen(false)
  }
  const fullName = `${walker.first_name} ${walker.last_name}`;
  return (
    <div className='flex flex-col items-center justify-center min-w-[100vw] min-h-[100vh] bg-black-primary/80 top-0 left-0 absolute text-sm overflow-y-scroll '>
    {modalIsOpen &&  <ModalWalkerEdit walker={walker} handleModalClose={handleModalClose} barrios={barrios} /> }

      <div className={sidebarClassnames}>
        <div className='flex justify-between mb-12'>
          <span>
            Paseadores &gt; <span className='underline'>{fullName}</span>
          </span>
          <button onClick={handleClose} className='pt-1'>
            <Image src={CloseWhite} alt='close' width={15} height={15} />
          </button>
        </div>
        <div>
          <div className='flex flex-row flex-1 w-[100%] justify-between items-center'>
            <h2 className='text-pink-primary text-3xl mb-[2px] font-bold'>
              {fullName}
            </h2>
            <div
              className='flex rounded-full border-[2px] border-pink-primary bg-gray-secondary w-16 h-16  justify-center cursor-pointer'
              onClick={() => setIsOpen(true)}
            >
              <Image src={Pen} width={20} height={20} alt=""/>
            </div>
          </div>

          <span>ID Paseador: {walker._id}</span>
          <div className='flex flex-row mt-6'>
            <Valoration fromReview valoration={walker.valoration} />
          </div>
          <div className='border-[1px] border-white-primary rounded-lg mt-6'>
            <div className='flex justify-between py-4 px-6 border-b-[1px] border-white-primary'>
              <span>Paseos de este mes</span>
              <span>{walker.turnos.length}</span>
            </div>
            <div className='flex justify-between py-4 px-6'>
              <span>Zona</span>
              <span>{walker.paseador_zone.barrio}</span>
            </div>
          </div>
          <div className='flex justify-between py-4 px-6 mt-9 mb-10 bg-gray-secondary rounded-lg shadow'>
            <span>Total a pagar el 15/03/2022</span>
            <span>$4500</span>
          </div>
          <div>
            <span>Turnos de paseo</span>
            <ul className='flex flex-row mt-6 mb-7'>
            
              {walker.turnos.map((turno) => (
        <Turno key={turno.id} turno={turno} onPress={() => { } } selected={[]}/>
      ))}
            </ul>
          </div>
          <div>
            <span>Cuenta bancaria</span>
            <div className='border-[1px] border-white-primary rounded-lg mt-6 mb-6'>
              <div className='flex justify-between py-4 px-6 border-b-[1px] border-white-primary relative'>
                <span>CBU</span>
                <input
                  type={'number'}
                  ref={cbuRef}
                  name='cbu'
                  value={bank_account?.cbu}
                  className='bg-gray-primary outline-none appearance-none'
                  // disabled={itemEdit !== 'cbu' && activeEdit !== true}
                  disabled={!activeEdit && itemEdit !== 'cbu'}
                  onChange={(data) => {
                    //  let cbu_number = parseInt(data.target.value)
                    setBank_account({
                      cbu: parseInt(data.target.value),
                      alias: bank_account.alias,
                      dni: bank_account.dni,
                      bank_name: bank_account.bank_name,
                    });
                  }}
                />
                <button
                  className='absolute right-3'
                  onClick={() => {
                    cbuRef.current?.focus();
                    setactiveEdit(!activeEdit);
                    setitemEdit(itemEdit == 'cbu' ? '' : 'cbu');
                  }}
                >
                  {itemEdit == 'cbu' && activeEdit ? (
                    'X'
                  ) : (
                    <MdModeEditOutline size={18} />
                  )}
                </button>
              </div>
              <div className='flex justify-between py-4 px-6 relative'>
                <span>ALIAS</span>
                <input
                  type={'text'}
                  ref={aliasRef}
                  value={bank_account?.alias}
                  name='alias'
                  className='bg-gray-primary outline-none'
                  // disabled={itemEdit !== 'alias' && activeEdit !== true}
                  disabled={!activeEdit && itemEdit !== 'alias'}
                  onChange={(data) => {
                    //  let cbu_number = parseInt(data.target.value)
                    setBank_account({
                      cbu: bank_account.cbu,
                      alias: data.target.value,
                      dni: bank_account.dni,
                      bank_name: bank_account.bank_name,
                    });
                  }}
                />
                <button
                  className='absolute right-3'
                  onClick={() => {
                    aliasRef.current?.focus();
                    setactiveEdit(!activeEdit);
                    setitemEdit(itemEdit == 'alias' ? '' : 'alias');
                  }}
                >
                  {itemEdit == 'alias' && activeEdit ? (
                    'X'
                  ) : (
                    <MdModeEditOutline size={18} />
                  )}
                </button>
              </div>

              <div className='flex justify-between py-4 px-6 border-t-[1px] border-white-primary relative'>
                <span>BANCO</span>
                <input
                  type={'text'}
                  ref={bankRef}
                  name='bank'
                  value={bank_account?.bank_name}
                  className='bg-gray-primary outline-none '
                  disabled={!activeEdit && itemEdit !== 'bank'}
                  onChange={(data) => {
                    //  let cbu_number = parseInt(data.target.value)
                    setBank_account({
                      cbu: bank_account.cbu,
                      alias: bank_account.alias,
                      dni: bank_account.dni,
                      bank_name: data.target.value,
                    });
                  }}
                />
                <button
                  className='absolute right-3'
                  onClick={() => {
                    bankRef.current?.focus();
                    setactiveEdit(!activeEdit);
                    setitemEdit(itemEdit == 'bank' ? '' : 'bank');
                  }}
                >
                  {itemEdit == 'bank' && activeEdit ? (
                    'X'
                  ) : (
                    <MdModeEditOutline size={18} />
                  )}
                </button>
              </div>
              <div className='flex justify-between py-4 px-6 border-t-[1px] border-white-primary relative'>
                <span>DNI</span>
                <input
                  type={'number'}
                  value={bank_account?.dni}
                  ref={dniRef}
                  name='dni'
                  className='bg-gray-primary outline-none'
                  disabled={!activeEdit && itemEdit !== 'dni'}
                  onChange={(data) => {
                    //  let cbu_number = parseInt(data.target.value)
                    setBank_account({
                      cbu: bank_account.cbu,
                      alias: bank_account.alias,
                      dni: parseInt(data.target.value),
                      bank_name: bank_account.bank_name,
                    });
                  }}
                />
                <button
                  className='absolute right-3'
                  onClick={() => {
                    dniRef.current?.focus();
                    setactiveEdit(!activeEdit);
                    setitemEdit(itemEdit == 'dni' ? '' : 'dni');
                  }}
                >
                  {itemEdit == 'dni' && activeEdit ? (
                    'X'
                  ) : (
                    <MdModeEditOutline size={18} />
                  )}
                </button>
              </div>
            </div>
            {activeEdit && (
              <div className=' justify-end  flex'>
                <button
                  className='bg-pink-secondary py-4 px-8 rounded-md'
                  onClick={() => handleBankSave()}
                >
                  GUARDAR
                </button>
              </div>
            )}
          </div>
          <div>
            <span>Dias de paseo</span>
            <ul className='flex flex-row mt-6'>
              {walker.days_available.days.map((day) => (
                <Day key={day} day={day} selected={[]} onPress={()=>{}} />
              ))}
            </ul>
          </div>
          {reviews.length >= 1 && (
            <div className='mt-5'>
              <span className='text-xl'>Reseñas</span>
              <ul className='mt-5'>
                {reviews!.map((review) => (
                  <Review key={review.id} comment={review.comment} />
                ))}
              </ul>
            </div>
          )}
        </div>
        <ToastContainer
          position='bottom-right'
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>
    </div>
  );
};

export default Sidebar;
