import { FunctionComponent, useState } from 'react';
import classnames from 'classnames';

import { ReservesHP } from '../lib/types';

import moment from 'moment';
import 'moment/locale/es';
import { DataGrid, GridRowParams } from '@mui/x-data-grid';
import { mySpanishTranslations } from '../lib/utils/DatGrid';

import { isMobile } from 'react-device-detect';

type Props = {
  reserves: Array<ReservesHP>;
  handleClick: (reserve: ReservesHP) => void;
  isClosed: boolean;
  isLoading: boolean;
};

const TableHP: FunctionComponent<Props> = (props) => {
  const { reserves, isClosed, isLoading, handleClick } = props;
  moment.locale('es');

  const [rowActive, setRowActive] = useState<number>();

  const handleClickRow = (indexRow: number, reserve: ReservesHP) => {
    setRowActive(indexRow);
    console.log('reserve', reserve);

    handleClick(reserve);
  };
  const columnStyle = {
    headeralign: 'center',
    headerClassName: 'text-white-primary',
    flex: 1,
    sortable: true,
  };

  function formatDate(date: any) {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() + 1);
    return newDate.toLocaleDateString('es-AR', {
      day: '2-digit',
      month: '2-digit' as const,
      year: 'numeric' as const,
    });
  }
  const columns = [
    {
      field: 'fechaIngreso',
      // comparator: compareDates,
      valueFormatter: (params: any) => formatDate(params.value),

      headerName: 'Fecha Ingreso',
      ...columnStyle,
    },
    {
      field: 'fechaEgreso',
      headerName: 'Fecha Egreso',
      // comparator: compareDates,
      valueFormatter: (params: any) => formatDate(params.value),
      ...columnStyle,
    },
    {
      field: 'cantidadDias', 
      headerName: 'Cantidad de dias',
      ...columnStyle,
    },
    {
      field: 'dueño',
      headerName: 'Dueño',
      ...columnStyle,
    },
    {
      field: 'perro',
      headerName: 'Perro',
      ...columnStyle,
    },
    {
      field: 'estado',
      headerName: 'Estado',
      ...columnStyle,
    },
    {
      field: 'precio',
      headerName: 'Precio',
      ...columnStyle,
    },
  ];

  const columnsMobile = [
    {
      field: 'perro',
      headerName: 'Perro',
      ...columnStyle,
    },

    {
      field: 'fechaIngreso',
      // comparator: compareDates,
      valueFormatter: (params: any) => formatDate(params.value),

      headerName: 'Fecha Ingreso',
      ...columnStyle,
    },
    {
      field: 'fechaEgreso',
      headerName: 'Fecha Egreso',
      // comparator: compareDates,
      valueFormatter: (params: any) => formatDate(params.value),
      ...columnStyle,
    },
    // {
    //   field: 'cantidadDias', 
    //   headerName: 'Cantidad de dias',
    //   ...columnStyle,
    // },
    // {
    //   field: 'estado',
    //   headerName: 'Estado',
    //   ...columnStyle,
    // },
    {
      field: 'precio',
      headerName: 'Precio',
      ...columnStyle,
    },
  ];

  const rows = reserves.map((reserve, idx) => {
    return {
      id: reserve.id,
      // moment(reserve.aob_date_start).format('  ')
      fechaIngreso: reserve.aob_date_start,
      fechaEgreso: reserve.aob_date_end,
      cantidadDias: moment(reserve.aob_date_end).diff(
        reserve.aob_date_start,
        'days'
      ),
      dueño: reserve.owner_name + ' ' + reserve.owner_surname,
      perro: reserve.dog_name,
      estado: reserve.status,
      precio: new Intl.NumberFormat('es-AR', {
        style: 'currency',
        currency: 'ARS',
        maximumSignificantDigits: 3,
      }).format(reserve.aob_price),
    };
  });

  const rowsMobile = reserves.map((reserve, idx) => {
    return {
      id: reserve.id,
      // moment(reserve.aob_date_start).format('  ')
      fechaIngreso: reserve.aob_date_start,
      fechaEgreso: reserve.aob_date_end,
      cantidadDias: moment(reserve.aob_date_end).diff(
        reserve.aob_date_start,
        'days'
      ),
      dueño: reserve.owner_name + ' ' + reserve.owner_surname,
      perro: reserve.dog_name,
      estado: reserve.status,
      precio: new Intl.NumberFormat('es-AR', {
        style: 'currency',
        currency: 'ARS',
        maximumSignificantDigits: 3,
      }).format(reserve.aob_price),
    };
  });

  return (
    <>
      <div className='w-full h-[1500px] overflow-y-scroll pb-24 sm:pb-0 flex'>
        <DataGrid
          // hideFooter
            rows={isMobile ? rowsMobile : rows}
            autoHeight={false}
            rowHeight={80}
            pagination
            localeText={mySpanishTranslations}
            className='w-[100%] h-full overflow-y-scroll'
        
            columns={isMobile ? columnsMobile : columns}
            getRowClassName={(params) =>
              classnames(
                'text-white-primary  hover:cursor-pointer hover:bg-pink-primary hover:font-bold ',
                {
                  'bg-pink-primary': params.id === rowActive,
                }
            )
          }
   
          onRowClick={(params: GridRowParams) => {
            let findReserve = reserves.find(
              (reserve) => reserve.id === params.id
            );
            if (findReserve) {
              handleClickRow(Number(params.id), findReserve);
            }
          }}
          autoPageSize
          disableRowSelectionOnClick
        />
      </div>
    </>
  );
};

export default TableHP;
