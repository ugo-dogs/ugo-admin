import { FunctionComponent, useState } from 'react';
import classnames from 'classnames';

import Image from 'next/image';

import { FinalResult, Walker } from '../lib/types';

import { turnFormatter } from '../lib/utils/turnFormatter';

type Props = {
  walks: Array<FinalResult>;
  handleClick: (walker: string) => void;
  isClosed: boolean;
  isLoading: boolean;
};

const Table: FunctionComponent<Props> = (props) => {
  const { walks, isClosed, isLoading, handleClick } = props;

  const [rowActive, setRowActive] = useState<number>();

  const rowClassnames = (idRow: number) =>
    classnames(
      'grid grid-cols-5 gap-5 border-b-[1px] border-white-primary border-opacity-30 pt-7 pb-7 hover:cursor-pointer hover:bg-pink-primary hover:font-bold',
      {
        'text-pink-primary hover:text-white-primary':
          idRow === rowActive && isClosed,
      }
    );

  const handleClickRow = (indexRow: number, walker: Walker) => {
    setRowActive(indexRow);
    handleClick(walker._id);
  };

  return (
    <>
      <div className='grid grid-cols-5 gap-5 border-b-[1px] border-white-primary pb-5 font-bold'>
        <div>
          <span>Fecha</span>
        </div>
        <div>
          <span>Paseador</span>
        </div>
        <div>
          <span>Turno</span>
        </div>
        <div className='col-span-2'>
          <span>Perros</span>
        </div>
      </div>
      {walks.map((walk, idx) => (
        <div
          key={walk.id}
          className={rowClassnames(idx)}
          onClick={() => handleClickRow(idx, walk.paseador)}
        >
          <div>{walk.date}</div>
          <div>
            <p>{walk.name}</p>
            <p className='text-xs'>{walk.phone}</p>
          </div>
          <div>{turnFormatter(walk.end)}</div>
          <div className='col-span-2'>
            <div className='inline overflow-hidden p-2'>
              {walk.dogs.map((mappedDog, idx) => {
                const dogNames = walk.dogs
                  .map((dog, idx) => {
                    if (idx < 5) {
                      return dog.dog.name;
                    }
                  })
                  .filter(Boolean);

                if (idx >= 5) {
                  return null;
                } else {
                  return (
                    <>
                      <div
                        key={idx}
                        className='inline-block rounded-full border-2 border-white-primary w-[30px] h-[30px] mr-0 shadow z-10'
                      >
                        <Image
                          src={mappedDog.dog.avatar.formats.thumbnail.url}
                          alt={mappedDog.dog.name}
                          width={30}
                          height={30}
                          className='rounded-full -ml-2'
                        />
                      </div>
                      {idx === 0 && (
                        <span className='mt-9 -ml-7 absolute text-[12px]'>
                          {dogNames.join(', ')}
                        </span>
                      )}
                    </>
                  );
                }
              })}
              {walk.dogs.length - idx >= 5 && (
                <span className='ml-2 mt-1 text-[12px] absolute'>{`+ ${
                  walk.dogs.length - 5
                } más`}</span>
              )}
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export default Table;
