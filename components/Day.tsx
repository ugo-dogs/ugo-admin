import { FunctionComponent } from 'react'

type Props = {
  day: string,
  selected:Array<string>,
  onPress: (day:string)=> void
}

const days: Record<string, string> = {
  '1': 'Lun',
  '2': 'Mar',
  '3': 'Mier',
  '4': 'Jue',
  '5': 'Vie',
  '6': 'Sab',
  '7': 'Dom',
}

const Day: FunctionComponent<Props> = ({ day,selected ,onPress }) => {
  return (
    <div className={`bg-gray-secondary text-center p-5 rounded-lg mr-3 ${selected?.includes(day) ? 'border-[2px] border-pink-primary' : 'bg-gray-secondary'} cursor-pointer `} onClick={() =>onPress(day)}>
      <span className="text-sm font-bold text-white-primary">{days[day]}</span>
    </div>
  )
}

export default Day
