import { FunctionComponent } from 'react'
import classnames from 'classnames'

import Image from 'next/image'

import EmptyStar from '../public/empty-star.svg'
import Star from '../public/star.svg'

type Props = {
  valoration?: {
    stars: number
    reviews_total: number
  }
  fromReview?: boolean
}

const maxStars = 5

const Valoration: FunctionComponent<Props> = ({ valoration, fromReview = false }) => {
  const starsClassname = classnames({ 'mr-4': fromReview })
  if (!valoration) {
    return (
      <>
        <div className={starsClassname}>
          {Array.from(Array(maxStars)).map((elem, idx) => (
            <Image key={idx} src={EmptyStar} alt="empty star" />
          ))}
        </div>
        <span className="underline">0 Reseñas</span>
      </>
    )
  }

  const { stars, reviews_total } = valoration

  const reviews = reviews_total === 1 ? '1 Reseña' : `${reviews_total} Reseñas`
  return (
    <>
      <div className={starsClassname}>
        {Array.from(Array(stars)).map((_, idx) => (
          <Image key={`${idx}-star`} src={Star} alt="star" />
        ))}
        {Array.from(Array(maxStars - stars)).map((_, idx) => (
          <Image key={`${idx}-empty-star`} src={EmptyStar} alt="empty star" />
        ))}
      </div>
      <span className="underline">{reviews}</span>
    </>
  )
}

export default Valoration
