import { FunctionComponent } from 'react'

import Image from 'next/image'

import Dawn from '../public/dawn.svg'
import Sunset from '../public/sunset.svg'

type Props = {
  turno: {
    titulo:string;
    
    id:string;
    start:string;
    end:string
  },
  selected:Array<{titulo:string}>,
  onPress:(turno:{
    titulo:string;
    
    id:string;
    start:string;
    end:string
  }) => void
}

const turnoSVG: Record<string, JSX.Element> = {
  'Turno mañana': (
    <Image src={Dawn} alt="Turno mañana" width={26} height={15} />
  ),
  'Turno tarde': (
    <Image src={Sunset} alt="Turno tarde" width={25} height={25} />
  ),
  'Turno noche': (
    <Image src={Sunset} alt="Turno noche" width={26} height={25} />
  ),
}

const Turno: FunctionComponent<Props> = ({ turno,selected ,onPress }) => {
  console.log('turno',turno);
  
  const TurnoSVG = turnoSVG[turno.titulo]
  
  return (


 
    <div className={`flex flex-col items-center bg-gray-secondary text-center p-5 rounded-lg w-[100px] mr-7 ${selected?.some((turnoItem)=> turnoItem.titulo == turno.titulo ) ? 'border-[2px] border-pink-primary' : 'bg-gray-secondary'} cursor-pointer`} onClick={()=>onPress(turno)}>
      <div className="mb-1">{TurnoSVG}</div>
      <span className="text-base font-bold text-gray-tertiary">{turno.titulo}</span>
    </div>

  )
}

export default Turno






