import { FunctionComponent, useState, ChangeEvent } from 'react';

import Image from 'next/image';

import ArrowDown from '../public/arrow-down.svg';

type Option = {
  name: string;
  value: string;
};

type SelectFilterProps = {
  label: string;
  defaultValue: string;
  options: Array<Option>;
  action: (value: string) => void;
};

const SelectFilter: FunctionComponent<SelectFilterProps> = ({
  label,
  defaultValue,
  options,
  action,
}) => {
  const [value, setValue] = useState(defaultValue);

  const handleOnChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const { value } = event.target;

    setValue(value);
    action(value);
  };

  return (
    <div className='ml-3 hidden sm:block'>
      <label className='relative'>
        {/* <span className='font-bold'>{label}</span> */}
        <div className='absolute top-0 left-5'>
          <Image src={ArrowDown} alt='arrow down' width={18} height={18} />
        </div>
        <select
          className='bg-black-primary w-full py-5 px-14 placeholder-white-primary rounded-lg focus:outline-none appearance-none f5'
          onChange={handleOnChange}
          value={value}
        >
          <option disabled selected>
            {defaultValue}
          </option>
          {options.map(({ name, value }) => (
            <option key={name} value={value}>
              {name}
            </option>
          ))}
        </select>
      </label>
    </div>
  );
};

export default SelectFilter;
