import { FunctionComponent } from 'react'

type Props = {
  comment: string
}

const Review: FunctionComponent<Props> = ({ comment }) => {
  return (
    <div className="bg-gray-secondary py-3 px-5 rounded-lg mb-4">
      <span className="text-white-primary text-xs">{comment}</span>
    </div>
  )
}

export default Review
