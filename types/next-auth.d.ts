import type { Session, DefaultSession, User } from "next-auth"
import type { JWT } from "next-auth/jwt"

declare module "next-auth" {
  interface User {
    user: {
      name: string
      email: string
      thumb: {
        url: string
      }
    }
    jwt: string
  }

  interface Session {
    jwt: string
  }
}

declare module "next-auth/jwt" {
  interface JWT {
    jwt: string
  }
}
