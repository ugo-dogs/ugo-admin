const rangeHours = {
  morning: {
    start: '01/01/2022 07:00:00.000',
    end: '01/01/2022 12:00:00.000'
  },
  afternoon: {
    start: '01/01/2022 12:00:01.000',
    end: '01/01/2022 18:00:00.000',
  },
  night: {
    start: '01/01/2022 18:00:01.000',
    end: '01/01/2022 22:00:00.000'
  }
};

export const turnFormatter = (endDate: string): string => {
  const endDateHelper = new Date(`01/01/2022 ${endDate}`);
  const { morning, afternoon } = rangeHours;

  if (endDateHelper > new Date(morning.start) && endDateHelper < new Date(morning.end)) {
    return 'Mañana'
  }

  if (endDateHelper > new Date(afternoon.start) && endDateHelper < new Date(afternoon.end)) {
    return 'Tarde'
  }
  return 'Noche'
}
