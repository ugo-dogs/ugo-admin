export default [
    {
      title: 'Datos Dueño',
      icon: 'personal',
      fields: [
        { type: 'text', value: 'first_name', placeholder: 'Nombre',section:'' },
        { type: 'text', value: 'last_name', placeholder: 'Apellido' ,section:'' },
        { type: 'email', value: 'email', placeholder: 'Mail' ,section:'' },
        { type: 'number', value: 'dni', placeholder: 'DNI' ,section:'' },

        { type: 'number', value: 'phone', placeholder: 'Telefono',section:'' },
        // { type: 'text', value: 'direccion', placeholder: 'direccion' ,section:'' },
      ],
    },
    {
      title: 'Datos del Perro',
      icon: 'dog',
      fields: [
        { type: 'text', value: 'first_name', placeholder: 'Nombre',section:'' },
        { type: 'number', value: 'age', placeholder: 'Edad' ,section:'' },
        { type: 'text', value: 'age', placeholder: 'Raza' ,section:'' },
        { type: 'text', value: 'age', placeholder: 'Genero' ,section:'' },

   
      ],
    },
    {
      title: 'Datos de estadia',
      icon: 'bank',
      fields: [
        { type: 'select', value: 'barrio', placeholder: 'barrio' ,section:'paseador_zone' },
        { type: 'text', value: 'dias de paseo', placeholder: 'dias de paseo',section:'days_available'  },
        {
          type: 'select',
          value: 'turnos',
          placeholder: 'Turnos de paseo',
          section:'turnos'
        },
      ],
    },
  ];
  