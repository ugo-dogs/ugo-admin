export default [
  {
    title: 'Datos Personales',
    icon: 'personal',
    fields: [
      { type: 'text', value: 'first_name', placeholder: 'Nombre',section:'' },
      { type: 'text', value: 'last_name', placeholder: 'Apellido' ,section:'' },
      { type: 'email', value: 'email', placeholder: 'Mail' ,section:'' },

      { type: 'date', value: 'bornDate', placeholder: 'Fecha de nacimiento' ,section:'' },
      { type: 'number', value: 'phone', placeholder: 'Telefono',section:'' },
      { type: 'text', value: 'direccion', placeholder: 'direccion' ,section:'' },
    ],
  },
  {
    title: 'Datos Bancarios',
    icon: 'bank',
    fields: [
      { type: 'text', value: 'bank_name', placeholder: 'banco',section:'bank_account' },
      { type: 'text', value: 'name', placeholder: 'Nombre Titular' ,section:'bank_account' },
      { type: 'number', value: 'dni', placeholder: 'DNI',section:'bank_account'  },
      { type: 'text', value: 'cbu', placeholder: 'cbu' ,section:'bank_account' },
      { type: 'text', value: 'alias', placeholder: 'ALIAS' ,section:'bank_account' },
    ],
  },
  {
    title: 'Datos de Paseo',
    icon: 'dog',
    fields: [
      { type: 'select', value: 'barrio', placeholder: 'barrio' ,section:'paseador_zone' },
      { type: 'text', value: 'dias de paseo', placeholder: 'dias de paseo',section:'days_available'  },
      {
        type: 'select',
        value: 'turnos',
        placeholder: 'Turnos de paseo',
        section:'turnos'
      },
    ],
  },
];
