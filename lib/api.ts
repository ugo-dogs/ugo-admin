import { getToken } from 'next-auth/jwt'
import axios from 'axios'

import type { Walk } from './types'

const { NEXT_PUBLIC_API_URL, SECRET } = process.env

export async function getAllDogs(req: any) {
  const token = getToken({ req, secret: process.env.SECRET! }) as any

  const { data } = await axios.get<Array<Walk>>(`${process.env.NEXT_PUBLIC_API_URL}/perros`, {
    headers: { Authorization: `Bearer ${token.jwt}` },
  })

  return data
}

export async function getAllUsers(req: any) {
  const token = getToken({ req, secret: process.env.SECRET! }) as any

  const { data } = await axios.get<Array<any>>(`${process.env.NEXT_PUBLIC_API_URL}/users`, {
    headers: { Authorization: `Bearer ${token.jwt}` },
  })

  return data
}

export async function getAllTransactions(req: any) {
  const token = getToken({ req, secret: process.env.SECRET! }) as any

  const { data } = await axios.get<Array<Walk>>(
    `${process.env.NEXT_PUBLIC_API_URL}/transactions`,
    {
      headers: { Authorization: `Bearer ${token.jwt}` },
    },
  )

  return data
}
