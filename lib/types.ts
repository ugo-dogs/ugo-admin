export type Walk = {
  _id: string;
  date: string;
  end: string;
  start: string;
  dog: Dog;
  paseador: Walker;
  direccion: Direccion;
};

export type Direccion = {
  barrio: {
    comuna: number;
    barrio: string;
  };
  label: string;
  desc?: string;
};

export type Dog = {
  dog: Dog;
  name: string;
  age: number;
  start: string;
  end: string;
  size: string;
  description: string;
  direccion: Direccion;
  paseador: object;
  user: User;
  avatar: {
    formats: {
      thumbnail: {
        url: string;
      };
    };
  };
  raza: {
    name: string;
  };
};

export type Walker = {
  _id: string;
  first_name: string;
  phone: number;
  last_name: string;
  direccion: string;
  bornDate: Date;
  description: string;
  email: string;
  days_available: {
    days: Array<string>;
  };
  paseador_zone: Barrios;
  bank_account: {
    bank_name: string;
    cbu: number;
    alias: string;
    dni: number;
  };
  turnos: Array<{
    id: string;
    titulo: string;
    start: string;
    end: string;
  }>;
  valoration?: {
    stars: number;
    reviews_total: number;
  };
  thumb?: {
    formats: {
      thumbnail: {
        url: string;
      };
    };
  };
};
export type User = {
  _id: string;
  first_name: string;
  phone: number;
  last_name: string;
  direccion: string;
  bornDate: Date;
  description: string;
  email: string;
  thumb: {
    formats: {
      thumbnail: {
        url: string;
      };
    };
  };
};
export type Transaction = {
  payment_type: string;
  status: string;
  bundleID: string;
  total_amount: number;
  paseador: Walker;
  user: User;
  payment: String;
  id: string;
  date: string;
};
export type FinalResult = {
  id: string;
  date: string;
  dogs: Array<Dog>;
  end: string;
  start: string;
  name: string;
  paseador: Walker;
  phone: number;
  zone: string;
};
export type ReservesHP = {
  id: string;
  date: string;
  dogs: Array<Dog>;
  owner_name: string;
  owner_surname: string;
  owner_email: string;
  owner_phone: string;
  owner_dni: string;
  owner_address: string;
  dog_genre: string;
  dog_raza: string;
  dog_social: string;
  dog_name: string;
  dog_castrado: string;
  date_celo: string;
  dog_behaviour: string;
  dog_age: number;
  dog_vaccine: boolean;
  dog_deworming: boolean;
  aob_date_start: string;
  aob_date_end: string;
  aob_price: number;
  status: string;
  aob_purchased: string;
  dog_comments : string;
  dog_food: string;
  dog_bite: string;
  dog_swim: string;
  discount_cupon : string;
  discount_amount : number;
  provincia : string;
  payment_id: string, 
  precio_noche : number,
  tarifa_traslado : number,
};
export type Payment = {
  id: string;
  date: string;
  expiration: string;
  bundleID: string;
  payment_id: number;
  total_amount: number;
  paseador: Walker;
  status: string;
  payment_type: string;
  transaction: Transaction;
};

export type Barrios = {
  _id: string;
  comuna: number;
  barrio: string;
};
export type Faqs = {
  _id: string;
  title: string;
};
export type Ticket = {
  _id: string;
  date: Date;
  message: string;
  category: string;
  user: User;
  status: string;
};

export type InputOwner = {
  title:string;
  icon:string;
  _id:string;
  inputsOwner: {
    map(arg0: (field: any) => JSX.Element): import("react").ReactNode;
    placeholder: string;
    type: string;
    id:string
  }

}
export type InputDog = {
  title:string;
  icon:string;
  _id:string;
  comportamiento:[
    {
      id:string;
    }
  ]
  healthDogs:[
    {
      id:string;
      question:string
    }
  ]
  inputDogs: {
    placeholder: string;
    type: string;
    id:string
    selectOptions?:[{
      nombre:string;
      value:string
      title:string;
      id:string;
    }]
  }
  social:[
    {
      desc:string;
      id:string;

    }
  ]
social_comportamiento:[
{
questions:string;
id:string;
options:[
  {
    title:string;
    value:string
    id:string
  }
]
  }
]
}
export type Inputs =  {
  map(arg0: (menu_item: any) => JSX.Element): import("react").ReactNode;
  owner: InputOwner;
  dog: InputDog;
}
export type EditReserve = {
  ownerData?: {
    nombre: string;
    apellido: string;
    mail: string;
    telefono: string;
    dni: string;
    dirección: string;
    provincia: string;
  },
  dogData?: {
    nombre: string;
    edad: string;
    raza: string;
    Género: string;
    castrado: string;
    dog_swim: string;
    dog_genre: string;
    dog_raza: string;
    dog_social: string;
    dog_name: string;
    dog_castrado: string;
    date_celo: string;
    dog_behaviour: string;
    dog_age: number;
    dog_vaccine: boolean;
    dog_deworming: boolean;
    dog_comments : string;
    dog_bite: string;
    dog_food: string;
    ['¿Cuál fue la fecha del último celo?']:string;
    ['¿Tiene alguno de estos comportamientos?']:string;
    ['Mordío otros perros?']:string;
    ["Sabe nadar?" ]:string;
    ["Tiene alérgias?"]:boolean;
    ["Se realizó alguna cirugía?"]:boolean;
    ["Confirmo que a mi mascota se le realizó una desparasitación externa e interna"]:boolean;
    ["Confirmo que mi mascota está vacunado con la Séxtuple, Antirrábica y Tos de las perreras"]:boolean;
  },
  dataTrip?: {
    aob_date_start: string;
    aob_date_end: string;
    aob_price: number;
    aob_purchased:string;
  },
  id: string;
}