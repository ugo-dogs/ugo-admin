module.exports = {
  mode: 'jit',
  darkMode: 'media',
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      inset: {
        15: '3.8rem',
      },
      fontFamily: {
        laussane: ['Laussane', 'sans-serif'],
        faro: ['Faro', 'serif'],
      },
    },
    colors: {
      black: {
        primary: '#000000',
        secondary: '#222',
        light: '#444',
      },
      white: {
        primary: '#FFFFFF',
        secondary: '#ccc',
      },
      pink: {
        primary: '#F4B5CD',
        secondary: '#FF619D',
      },
      aob: {
        green: '#7BDE96',
      },
      red: {
        primary: '#ED4C5C',
      },
      gray: {
        primary: '#231F20',
        secondary: '#343031',
        tertiary: '#4f4b4c',
        hover: '#4b4446',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
