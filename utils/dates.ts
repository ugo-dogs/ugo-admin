const formatDate = (date: Date) => {
  return date.toISOString().split('T')[0]
}

const todayDate = new Date()

const today = formatDate(todayDate)

const tomorrowDate = new Date()
tomorrowDate.setDate(todayDate.getDate() + 1)

const tomorrow = formatDate(tomorrowDate)

const dayAfterTomorrowDate = new Date()
dayAfterTomorrowDate.setDate(todayDate.getDate() + 2)

const dayAfterTomorrow = formatDate(dayAfterTomorrowDate)

export { today, tomorrow, dayAfterTomorrow }
