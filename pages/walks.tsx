import type { GetServerSideProps } from 'next';
import { getToken } from 'next-auth/jwt';
import { useSession } from 'next-auth/react';

import { useState } from 'react';
import classnames from 'classnames';
import groupBy from 'lodash/groupBy';
import { QueryFunctionContext, useQuery, useQueryClient } from 'react-query';

import type { NextComponentWithAuth } from './_app';

import type { Barrios, FinalResult } from '../lib/types';

import TextFilter from '../components/TextFilter';
import SelectFilter from '../components/SelectFilter';
import WalkDetail from '../components/WalkDetail';
import Table from '../components/Table';

import Image from 'next/image';

import Close from '../public/close.png';

import { today, tomorrow, dayAfterTomorrow } from '../utils/dates';

const { NEXT_PUBLIC_API_URL, SECRET } = process.env;

const getWalks = async (jwt: string, key?: QueryFunctionContext<any, any>) => {
  const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/paseos`);

  const dogName = key?.queryKey[1].dogName;
  if (dogName) {
    apiUrl.searchParams.append('dog.name_contains', dogName);
  }

  const walkerName = key?.queryKey[2].walkerName;
  if (walkerName) {
    apiUrl.searchParams.append('paseador.first_name_contains', walkerName);
  }

  const date = key?.queryKey[3].date;
  if (date) {
    apiUrl.searchParams.append('date_gte', date);
  } else {
    apiUrl.searchParams.append('date_lte', today);
  }
  // apiUrl.searchParams.append('date_lte', today);

  const walkState = key?.queryKey[4].walkState;
  if (walkState) {
    apiUrl.searchParams.append('status', walkState);
  }

  const zone = key?.queryKey[5].zone;
  if (zone) {
    apiUrl.searchParams.append('direccion.barrio.barrio_contains', zone);
  }

  apiUrl.searchParams.append('_sort', 'date:DESC');

  const res = await fetch(apiUrl.href, {
    headers: {
      Authorization: `Bearer ${jwt}`,
    },
  });

  const data = await res.json();

  const groupedByPaseadores = groupBy(data, (walk) => walk.paseador._id);

  const finalResult: Array<FinalResult> = Object.entries(
    groupedByPaseadores
  ).map(([paseadorId, walks]) => {
    console.log('ss',walks[0]);
    
    return {
      id: paseadorId,
      date: walks[0].date,
      end: walks[0].end,
      start: walks[0].start,
      name: walks[0].paseador.first_name,
      phone: walks[0].paseador.phone,
      paseador:{
        _id: walks[0].paseador._id,
        first_name: walks[0].paseador.first_name,
        phone: walks[0].paseador.phone,
        last_name: walks[0].paseador.last_name,
        direccion: walks[0].paseador.direccion,
        bornDate: walks[0].paseador.bornDate,
        description: walks[0].paseador?.description,
      email:walks[0].paseador.email,
  turnos:walks[0].paseador.turnos,
  paseador_zone:{ _id:'321312',barrio:'Recoleta',comuna:11 },
  bank_account:{ bank_name:'null', cbu: 0,
  alias: 's',
  dni:1 },
  days_available:{ days:[] }
        },
      zone: walks[0].direccion.barrio.barrio,
      dogs: walks,
    };
  });

  return finalResult;
};
const getBarrios = async ()=>{
  try {
    const result = await fetch(NEXT_PUBLIC_API_URL + '/barrios');
    const parsed = await result.json();
    return parsed.zonas
  } catch (err) {
    console.log('err',err);
  }
}
export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  try {
    const token = (await getToken({ req, secret: SECRET! })) as any;

    const finalResult = await getWalks(token.jwt);
    const barrios = await getBarrios();

    return {
      props: {
        walks: finalResult,
        barrios: barrios,
      },
    };
  } catch (e) {
    console.log(e);
    return {
      props: {
        walks: [],
      },
    };
  }
};

type Props = {
  walks: Array<FinalResult>;
  barrios:Array<Barrios>
};

export type FilterType = {
  key: string;
  value: string;
};

const Walks: NextComponentWithAuth<Props> = ({ walks ,barrios }) => {
  const [dogName, setDogName] = useState('');
  const [walker, setWalker] = useState('');
  const [date, setDate] = useState('');
  const [walkState, setWalkState] = useState('');
  const [zone, setZone] = useState('');

  const [showWalk, setShowWalk] = useState(false);
  const [paseadorId, setPaseadorId] = useState('');

  const [isActiveWalksEnabled, setActiveWalksEnabled] = useState(false);
  const [isActiveTodayWalks, setActiveTodayWalks] = useState(false);

  const session = useSession();

  const queryClient = useQueryClient();

  const { data, isSuccess, isLoading } = useQuery(
    [
      'walks',
      { dogName },
      { walkerName: walker },
      { date },
      { walkState },
      { zone },
    ],
    (key) => getWalks(session.data!.jwt, key),
    { initialData: walks, retry: false }
  );

  const handleDogName = (value: string) => {
    setPaseadorId('');
    setShowWalk(false);
    setDogName(value);
  };

  const handleWalkerName = (value: string) => {
    setPaseadorId('');
    setShowWalk(false);
    setWalker(value);
  };

  const handleDate = (value: string) => {
    setPaseadorId('');
    setShowWalk(false);
    setDate(value);
  };

  const handleWalkState = (value: string) => {
    setPaseadorId('');
    setShowWalk(false);
    setWalkState(value);
  };

  const handleZone = (value: string) => {
    setPaseadorId('');
    setShowWalk(false);
    setZone(value);
  };

  const showWalkDetail = (paseadorId: string) => {
    setShowWalk(true);
    setPaseadorId(paseadorId);
  };

  const hideWalkDetail = () => {
    setShowWalk(false);
  };

  const walkDetail = data!.filter((walk) => walk.id === paseadorId)[0];

  const buttonClassnames = (isActive: boolean) =>
    classnames(
      'pt-2 pb-2 pr-7 pl-7 border border-white-primary text-center rounded-full hover:border-pink-primary hover:text-pink-primary',
      {
        'border-pink-primary bg-pink-primary text-gray-primary hover:text-gray-primary':
          isActive,
      }
    );

  return (
    <div className='flex'>
      <div className={`h-screen w-80 bg-gray-secondary pl-8 pr-4 pt-9 pb-5 overflow-auto  ${showWalk && 'opacity-10'}`}>
        <h1 className='font-bold text-3xl'>Paseos</h1>

        <TextFilter
          label='Usuarios / Perros'
          placeholder='Buscar por nombre'
          action={handleDogName}
        />

        <TextFilter
          label='Paseadores'
          placeholder='Buscar por nombre'
          action={handleWalkerName}
        />

        <SelectFilter
          label='Fecha'
          defaultValue='Buscar por fecha'
          options={[
            { name: today, value: today },
            { name: tomorrow, value: tomorrow },
            { name: dayAfterTomorrow, value: dayAfterTomorrow },
          ]}
          action={handleDate}
        />

        <SelectFilter
          label='Estado del paseo'
          defaultValue='Buscar por estado'
          options={[
            { name: 'No empezado', value: '-1' },
            { name: 'En paseo', value: '0' },
            { name: 'Terminado', value: '1' },
          ]}
          action={handleWalkState}
        />

        <SelectFilter
          label='Zona de paseo'
          defaultValue='Buscar por zona'
          options={[
            { name: 'Palermo', value: 'Palermo' },
            { name: 'Recoleta', value: 'Palermo' },
          ]}
          action={handleZone}
        />

        {(dogName || walker || date || walkState || zone) && (
          <div className='border-t-2 border-black-primary mt-10'>
            <div className='mt-6 font-bold'>
              <span className='text-[13px]'>Filtros Aplicados</span>
              <ul className='space-x-4 flex flex-wrap'>
                {dogName && (
                  <li className='mt-4 p-2 bg-black-primary text-pink-primary max-w-max rounded-md flex'>
                    <span className='text-[11px]'>{dogName}</span>
                    <button
                      className='bg-white-primary hover:bg-gray-tertiary text-black-primary rounded-full h-4 w-4 flex items-center justify-center ml-5'
                      onClick={() => handleDogName('')}
                    >
                      <Image src={Close} alt='close' width={5} height={5} />
                    </button>
                  </li>
                )}
                {walker && (
                  <li className='mt-4 p-2 bg-black-primary text-pink-primary max-w-max rounded-md flex'>
                    <span className='text-[11px]'>{walker}</span>
                    <button
                      className='bg-white-primary hover:bg-gray-tertiary text-black-primary rounded-full h-4 w-4 flex items-center justify-center ml-5'
                      onClick={() => handleWalkerName('')}
                    >
                      <Image src={Close} alt='close' width={5} height={5} />
                    </button>
                  </li>
                )}
                {date && (
                  <li className='mt-4 p-2 bg-black-primary text-pink-primary max-w-max rounded-md flex'>
                    <span className='text-[11px]'>{date}</span>
                    <button
                      className='bg-white-primary hover:bg-gray-tertiary text-black-primary rounded-full h-4 w-4 flex items-center justify-center ml-5'
                      onClick={() => handleDate('')}
                    >
                      <Image src={Close} alt='close' width={5} height={5} />
                    </button>
                  </li>
                )}
                {walkState && (
                  <li className='mt-4 p-2 bg-black-primary text-pink-primary max-w-max rounded-md flex'>
                    <span className='text-[11px]'>{walkState}</span>
                    <button
                      className='bg-white-primary hover:bg-gray-tertiary text-black-primary rounded-full h-4 w-4 flex items-center justify-center ml-5'
                      onClick={() => handleWalkState('')}
                    >
                      <Image src={Close} alt='close' width={5} height={5} />
                    </button>
                  </li>
                )}
                {zone && (
                  <li className='mt-4 p-2 bg-black-primary text-pink-primary max-w-max rounded-md flex'>
                    <span className='text-[11px]'>{zone}</span>
                    <button
                      className='bg-white-primary hover:bg-gray-tertiary text-black-primary rounded-full h-4 w-4 flex items-center justify-center ml-5'
                      onClick={() => handleZone('')}
                    >
                      <Image src={Close} alt='close' width={5} height={5} />
                    </button>
                  </li>
                )}
              </ul>
            </div>
          </div>
        )}
      </div>
      <div className={`p-11 lg:w-[50%] md:w-[75%] sm:w-[90%] ${showWalk && 'opacity-10'}`}> 
        <div className='mb-10'>
          <button
            className={`mr-5 ${buttonClassnames(isActiveWalksEnabled)}`}
            onClick={() => setActiveWalksEnabled(!isActiveWalksEnabled)}
          >
            Paseos Activos
          </button>
          <button
            className={buttonClassnames(isActiveTodayWalks)}
            onClick={() => setActiveTodayWalks(!isActiveTodayWalks)}
          >
            Paseos del día
          </button>
        </div>
        {isSuccess && (
          <Table
            walks={data!}
            handleClick={(walker: string) => showWalkDetail(walker)}
            isClosed={showWalk}
            isLoading={isLoading}
           
          />
        )}
      </div>

      {showWalk && (
        <WalkDetail
          detail={walkDetail}
          handleCloseDetail={() => hideWalkDetail()}
          barrios={barrios}
        />
      )}
    </div>
  );
};

Walks.authenticationEnabled = true;

export default Walks;
