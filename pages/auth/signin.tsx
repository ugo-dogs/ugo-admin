import type { GetServerSideProps } from 'next';

import Link from 'next/link';
import Image from 'next/image';

import { getCsrfToken, getSession } from 'next-auth/react';

import logo from '../../public/logo.svg';

export default function SignIn({ csrfToken }: { csrfToken: string }) {
  const year = new Date().getFullYear();
  return (
    <>
      <header className='bg-pink-primary pl-14 pt-4 pb-4'>
        <Link legacyBehavior href='/'>
          <a>
            <Image src={logo} alt='uGo admin logo' width={110} height={36} />
          </a>
        </Link>
      </header>
      <main>
        <div className='h-auto'>
          <div className='pb-6'>
            <div className='bg-pink-primary block h-[182px] w-full' />
            <div className='bg-gray-secondary rounded-md text-center pt-8 pb-8 pl-16 pr-16 max-w-[424px] -mt-28 ml-auto mr-auto mb-0'>
              <div className='mt-6'>
                <h1 className='font-bold text-xl'>Bienvenido a uGo Admin!</h1>
              </div>
              <form
                method='post'
                action='/api/auth/callback/credentials'
                className='flex flex-col items-center  mt-12'
              >
                <input
                  name='csrfToken'
                  type='hidden'
                  defaultValue={csrfToken}
                />
                <label className='w-full'>
                  <div className='flex items-center rounded shadow min-h-[48px]'>
                    <input
                      name='email'
                      type='text'
                      placeholder='ejemplo@gmail.com'
                      className='h-12 bg-gray-primary w-full border-none overflow-hidden pt-4 pb-4 pl-3 pr-3 text-center'
                    />
                  </div>
                </label>
                <label className='w-full mt-4'>
                  <div className='flex items-center rounded shadow min-h-[48px]'>
                    <input
                      name='password'
                      type='password'
                      placeholder='Contraseña'
                      className='h-12 bg-gray-primary border-none overflow-hidden pt-4 pb-4 pl-3 pr-3  w-full text-center'
                    />
                  </div>
                </label>

                <button
                  className='mt-4 min-h-[62px] w-full rounded-xl bg-gradient-to-r from-pink-primary to-pink-secondary font-semibold'
                  type='submit'
                >
                  Iniciar sesión
                </button>
              </form>
            </div>
          </div>
        </div>
      </main>
      <footer className='flex flex-col items-center absolute bottom-0 w-full bg-gray-secondary pt-7 pb-7'>
        <span className='underline'>{`${year}® UGO Argentina S.A — Todos los derechos reservados`}</span>
      </footer>
    </>
  );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  console.log('conext',context);
  
  const session = await getSession(context);

  if (session) {
    return {
      redirect: {
        permanent: false,
        destination: '/houseParadise',
      },
    };
  }

  return {
    props: {
      csrfToken: await getCsrfToken(context),
    },
  };
};
