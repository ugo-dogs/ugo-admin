import { GetServerSideProps } from 'next';
import { getToken } from 'next-auth/jwt';
import { QueryFunctionContext, useQuery } from 'react-query';
import moment from 'moment';
import type { NextComponentWithAuth } from './_app';
import type { Faqs, Ticket, User } from '../lib/types';
import SelectFilter from '../components/SelectFilter';
import { useRef, useState, useEffect } from 'react';
import { today, tomorrow, dayAfterTomorrow } from '../utils/dates';
import MagnifyingGlass from '../public/magnifying-glass.svg';
import Help from '../public/help.svg';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import Image from 'next/image';
import { useSession } from 'next-auth/react';
import { AiOutlineCalendar } from 'react-icons/ai';

const { NEXT_PUBLIC_API_URL, SECRET } = process.env;

const getFAQS = async (
  // key?: QueryFunctionContext<any, any>,
  jwt: string
): Promise<Array<Faqs>> => {
  // const token = (await getToken({ req, secret: SECRET! })) as any;

  const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/faq`);

  // apiUrl.searchParams.append('paseador', 'true');
  // apiUrl.searchParams.append('verified', null);

  const res = await fetch(apiUrl.href, {
    headers: {
      Authorization: `Bearer ${jwt}`,
    },
  });
  const data = await res.json();

  return data.categories;
};

const getTickets = async (
  jwt: string,
  key?: QueryFunctionContext<any, any>
): Promise<Array<Ticket>> => {
  const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/tickets`);
  let filters: Array<(user: User) => boolean> = [];

  const userName = key?.queryKey[1].name;
  if (userName) {
    apiUrl.searchParams.append('user.first_name_contains', userName);
  }

  const date = key?.queryKey[2].date;
  if (date) {
    apiUrl.searchParams.append('date_eq', date);
  }
  const statusActive = key?.queryKey[3].statusActive;
  if (statusActive && statusActive !== 'default') {
    apiUrl.searchParams.append('status', statusActive);
  }
  const categoryActive = key?.queryKey[4].categoryActive;
  if (categoryActive) {
    apiUrl.searchParams.append('category', categoryActive);
  }

  //  } else {
  //    apiUrl.searchParams.append('date_gte', today);
  //  }
  // apiUrl.searchParams.append('paseador', 'true');
  // apiUrl.searchParams.append('verified', null);

  const res = await fetch(apiUrl.href, {
    headers: {
      Authorization: `Bearer ${jwt}`,
    },
  });
  const data = await res.json();
  console.log('data', data);

  return data;
};

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  const token = (await getToken({ req, secret: process.env.SECRET! })) as any;

  try {
    const faqs = await getFAQS(token.jwt);
    const tickets = await getTickets(token.jwt);

    // const feed = await getWalkersFeed()
    // const barrios = await getBarrios();

    return {
      props: {
        faqs: faqs,
        tickets: tickets,
        // barrios: barrios,
      },
    };
  } catch (e) {
    console.log(e);
    return {
      props: {
        faqs: [],
        tickets: [],
      },
    };
  }
};
type Props = {
  faqs: Array<Faqs>;
  tickets: Array<Ticket>;
};

const ClaimsPage: NextComponentWithAuth<Props> = ({ faqs, tickets }) => {
  const [statusActive, setstatusActive] = useState<string>('default');
  const dateRef = useRef();
  const [date, setDate] = useState('');
  const [showDate, setShowDate] = useState(false);
  const [name, Setname] = useState('');
  const session = useSession();
  const [categoryActive, setcategoryActive] = useState('');
  const { data, isSuccess, isLoading } = useQuery(
    ['tickets', { name }, { date }, { statusActive }, { categoryActive }],
    (key) => getTickets(session.data!.jwt, key),
    {
      initialData: tickets,
      retry: false,
    }
  );
  const [startDate, setStartDate] = useState(new Date());

  const handleStatus = (value: string) => {
    setstatusActive(value);
  };
  const handleDate = (value: string) => {
    setDate(value);
  };

  const handleCategory = (category: string) => {
    if (category == categoryActive) {
      setcategoryActive('');
    } else {
      setcategoryActive(category);
    }
  };
  // useEffect(() => {
  //  if(showDate && dateRef.current){
  //    dateRef.current.focus()
  //  }
  // }, [showDate])

  return (
    <div className='p-16 h-screen overflow-y-scroll'>
      <div className='w-[100%] flex flex-row items-center justify-between'>
        <div className='flex flex-row'>
          {/* {!showDate ? (
      <div onClick={()=> {setShowDate(true)}} className=' h-14  px-10 border border-white-primary text-center rounded-full bg-gray-secondary outline-none items-center flex' ><p>Fecha</p></div>
    ) : (
      <input  ref={dateRef} type="date" value={date} onChange={(value)=>setDate(value.target.value)} placeholder='Fecha' autoFocus className=' h-14  px-10 border border-white-primary text-center rounded-full bg-gray-secondary outline-none' />

    )} */}
          <DatePicker
            selected={startDate}
            onChange={(date: Date) => {
              setStartDate(date);
              setDate(moment(date).format('YYYY-MM-DD'));
            }}
            dateFormat='dd/MM/yyyy'
            startOpen={false}
            preventOpenOnFocus={true}
            popperPlacement='bottom-start'
            autoFocus={false}
            customInput={
              <button className=' h-14  pl-14 pr-10 border border-white-primary text-center rounded-full bg-gray-secondary outline-none'>
                <div className='absolute left-5'>
                  <AiOutlineCalendar size={25} color='#6C6C6C' />
                </div>
                <p>{moment(startDate).format('DD/MM/yyyy')}</p>
              </button>
            }
          />
          <select
            placeholder='Estado'
            value={statusActive}
            onChange={(value) => setstatusActive(value.target.value)}
            defaultValue={statusActive}
            className='ml-8 h-14  px-10 border border-white-primary text-center rounded-full bg-gray-secondary outline-none  items-center'
          >
            <option value='default' disabled hidden>
              Estado
            </option>
            <option value={'pendiente'}>pendiente</option>
            <option value={'resuelto'}>resuelto</option>
          </select>
        </div>

        <div className=' flex flex-row self-end items-center  px-3 justify-between bg-gray-secondary py-3 lg:py-5  md:py-4 placeholder-white-primary border-[1px] border-white-primary rounded-lg focus:outline-none'>
          <Image
            src={MagnifyingGlass}
            alt='magnifying glass'
            width={18}
            height={18}
          />

          <input
            type='text'
            placeholder='Buscar por nombre'
            className='bg-gray-secondary focus:outline-none pl-4'
            value={name}
            onChange={(e) => Setname(e.target.value)}
          />
        </div>
      </div>

      <div className='flex justify-between flex-row flex-wrap lg:w-[60%] sm:w-[90%] mt-12'>
        {faqs.map((item) => (
          <div
            key={item._id}
            onClick={() => handleCategory(item.title)}
            className={`bg-gray-secondary cursor-pointer rounded-md h-24  w-60 items-center flex justify-center mb-6 border-2 ${
              categoryActive == item.title
                ? 'border-pink-primary'
                : ' border-gray-secondary'
            }`}
          >
            <Image src={Help} alt='magnifying glass' width={35} height={35} />
            <p className='pl-3'>{item.title}</p>{' '}
          </div>
        ))}
      </div>
      <div className='flex justify-between flex-row flex-wrap lg:w-[80%] sm:w-[90%] mt-12'>
        {data &&
          !isLoading &&
          data.map((ticket,i) => (
            <div key={i} className=' bg-gray-secondary cursor-pointer rounded-lg   items-center  mb-12 relative w-[46%] h-64 px-5 '>
              <div
                className={`absolute top-[-10px] left-5 ${
                  ticket.status == 'pendiente'
                    ? 'bg-white-secondary'
                    : 'bg-pink-primary'
                } rounded-full px-5 py-1 capitalize`}
              >
                <span>{ticket.status}</span>{' '}
              </div>
              <div className='flex flex-1 flex-row justify-between flex-wrap items-center  h-full py-3'>
                <h2 className=' w-full text-pink-primary text-xl font-semibold'>
                  {ticket.user.first_name + ' ' + ticket.user.last_name}
                </h2>
                <p className='w-full'>{ticket.message}</p>
                <span>{moment(ticket.date).format('YYYY.MM.DD')}</span>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};

ClaimsPage.authenticationEnabled = true;

export default ClaimsPage;
