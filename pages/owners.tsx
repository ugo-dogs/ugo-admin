import type { NextComponentWithAuth } from './_app';

const OwnersPage: NextComponentWithAuth<any> = () => {
  return <h1>Dueños</h1>
}

OwnersPage.authenticationEnabled = true

export default OwnersPage
