import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';

import axios from 'axios';

export default NextAuth({
  providers: [
    CredentialsProvider({
      name: 'credenciales',
      credentials: {
        email: {
          label: 'Email',
          type: 'text',
          placeholder: 'prueba@gmail.com',
        },
        password: { label: 'Contraseña', type: 'password' },
      },
      async authorize(credentials) {
        try {
          const { data } = await axios.post(
            `${process.env.NEXT_PUBLIC_API_URL}/auth/local`,
            {
              identifier: credentials.email,
              password: credentials.password,
            }
          );

          if (data) {
            return {
              user: data.user,
              jwt: data.jwt,
            };
          } else {
            return null;
          }
        } catch (e) {
          // console.log('caught error');
          // const errorMessage = e.response.data.message
          // Redirecting to the login page with error message          in the URL
          // throw new Error(errorMessage + '&email=' + credentials.email)
          return null;
        }
      },
    }),
  ],
  secret: process.env.SECRET,
  session: {
    strategy: 'jwt',
  },
  jwt: {
    secret: process.env.SECRET,
  },
  callbacks: {
    jwt: ({ token, user: data }) => {
      if (data) {
        token.jwt = data.jwt;
        token.name = data.user.name;
        token.email = data.user.email;
        token.picture = data.user.thumb.url;
      }
      return token;
    },
    session: ({ session, token }) => {
      if (token) {
        session.user.name = token.name;
        session.user.email = token.email;
        session.user.image = token.picture;
        session.jwt = token.jwt;
      }
      return session;
    },
  },
  pages: {
    signIn: '/auth/signin',
  },
});
