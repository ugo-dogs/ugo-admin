import { useState } from 'react';
import type { GetServerSideProps } from 'next';
import { QueryFunctionContext, useQuery, useQueryClient } from 'react-query';

import Image from 'next/image';

import Sidebar from '../components/Sidebar';
import Valoration from '../components/Valoration';

import MagnifyingGlass from '../public/magnifying-glass.svg';
import Alerts from '../public/alerts.svg';

import type { NextComponentWithAuth } from './_app';
import SidebarFeed from '../components/SideBarFeed';

const { NEXT_PUBLIC_API_URL } = process.env;
import { Barrios, Walker } from '../lib/types';
import FlatlistPaseadores from '../components/FlatlistPaseadores';


const getWalkersFeed = async (
  key?: QueryFunctionContext<any, any>
): Promise<Array<Walker>> => {
  const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/users?paseador=true&verified=false`);

  // apiUrl.searchParams.append('paseador', 'true');
  // apiUrl.searchParams.append('verified', null);


  const res = await fetch(apiUrl.href);
  const data = await res.json();


  
  

  return data;
};


const getWalkers = async (
  key?: QueryFunctionContext<any, any>
): Promise<Array<Walker>> => {
  const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/users`);

  apiUrl.searchParams.append('paseador', 'true');
  apiUrl.searchParams.append('verified', 'true');

  let filters: Array<(paseador: Walker) => boolean> = [];

  const name = key?.queryKey[1].name;
  if (name) {
    const regex = new RegExp(name, 'i');
    filters.push(function (paseador: Walker) {
      return regex.test(paseador.first_name) || regex.test(paseador.last_name);
    });
  }
  const zone = key?.queryKey[2].zone;
  if (zone) {
    filters.push(function (paseador: Walker) {
      return paseador.paseador_zone.barrio === zone;
    });
  }

  const punctuation = key?.queryKey[3].punctuation;
  if (punctuation) {
    filters.push(function (paseador: Walker) {
      return paseador.valoration?.stars === parseInt(punctuation);
    });
  }

  const walkQuantity = key?.queryKey[4].walkQuantity;
  if (walkQuantity) {
    filters.push(function (paseador: Walker) {
      return walkQuantity === 5
        ? paseador.turnos.length >= 5
        : paseador.turnos.length === parseInt(walkQuantity);
    });
  }

  const res = await fetch(apiUrl.href);
  const data = await res.json();

  if (filters.length > 0) {
    return data.filter((paseador: Walker) =>
      filters.every((f) => f(paseador))
    );
  }

  return data;
};
const getBarrios = async ()=>{
  try {
    const result = await fetch(process.env.NEXT_PUBLIC_API_URL + '/barrios');
    const parsed = await result.json();
    return parsed.zonas
  } catch (err) {
    console.log('err',err);
  }
}
export const getServerSideProps: GetServerSideProps = async () => {
  try {
    const walkers = await getWalkers();
const feed = await getWalkersFeed()
const barrios = await getBarrios();

    return {
      props: {
        walkers: walkers,
        feed:feed,
        barrios: barrios,
      },
    };
  } catch (e) {
    console.log(e);
    return {
      props: {
        walks: [],
        feed:[]
      },
    };
  }
};
type Props = {
  walkers: Array<Walker>;
  feed:Array<Walker>,
  barrios:Array<Barrios>
};

const WalkersPage: NextComponentWithAuth<Props> = ({ walkers,feed ,barrios }) => {
  const [showSidebar, setShowSidebar] = useState(false);
  const [showSidebarFeed, setShowSidebarFeed] = useState(false);

  const [activeWalker, setActiveWalker] = useState('');
  const [reviews, setReviews] = useState([]);
  const [punctuation, setPunctuation] = useState('');
  const [walkQuantity, setWalkQuantity] = useState('');
  const [zone, setZone] = useState('');
  const [name, setName] = useState('');

  const queryClient = useQueryClient();

  const handleSidebar = async (id: string) => {
   
    
    setActiveWalker(id);
    setShowSidebar(!showSidebar);
    try {
      const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/resenas`);

      apiUrl.searchParams.append('paseador._id', id);

      const res = await fetch(apiUrl.href);

      const reviews = await res.json();
      setReviews(reviews);
    } catch (e) {
      console.log(e);
      setReviews([]);
    }
  };

  const handleCloseSidebar = () => {
    setActiveWalker('');
    setShowSidebar(false);
    setReviews([]);
  };

  const walker = walkers.find((walker) => walker._id === activeWalker);

  const { data, isSuccess, isLoading } = useQuery(
    ['walks', { name }, { zone }, { punctuation }, { walkQuantity }],
    getWalkers,
    {
      initialData: walkers,
      retry: false,
    }
  );

  return (
    <div className='p-16'>

      <div className='flex justify-between'>
        <div>
          <select
            name='stars'
            className='mr-8 py-2 px-4 bg-gray-secondary rounded-full border-white-primary border-[1px]'
            onChange={(e) => setPunctuation(e.target.value)}
          >
            <option value='' selected>
              Puntuación
            </option>
            <option value='1'>1 estrella</option>
            <option value='2'>2 estrellas</option>
            <option value='3'>3 estrellas</option>
            <option value='4'>4 estrellas</option>
            <option value='5'>5 estrellas</option>
          </select>

          <select
            name='walksQuantity'
            className='mr-8 py-2 px-4 bg-gray-secondary rounded-full border-white-primary border-[1px]'
            onChange={(e) => setWalkQuantity(e.target.value)}
          >
            <option value='' selected>
              Cantidad de Paseos
            </option>
            <option value='1'>1</option>
            <option value='2'>2</option>
            <option value='3'>3</option>
            <option value='4'>4</option>
            <option value='5'>5+</option>
          </select>

          <select
            name='zone'
            className='mr-8 py-2 px-4 bg-gray-secondary rounded-full border-white-primary border-[1px]'
            onChange={(e) => setZone(e.target.value)}
          >
            <option value='' selected>
              Zona de Paseo
            </option>
            <option value='Palermo'>Palermo</option>
            <option value='Recoleta'>Recoleta</option>
          </select>
        </div>
        <div className='flex flex-row items-center  px-3 justify-between bg-gray-secondary py-3 lg:py-5  md:py-4 placeholder-white-primary border-[1px] border-white-primary rounded-lg focus:outline-none'>
            <Image
              src={MagnifyingGlass}
              alt='magnifying glass'
              width={18}
              height={18}
            />
    
          <input
            type='text'
            placeholder='Buscar por nombre'
            className='bg-gray-secondary focus:outline-none pl-4'
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
        {feed.length > 0 && (

<div className='flex flex-row items-center md:ml-5  px-4 relative justify-between rounded-lg focus:outline-none bg-pink-secondary cursor-pointer' onClick={()=> {setShowSidebarFeed(true)}}>
<Image
  src={Alerts}
  alt='magnifying glass'
  width={18}
  height={18}
/>
<div className='absolute top-[-10px] right-[-10px] bg-white-primary rounded-full h-8 w-8 justify-center items-center flex'>
  <p className='text-pink-primary font-bold'>{feed.length}</p>
</div>
<p className='pl-3'>Solicitudes de paseadores</p>
</div>
       )} 
      
      </div>
      <FlatlistPaseadores data={walkers} handleSidebar={(id: string) => handleSidebar(id)} onSelectPaseador={(walker: object) =>{}} initialValue={undefined} />
     
      {showSidebar && walker && (
        <Sidebar
          walker={walker!}
          reviews={reviews}
          barrios={barrios}
          showSidebar={showSidebar}
          handleClose={handleCloseSidebar}
        />
      )}

      {showSidebarFeed && (
            <SidebarFeed
            walker={walker!}
            feeds={feed}
            barrios={barrios}
            showSidebar={showSidebarFeed}
            handleClose={()=>setShowSidebarFeed(false)}
          />
      )}
    </div>
  );
};

WalkersPage.authenticationEnabled = true;

export default WalkersPage;
