import type { NextComponentWithAuth } from './_app'

import NotFoundComponent from '../components/NotFound'

const NotFoundPage: NextComponentWithAuth<any> = () => {
  return (
    <div className="flex min-h-screen">
      <NotFoundComponent />
    </div>
  )
}

NotFoundPage.authenticationEnabled = false

export default NotFoundPage
