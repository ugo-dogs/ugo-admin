import type { NextComponentWithAuth } from './_app';
import type { GetServerSideProps } from 'next';
import { getToken } from 'next-auth/jwt';

import { signIn, useSession } from 'next-auth/react';
import { QueryFunctionContext, useQuery, useQueryClient } from 'react-query';
import type { Payment } from '../lib/types';
import TextFilter from '../components/TextFilter';
import SelectFilter from '../components/SelectFilter';
import { useState } from 'react';
import { today, tomorrow, dayAfterTomorrow } from '../utils/dates';
import classnames from 'classnames';
import PaymentsTable from '../components/PaymentsTable';
import Modal from '../components/Modal';
import { signOut } from 'next-auth/react';

const { NEXT_PUBLIC_API_URL, SECRET } = process.env;
const handlePayment = async (jwt: string, data: object) => {
  const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/transactions/payment`);

  const res = await fetch(apiUrl.href, {
    headers: {
      Authorization: `Bearer ${jwt}`,
    },
  });

  const parsed = await res.json();
  console.log('data', parsed);
  return parsed;
};
const getPayments = async (
  jwt: string,
  key?: QueryFunctionContext<any, any>
) => {
  const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/payments`);

  const walkerName = key?.queryKey[1].walkerName;
  console.log('walker', walkerName);

  if (walkerName) {
    apiUrl.searchParams.append('paseador.first_name_contains', walkerName);
  }
  const paymentState = key?.queryKey[2].paymentState;
  if (paymentState) {
    console.log('aca', paymentState);

    apiUrl.searchParams.append('status', paymentState);
  }
try {
  const res = await fetch(apiUrl.href, {
    headers: {
      Authorization: `Bearer ${jwt}`,
    },
  });

  const data = await res.json();
 

    return data;
  
} catch (error) {
  console.log('entro aca?');

 return error
}



  
};
export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  try {
    const token = (await getToken({ req, secret: SECRET! })) as any;


    const finalResult = await getPayments(token.jwt);
    console.log('final',finalResult);

    if(finalResult.error){
      signOut()
      return {
        
        redirect :{
          destination: '/api/auth/signout',
          permanent: true,
      }
    }
    }
    return {
      props: {
        payments: finalResult,
      },
    };
  } catch (e) {
    console.log(e);
    
    return {
      props: {
        payments: [],
      },
    };
  }
};
type Props = {
  payments: Array<Payment>;
};

const PaymentsPage: NextComponentWithAuth<Props> = ({ payments }) => {
  const [walker, setWalker] = useState('');
  const [date, setDate] = useState('');
  const [showModal, setshowModal] = useState(false);
  const [ExpirationPayments, setExpirationPayments] = useState(true);

  const [historyPayments, setHistoryPayments] = useState(false);
  const [modalPayment, setmodalPayment] = useState<Payment>();
  const [paymentState, setPaymentState] = useState('');

  const session = useSession();
  const queryClient = useQueryClient();

  const { data, isSuccess, isLoading } = useQuery(
    ['payments', { walkerName: walker }, { paymentState }],
    (key) => getPayments(session.data!.jwt, key),
    { initialData: payments, retry: false }
  );
  const handleWalkerName = (value: string) => {
    if (value || walker !== '') {
      setWalker(value);
    }
    // setPaseadorId('')
    // setShowWalk(false)
  };

  const handleDate = (value: string) => {
    // setPaseadorId('')
    // setShowWalk(false)
    setDate(value);
  };
  const handleWalkState = (value: string) => {
    // setPaseadorId('')
    // setShowWalk(false)
    setPaymentState(value);
  };
  const showModalPayment = (payment: Payment) => {
    setshowModal(true);
    setmodalPayment(payment);
  };
  const buttonClassnames = (isActive: boolean) =>
    classnames(
      'pt-2 pb-2 pr-7 pl-7 border border-white-primary text-center rounded-full hover:border-pink-primary hover:text-pink-primary',
      {
        'border-pink-primary bg-pink-primary text-gray-primary hover:text-gray-primary':
          isActive,
      }
    );
  console.log('effect', data);
  const handlePayment = async (data: object) => {};
  return (
    <div className='flex '>
      <div className='h-screen w-80  bg-gray-secondary pl-8 pr-4 pt-9 pb-5 overflow-auto'>
        <h1 className='font-bold text-3xl'>Pagos</h1>

        <TextFilter
          label='Paseadores'
          placeholder='Buscar por nombre'
          action={handleWalkerName}
        />

        <SelectFilter
          label='Fecha'
          defaultValue='Buscar por fecha'
          options={[
            { name: today, value: today },
            { name: tomorrow, value: tomorrow },
            { name: dayAfterTomorrow, value: dayAfterTomorrow },
          ]}
          action={handleDate}
        />

        <SelectFilter
          label='Estado del pago'
          defaultValue='Buscar por estado'
          options={[
            { name: 'Todos', value: '' },
            { name: 'A pagar', value: 'pending' },
            { name: 'Pagado', value: 'approved' },
            { name: 'devuelto', value: 'refund' },
            { name: 'cancelado', value: 'cancelled' },
            { name: 'vencido', value: 'expirated' },
          ]}
          action={handleWalkState}
        />
      </div>
      <div className='p-11 lg:w-[70%]  md:w-[75%] sm:w-[90%] relative'>
        {showModal && (
          <Modal
            onClose={() => setshowModal(false)}
            data={modalPayment!}
            handlePayment={(data: Payment) => handlePayment(data)}
          />
        )}
        <div className='mb-10 '>
          <button
            className={`mr-5 ${buttonClassnames(ExpirationPayments)}`}
            onClick={() => {
              setExpirationPayments(!ExpirationPayments);
              setHistoryPayments(false);
            }}
          >
            Paseos próximos a vencer
          </button>
          <button
            className={buttonClassnames(historyPayments)}
            onClick={() => {
              setHistoryPayments(!historyPayments);
              setExpirationPayments(false);
            }}
          >
            Historial de Pagos
          </button>
        </div>
        {/* {isSuccess && (
          <Table
            walks={data!}
            handleClick={(paseadorId: string) => showWalkDetail(paseadorId)}
            isClosed={showWalk}
            isLoading={isLoading}
          />
        )} */}
        {isSuccess && (
          <PaymentsTable
            payments={data!}
            handleClick={(payment: Payment) => showModalPayment(payment)}
            isClosed={showModal}
            historyPayments={historyPayments}
            ExpirationPayment={ExpirationPayments}
            isLoading={isLoading}
          />
        )}
      </div>
    </div>
  );
};

PaymentsPage.authenticationEnabled = true;

export default PaymentsPage;
