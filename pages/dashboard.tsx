import React,  { useState, useEffect } from 'react';
import { QueryFunctionContext, useQuery, useQueryClient } from 'react-query';

import type { NextComponentWithAuth } from './_app';
import type { ReservesHP,InputDog,InputOwner,Inputs } from '../lib/types';
import { GetServerSideProps } from 'next';
import { getToken } from 'next-auth/jwt';
import { useSession } from 'next-auth/react';
const { NEXT_PUBLIC_API_URL, SECRET } = process.env;

import DashCard from '../components/HouseParadise/DashCard';
import DaysResume from '../components/HouseParadise/DaysResume';



const getReserves = async (
    jwt: string,
    key?: QueryFunctionContext<any, any>
  ) => {
    const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/reserves-hps`);
  
    // const dogName = key?.queryKey[1].dogName;
    // if (dogName) {
    //   apiUrl.searchParams.append('dog.name_contains', dogName);
    // }
  
    const type = key?.queryKey[1].type;
    // console.log('type', type);
  
    if (type) {
      apiUrl.searchParams.append('aob_purchased_eq', type);
    }
    const ownerName = key?.queryKey[2].ownerName;
    // console.log('ownerName', ownerName);
  
    if (ownerName) {
      apiUrl.searchParams.append('owner_name_contains', ownerName);
    }
  
  
    apiUrl.searchParams.append('_sort', 'createdAt:DESC');
  
    const res = await fetch(apiUrl.href, {
      headers: {
        Authorization: `Bearer ${jwt}`,
      },
    });
  
    const data = await res.json();
    // console.log('data', data);
  
    return data;
};


export const getServerSideProps: GetServerSideProps = async ({ req }) => {
    try {
        const token = (await getToken({ req, secret: SECRET! })) as any;

        const currentDate = new Date();
        const startOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1, 0, 0, 0);
        const lastDay = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0, 23, 59, 59);


        const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/reserves-hps`);
        apiUrl.searchParams.append('aob_date_start_gte', startOfMonth.toISOString());
        apiUrl.searchParams.append('aob_date_end_lte', lastDay.toISOString()); // Use _lte to filter <= lastDay
        apiUrl.searchParams.append('_sort', 'createdAt:DESC');
    

        const finalResult = await fetch(apiUrl.href, {
            headers: {
                Authorization: `Bearer ${token.jwt}`,
            },
        }).then((res) => res.json());

        return {
            props: {
                reserves: finalResult,
            },
        };
    } catch (e) {
        // console.log(e);
        return {
            props: {
                reserves: [],
            },
        };
    }
};
  
  type Props = {
    reserves: Array<ReservesHP>;
  };
  
  export type FilterType = {
    key: string;
    value: string;
  };

const DashBoard: NextComponentWithAuth<Props> = ({ reserves }) => {

    const [dataDashcard, setdataDashcard] = useState<any>({});

    const countReservesToday = () => {
        let count = 0;
        
        const today = new Date();
        reserves.map((reserve) => {
            const startDate = new Date(reserve.aob_date_start);
            const endDate = new Date(reserve.aob_date_end);
            if (today >= startDate && today <= endDate) {
                count++;
            }
        });
        return count;
    };


    const filterReservesStartingToday = () => {
        const today = new Date();
        const todayStart = new Date(today.getFullYear(), today.getMonth(), today.getDate()); // Set hours, minutes, and seconds to 0
    
        const reservesStartingToday = reserves.filter((reserve) => {
        const startDate = new Date(reserve.aob_date_start);
        startDate.setHours(0, 0, 0, 0); // Set hours, minutes, and seconds to 0
    
        // Add one day to startDate
        startDate.setDate(startDate.getDate() + 1);
    
        return startDate.getTime() === todayStart.getTime(); // Compare only year, month, and day
        });
    
        return reservesStartingToday;
    };

    const filterReservesEndingToday = () => {
        const today = new Date();
        const todayStart = new Date(today.getFullYear(), today.getMonth(), today.getDate()); // Set hours, minutes, and seconds to 0

        const reservesEndingToday = reserves.filter((reserve) => {
            const endDate = new Date(reserve.aob_date_end);
            endDate.setHours(0, 0, 0, 0); // Set hours, minutes, and seconds to 0

            // Add one day to startDate
            endDate.setDate(endDate.getDate() + 1);

            console.log(endDate + reserve.dog_name)

            return endDate.getTime() === todayStart.getTime(); // Compare only year, month, and day
        });

        return reservesEndingToday;
    };


    const totalPrice = () => {
            let total = 0;
            reserves.map((reserve) => {
                total += reserve.aob_price;
            })
            return total;
    }
        
    const averageTicket = () => {
            let total = 0;
            reserves.map((reserve) => {
                total += reserve.aob_price;
            })
            return total / reserves.length;
    }

    
    const averageDays = () => {
        let totalDays = 0;
        reserves.forEach((reserve) => {
            const startDate = new Date(reserve.aob_date_start);
            const endDate = new Date(reserve.aob_date_end);
            const diffTime = Math.abs(endDate.getTime() - startDate.getTime());
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            totalDays += diffDays;
        });
        return Number((totalDays / reserves.length).toFixed(0));
    };

    console.log('reserves', reserves);

    function formatPrice(price: number): string {
        const formatter = new Intl.NumberFormat('es-AR', {
            style: 'currency',
            currency: 'ARS',
            minimumFractionDigits: 0,
        });
        return formatter.format(price);
    }
    
    return (
    <section className="container p-5 sm:p-16 dashboard-container font-laussane ">
        <div className="flex sm:flex-row flex-wrap justify-between gap-5 sm:gap-10 mb-10">
            <DashCard  
                title={'Perros Real time'}
                comparison={'+5%'} 
                dataDaschard={dataDashcard}
                setdataDaschard={setdataDashcard} 
            >
                <h2 className="text-8xl text-white font-bold mt-10 font-faro">{countReservesToday()}</h2>
            </DashCard>


            <DashCard  
                title={'Facturación mensual'}
                comparison={'+3%'} 
                dataDaschard={dataDashcard}
                setdataDaschard={setdataDashcard} 
            >
                <h2 className="text-4xl text-white font-bold mt-10 font-faro">{formatPrice(totalPrice())}</h2>
            
            </DashCard>


            <DashCard  
                title={'Total Cobrado'}
                comparison={''} 
                dataDaschard={dataDashcard}
                setdataDaschard={setdataDashcard} 
            >
                <div>
                    <p className="text-[#888]">45% del total</p>
                    <h2 className="text-4xl text-white font-bold font-faro">{formatPrice(150000)}</h2>
                </div>
            </DashCard>

            <DashCard  
                title={'A cobrar'}
                comparison={''} 
                dataDaschard={dataDashcard}
                setdataDaschard={setdataDashcard} 
            >
                <div>
                    <p className="text-[#888]">55% del total</p>
                    <h2 className="text-4xl text-white font-bold font-faro">{formatPrice(171345)}</h2>
                </div>
            </DashCard>


        
        </div>

        <div className="flex flex-col sm:flex-row justify-between gap-5 sm:gap-10">
            <DashCard  
                title={'Ticket Promedio'}
                comparison={''} 
                dataDaschard={dataDashcard}
                setdataDaschard={setdataDashcard} 
            >
                <h2 className="text-4xl text-white font-bold mt-10 font-faro">{formatPrice(averageTicket())}</h2>
            </DashCard>


            <DashCard  
                title={'Días Promedio'}
                comparison={''} 
                dataDaschard={dataDashcard}
                setdataDaschard={setdataDashcard} 
            >
                <h2 className="text-8xl text-white font-bold mt-10 font-faro">{averageDays()}</h2>
            
            </DashCard>

            <DashCard  
                title={'Perros totales este mes'}
                comparison={''} 
                dataDaschard={dataDashcard}
                setdataDaschard={setdataDashcard} 
            >
                <h2 className="text-8xl text-white font-bold mt-10 font-faro">{reserves.length}</h2>
            
            </DashCard>


            <DashCard  
                title={'Usuarios'}
                comparison={''} 
                dataDaschard={dataDashcard}
                setdataDaschard={setdataDashcard} 
            >
                <div>
                    <div className="flex items-center gap-2">
                        <h2 className="text-4xl text-white font-bold font-faro">30</h2>
                        <p className="text-[#888]">Nuevos</p>
                    </div>

                    <div className="flex items-center gap-2">
                        <h2 className="text-4xl text-white font-bold font-faro">72</h2>
                        <p className="text-[#888]">recurrentes</p>
                    </div>
                </div>
            </DashCard>


        
        </div>

        <div className="flex flex-col sm:flex-row justify-between gap-10 my-20">
            <DaysResume
                // dataDaysResume={dataDashcard}
                // setdataDaysResume={setdataDashcard}
                title={'Entrando hoy'}
                comparison={''}
                isEntering={true}
                data = {filterReservesStartingToday()}
            />



            <DaysResume
                title={'Saliendo hoy'}
                comparison={''}
                isEntering={false}
                data = {filterReservesEndingToday()}
            />
            </div>
    </section>
    )
}
DashBoard.authenticationEnabled = true;

export default DashBoard

