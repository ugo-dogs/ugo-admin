import { SessionProvider } from 'next-auth/react';
import { NextComponentType, NextPageContext } from 'next';
import type { AppProps } from 'next/app';
import { QueryClientProvider, QueryClient } from 'react-query';

import 'tachyons/css/tachyons.min.css';
import '../styles/globals.css';
import '../styles/dataGridStyles.css';

import AuthGuard from '../components/AuthGuard';
import { AuthEnabledComponentConfig } from '../utils/auth';

import localFont from 'next/font/local'
 

const Lausanne = localFont({ src: '../src/fonts/Lausanne-300.woff', variable: '--font-laussane', })
const Faro = localFont({ src: '../src/fonts/Faro-DisplayLucky.woff', variable: '--font-faro' })


export type NextComponentWithAuth<T> = NextComponentType<
  NextPageContext,
  any,
  T
> &
  Partial<AuthEnabledComponentConfig>;

const queryClient = new QueryClient();

function MyApp(props: AppProps) {
  const {
    Component,
    pageProps,
  }: { Component: NextComponentWithAuth<any>; pageProps: any } = props;

  return (

    
    <SessionProvider session={pageProps.session}>

    <style jsx global>{`
        .font-faro {
          font-family: ${Faro.style.fontFamily} !important;
        }
        
        .font-laussane  {
          font-family: ${Lausanne.style.fontFamily} !important;
        };
      `}</style>


      {Component.authenticationEnabled ? (
        <QueryClientProvider client={queryClient}>
          <AuthGuard>
            <Component {...pageProps} />
          </AuthGuard>
        </QueryClientProvider>
      ) : (
        // public page
        <Component {...pageProps} />
      )}
    </SessionProvider>
  );
}
export default MyApp;
