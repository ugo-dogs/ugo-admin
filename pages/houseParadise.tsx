import type { GetServerSideProps } from 'next';
import { getToken } from 'next-auth/jwt';
import { useSession } from 'next-auth/react';

import { useEffect, useState } from 'react';
import classnames from 'classnames';
import { QueryFunctionContext, useQuery, useQueryClient } from 'react-query';

import type { NextComponentWithAuth } from './_app';

import type { ReservesHP,InputDog,InputOwner,Inputs, EditReserve } from '../lib/types';

import TextFilter from '../components/TextFilter';
import SelectFilter from '../components/SelectFilter';
import { isMobile } from 'react-device-detect';

import Image from 'next/image';

import Close from '../public/close.png';

import TableHP from '../components/TableHP';
import ReserveDetail from '../components/ReserveDetailNew';
import ModalAddReserve from '../components/ModalAddReserve';
import moment from 'moment';

const { NEXT_PUBLIC_API_URL, SECRET } = process.env;

const getReserves = async (
  jwt: string,
  key?: QueryFunctionContext<any, any>
) => {
  const apiUrl = new URL(`${process.env.NEXT_PUBLIC_API_URL}/reserves-hps`);
  // const dogName = key?.queryKey[1].dogName;
  // if (dogName) {
  //   apiUrl.searchParams.append('dog.name_contains', dogName);
  // }

  const type = key?.queryKey[1].type;
  console.log('type', type);

  if (type) {
    apiUrl.searchParams.append('aob_purchased_eq', type);
  }
  const ownerName = key?.queryKey[2].ownerName;
  console.log('ownerName', ownerName);

  if (ownerName) {
    apiUrl.searchParams.append('owner_name_contains', ownerName);
  }
  // const walkState = key?.queryKey[4].walkState;
  // if (walkState) {
  //   apiUrl.searchParams.append('status', walkState);
  // }

  // const zone = key?.queryKey[5].zone;
  // if (zone) {
  //   apiUrl.searchParams.append('direccion.barrio.barrio_contains', zone);
  // }

  apiUrl.searchParams.append('_sort', 'createdAt:DESC');

  const res = await fetch(apiUrl.href, {
    headers: {
      Authorization: `Bearer ${jwt}`,
    },
  });

  const data = await res.json();
  // console.log('data', data);

  return data;
};

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
  try {
    const token = (await getToken({ req, secret: SECRET! })) as any;

    const finalResult = await getReserves(token.jwt);

    return {
      props: {
        reserves: finalResult,
      },
    };
  } catch (e) {
    console.log(e);
    return {
      props: {
        walks: [],
      },
    };
  }
};

type Props = {
  reserves: Array<ReservesHP>;
};

export type FilterType = {
  key: string;
  value: string;
};

const HouseParadise: NextComponentWithAuth<Props> = ({ reserves }) => {

  const [date, setDate] = useState('');
  const [type, setype] = useState('');
  const [ownerName, setownerName] = useState('');

  const [walkState, setWalkState] = useState('');
  const [zone, setZone] = useState('');

  const [showReserve, setshowReserve] = useState(false);
  const [reserve, setReserve] = useState<ReservesHP>();
  const [showAddReserve, setshowAddReserve] = useState(false)
  const [InputReserve, setInputReserve] = useState<any>([]);


  const [isStartingToday, setisStartingToday] = useState(false);
  const [isActiveTodayWalks, setActiveTodayWalks] = useState(false);
  const [reserves_arr, setreserves_arr] = useState<ReservesHP[]>()
  const [editReserve, seteditReserve] = useState<EditReserve>()
  const session = useSession();


  const { data, isSuccess, isLoading } = useQuery(
    [
      'reserves',
      { type },
      { ownerName },
 
    ],
    (key) => getReserves(session.data!.jwt, key),
    { initialData: reserves, retry: false }
  );
  useEffect(() => {
    
    if(data){
      setreserves_arr(data)
    }
  }
  , [data])
  useEffect(() => {
    
    if(isActiveTodayWalks){
    
      setreserves_arr(data.filter((reserve:ReservesHP) =>moment().isSameOrBefore(reserve.aob_date_end) && moment().isSameOrAfter(reserve.aob_date_start)))
    } else {
      setreserves_arr(data)
    }
  }
  , [isActiveTodayWalks])
  useEffect(() => {
    
    if(isStartingToday){
    
      setreserves_arr(data.filter((reserve:ReservesHP) =>moment().isSame(reserve.aob_date_start)))
    } else {
      setreserves_arr(data)
    }
  }
  , [isStartingToday])
  const handleOwnerName = (value: string) => {
    console.log('value', value);

    setownerName(value);
  };
  const handleType = (value: string) => {
    setype(value);
  };
const handleEditReserve = async  (id:string) => {
try {
   const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/reserves-hps/${id}`,{
    headers: {
      'Content-Type': 'application/json',
    },
   })
    const data = await response.json()
    
    seteditReserve({
      ownerData: {
        mail: data.owner_email,
        nombre: data.owner_name,
        telefono: data.owner_phone,
        dni: data.owner_dni,
        apellido: data.owner_surname,
        dirección:data.owner?.dirección || "",
        provincia:data.provincia
      },
      dogData:{
        nombre: data.dog_name,
        dog_name: data.dog_name,
        date_celo: data.date_celo, // no anda
        dog_behaviour:data.dog_behaviour,
        edad: data.dog_age,
        dog_age: data.dog_age,
        dog_swim: data.dog_swim,
        dog_vaccine: data.dog_vaccine,
        dog_deworming:  data.dog_deworming,
        dog_comments: data.dog_comments,
        dog_food: data.dog_food,
        dog_castrado: data.dog_castrado,
        Género: data.dog_genre,
        raza: data.dog_raza,
        dog_social: data.dog_social,
        castrado: data.dog_castrado,
        dog_genre: data.dog_genre,
        dog_raza: data.dog_raza,
        dog_bite: data.dog_bite,
        ['¿Cuál fue la fecha del último celo?']: data.date_celo,
        ['¿Tiene alguno de estos comportamientos?']:data.dog_behaviour,
        ['Mordío otros perros?']: data.dog_bite,
        ["Sabe nadar?"]: "no",
        ["Tiene alérgias?" ]: data.dog_alergia == 'No' ?false:true,
        ["Se realizó alguna cirugía?"]: data.dog_cirugia == 'No' ?false:true,
        ["Confirmo que a mi mascota se le realizó una desparasitación externa e interna"]:data.dog_deworming,
        ["Confirmo que mi mascota está vacunado con la Séxtuple, Antirrábica y Tos de las perreras"]:data.dog_vaccine,
     
      },
      dataTrip:{
        aob_date_start:data.aob_date_start,
        aob_date_end: data.aob_date_end,
        aob_price: data.aob_price,
        aob_purchased: data.aob_purchased,
      },
      id:data._id

    })
    handleInputReserve()
} catch (error) {
    console.log('error',error);    
}
}
const handleInputReserve = async  () => {
  if(InputReserve.length == 0){


    const inputs = await Promise.all([
    
     await fetch(`${process.env.NEXT_PUBLIC_API_URL}/inputs-web-owner`),
      await fetch(`${process.env.NEXT_PUBLIC_API_URL}/inputs-web-dog`)]
      ) 
      const [inputsOwnerResponse, inputsDogResponse] = inputs;
    
      // Asegúrate de que las respuestas son exitosas antes de continuar
      if (!inputsOwnerResponse.ok || !inputsDogResponse.ok) {
        throw new Error("Error al obtener los datos de las API");
      }
    
      // Convierte las respuestas en objetos JSON y asigna los tipos
      let inputsOwner = await inputsOwnerResponse.json();
      inputsOwner.title = "Datos dueño";
      inputsOwner.icon = "personal";
      let inputsDog = await inputsDogResponse.json();
    inputsDog.title = "Datos perro";
    inputsDog.icon = "dog";
    const inputTrip: any = {
      title: "Datos estadia",
      icon: "calendar"
    }
    const inputPayment: any = {
      title: "Pagos",
      icon: "bank"
    }
    
    const format = [inputsOwner, inputsDog,inputTrip,inputPayment];
    
    
      setInputReserve(format)
    }
      setshowAddReserve(true)
}
const handleAddReserve =async  () => {
 await  handleInputReserve()
}

const showReserveDetail = (reserve: ReservesHP) => {
  setshowReserve(true);
  setReserve(reserve);
};

const hideWalkDetail = () => {
  setshowReserve(false);
};

const buttonClassnames = (isActive: boolean) =>
  classnames(
    'pt-2 pb-2 pr-7 pl-7 border border-white-primary text-center rounded-full hover:border-pink-primary hover:text-pink-primary',
    {
      'border-pink-primary bg-pink-primary text-gray-primary hover:text-gray-primary':
        isActive,
    }
  );

  return (
    <div className='flex overflow-auto h-full'>
      <div
        className={`h-full w-30 bg-gray-secondary pl-8 pr-4 pt-9 pb-5 sticky top-0 hidden  ${
          showReserve && 'opacity-10'
        }`}
      >
        <h1 className='font-bold f5'>Reservas</h1>
     
 
        {(type || ownerName || date || walkState || zone) && (
          <div className='border-t-2 border-black-primary mt-10'>
            <div className='mt-6 font-bold'>
              <span className='text-[13px]'>Filtros Aplicados</span>
              <ul className='space-x-4 flex flex-wrap'>
                {type && (
                  <li className='mt-4 p-2 bg-black-primary text-pink-primary max-w-max rounded-md flex'>
                    <span className='text-[11px]'>{type}</span>
                    <button
                      className='bg-white-primary hover:bg-gray-tertiary text-black-primary rounded-full h-4 w-4 flex items-center justify-center ml-5'
                      onClick={() => handleType('')}
                    >
                      <Image src={Close} alt='close' width={5} height={5} />
                    </button>
                  </li>
                )}
                {ownerName && (
                  <li className='mt-4 p-2 bg-black-primary text-pink-primary max-w-max rounded-md flex'>
                    <span className='text-[11px]'>{ownerName}</span>
                    <button
                      className='bg-white-primary hover:bg-gray-tertiary text-black-primary rounded-full h-4 w-4 flex items-center justify-center ml-5'
                      onClick={() => handleOwnerName('')}
                    >
                      <Image src={Close} alt='close' width={5} height={5} />
                    </button>
                  </li>
                )}
              </ul>
            </div>
          </div>
        )}
      </div>
      <div
        className={`sm:p-11 p-5 flex-1 relative h-[100vh] w-screen  sm:w-full ${
          showReserve && 'opacity-10 overflow-scroll sm:h-full'
        }`}
      >
        <div className='mb-10 flex justify-between items-center'>
          <div className="flex items-center justify-between">

          <TextFilter
              label=''
              placeholder='Buscar por nombre'
              action={handleOwnerName}
            />
     
        <SelectFilter
          label=''
          // className="f5"
          defaultValue={'Buscar por tipo'}
          options={[
            { name: 'Consulta', value: 'consulta' },
            { name: 'Anticipo', value: 'anticipo' },
            { name: 'Pagado', value: 'full' },
          ]}
          action={handleType}
        />

       <div className='sm:flex ml-5 gap-3 hidden'>
            <button
              className={buttonClassnames(isActiveTodayWalks)}
              onClick={() => setActiveTodayWalks(!isActiveTodayWalks)}
            >
              Reservas Activas
            </button>
            
            <button
              className={buttonClassnames(isStartingToday)}
              onClick={() => setisStartingToday(!isStartingToday)}
            >
             Entrando hoy
            </button>
            </div>

         

          </div>
          
          <div className="new-reserve-button flex items-center p-2 sm:pt-2 sm:pb-2 sm:pr-7 sm:pl-7 pointer border border-white-primary rounded-full hover:border-pink-primary hover:text-pink-primary"  onClick={() => handleAddReserve()}  >
            <Image  src={require(`../public/content/new.svg`)} height={20}  width={20} alt='menu icon' />
            <button className={`${!isMobile && 'pl3'} text-center`}> {!isMobile && 'Agregar reserva'} </button>
            </div>
            </div>
      
            {isSuccess && reserves_arr &&  (
              <TableHP
                reserves={reserves_arr} // provide a default value of an empty array
                handleClick={(reserve: ReservesHP) => showReserveDetail(reserve)}
                isClosed={showReserve}
                isLoading={isLoading}
              />
            )}
      </div>

      {showReserve && reserve && (
        <ReserveDetail
          detail={reserve}
          showEditReserve={(id) => 
           handleEditReserve(id)
          }
          handleCloseDetail={() => hideWalkDetail()}
        />
      )}
      {showAddReserve && InputReserve && (
        <ModalAddReserve
        isEditMode={editReserve ? true : false}
        reserveData={editReserve}
        handleCloseDetail={() => hideWalkDetail()}

        input={InputReserve} hideModal={() => setshowAddReserve(false)}
         editReserve={undefined} />
      )}
    </div>
  );
};

HouseParadise.authenticationEnabled = true;

export default HouseParadise;
